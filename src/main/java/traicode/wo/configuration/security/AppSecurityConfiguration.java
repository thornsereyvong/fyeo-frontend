package traicode.wo.configuration.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class AppSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private CustomAuthenticationProvider authenticationProvider;

	@Autowired
	private CustomAuthenticationFailureHandler authenticationFailureHandler;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(this.authenticationProvider);
	}

	@Autowired
	private CustomSuccessHandler authenticationSuccessHandler;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests().antMatchers("/login.html", "/additional/**", "/resources/**", "/SlideShow/**",
				"/public/**", "/admin/layout/**", "/layout/**").permitAll();
		http.authorizeRequests().anyRequest().hasAnyAuthority("").and().formLogin().loginPage("/login.html")
				.loginProcessingUrl("/login.html").failureHandler(authenticationFailureHandler).successHandler(authenticationSuccessHandler)
				.usernameParameter("tc_username").passwordParameter("tc_password").and()
				.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class).logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout.html")).logoutSuccessUrl("/login.html?logout")
				.deleteCookies("JSESSIONID").invalidateHttpSession(true).and().exceptionHandling()
				.accessDeniedPage("/login.html?error");
		http.headers().frameOptions().sameOrigin().cacheControl().and().xssProtection().and().contentTypeOptions().and()
				.httpStrictTransportSecurity();
		http.authorizeRequests().anyRequest().authenticated();
		http.csrf().disable();

	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/additional/**", "/resources/**", "/SlideShow/**", "/public/**", "/admin/layout/**",
				"/layout/**");
	}

	@Bean
	public UsernamePasswordAuthenticationFilter authenticationFilter() {
		CustomUsernamPasswordAuthenticationFilter authFilter = new CustomUsernamPasswordAuthenticationFilter();
		List<AuthenticationProvider> authenticationProviderList = new ArrayList<AuthenticationProvider>();
		authenticationProviderList.add(authenticationProvider);
		AuthenticationManager authenticationManager = new ProviderManager(authenticationProviderList);
		authFilter.setAuthenticationManager(authenticationManager);
		authFilter.setAuthenticationSuccessHandler(authenticationSuccessHandler);
		authFilter.setAuthenticationFailureHandler(authenticationFailureHandler);
		authFilter.setUsernameParameter("tc_username");
		authFilter.setPasswordParameter("tc_password");
		return authFilter;
	}

}
