package traicode.wo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import traicode.wo.model.UserLogin;


@Controller
public class MainController {
	
	@Autowired
	private UserLogin user;

	@RequestMapping({ "/", "/index.html", "/home.html" })
	public String index(ModelMap model, HttpSession session, HttpServletRequest request) {
		model.addAttribute("title", "Home");
		model.addAttribute("homeMenu","active");
		//System.out.println(session.getAttribute("userDetails"));
		//System.out.println(userLogin.getShopId());
		this.openMenu(true, model);
		return "index";
	}
	
	@RequestMapping({ "/contact-us.html" })
	public String contact_us(ModelMap model, HttpSession session, HttpServletRequest request) {
		model.addAttribute("title", "Contact Us");
		model.addAttribute("contactMenu","active");
		this.openMenu(false, model);
		return "contact_us";
	}
	
	@RequestMapping({ "/about-us.html" })
	public String about(ModelMap model, HttpSession session, HttpServletRequest request) {
		model.addAttribute("title", "About Us");
		this.openMenu(false, model);
		model.addAttribute("aboutMenu","active");
		return "about_us";
	}
	
	
	@RequestMapping({ "/login.html" })
	public String userLogin(ModelMap model, HttpSession session, HttpServletRequest request) {
		model.addAttribute("title", "User Login");
		this.openMenu(false, model);
		return "user_login";
	}
	
	
	@RequestMapping({ "/products.html" })
	public String products(ModelMap model, HttpSession session, HttpServletRequest request) {
		model.addAttribute("title", "Products");
		this.openMenu(false, model);
		model.addAttribute("proMenu","active");
		return "products";
	}
	
	
	@RequestMapping({ "/product/{model}/{itemId}.html" })
	public String productsitemId(ModelMap model, @PathVariable("itemId") String itemid,@PathVariable("model") String models) {
		model.addAttribute("title", "Products");
		model.addAttribute("itemId", itemid);
		model.addAttribute("model", models);
		this.openMenu(false, model);
		model.addAttribute("proMenu","active");
		return "product_detail";
	}
	
	@RequestMapping({ "/register.html"})
	public String register(ModelMap model, HttpSession session, HttpServletRequest request) {
		model.addAttribute("title", "User Register");
		this.openMenu(false, model);
		return "user_register";
	}
	
	@RequestMapping({ "/user/dashboard.html" })
	public String userdashboard(ModelMap model, HttpSession session) {
		user = (UserLogin) session.getAttribute("userLogin");
		model.addAttribute("title", "Dashboard");
		this.openMenu(false, model);
		return "user_dashboard";
	}
	
	
	@RequestMapping({ "/user/profile.html" })
	public String profile(ModelMap model, HttpSession session, HttpServletRequest request) {
		model.addAttribute("title", "Profile");
		this.openMenu(false, model);
		return "user_profile";
	}
	
	@RequestMapping({ "/user/order.html" })
	public String order(ModelMap model, HttpSession session, HttpServletRequest request) {
		model.addAttribute("title", "Order");
		this.openMenu(false, model);
		return "user_order";
	}
	
	@RequestMapping({ "/user/order/{orderId}/detail.html" })
	public String orderdetail(ModelMap model,@PathVariable("orderId") String orderId , HttpSession session, HttpServletRequest request) {
		model.addAttribute("title", "Order detail");
		model.addAttribute("id", orderId);
		this.openMenu(false, model);
		return "user_order_detail";
	}
	
	
	@RequestMapping({ "/user/inbox.html" })
	public String inbox(ModelMap model, HttpSession session, HttpServletRequest request) {
		model.addAttribute("title", "Order");
		this.openMenu(false, model);
		return "user_inbox";
	}
	
	@RequestMapping({ "/user/wishlist.html" })
	public String wishlist(ModelMap model, HttpSession session, HttpServletRequest request) {
		model.addAttribute("title", "Order");
		this.openMenu(false, model);
		return "user_wishlist";
	}
	
	@RequestMapping({ "/user/cart.html" })
	public String usercart(ModelMap model, HttpSession session, HttpServletRequest request) {
		model.addAttribute("title", "Cart");
		this.openMenu(false, model);
		return "user_cart";
	}
	
	@RequestMapping({ "privancy-policy.html" })
	public String privancy(ModelMap model, HttpSession session, HttpServletRequest request) {
		model.addAttribute("title", "Privancy Policy");
		this.openMenu(false, model);
		return "privancy_policy";
	}
	
	
	@RequestMapping({ "term-condition.html" })
	public String term(ModelMap model, HttpSession session, HttpServletRequest request) {
		model.addAttribute("title", "Term Condition");
		this.openMenu(false, model);
		return "term_condition";
	}
	
	
	
	@RequestMapping({ "/popup"})
	public String popup() {
		return "layout/popup";
	}

	@RequestMapping({ "/head"})
	public String head(ModelMap model) {
		return "layout/head";
	}
	
	
	@RequestMapping({ "/header"})
	public String header(ModelMap model) {
		return "layout/header";
	}
	
	
	@RequestMapping({ "/menu"})
	public String menu(ModelMap model) {
		model.addAttribute("user", this.getPrincipal());
		return "layout/menu";
	}
	
	@RequestMapping({ "/slide"})
	public String slide(ModelMap model) {
		return "layout/slide";
	}
	
	@RequestMapping({ "/foot"})
	public String foot(ModelMap model) {
		return "layout/foot";
	}
	
	@RequestMapping({ "/footer"})
	public String footer(ModelMap model) {
		return "layout/footer";
	}
	
	@RequestMapping({ "/user-admin" })
	public String useradmin(ModelMap model, HttpSession session, HttpServletRequest request) {
		return "layout/user_menu";
	}
	
	private void openMenu(boolean check,ModelMap model){
		if(check){
			model.addAttribute("menuOpening", "opened");
			model.addAttribute("menuStyle", "display: block;");
		}else{
			model.addAttribute("menuOpening", "");
			model.addAttribute("menuStyle", "display: none;");
		}
	}
	
	public String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}
}
