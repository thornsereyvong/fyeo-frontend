package traicode.wo.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import traicode.wo.model.UserLogin;
import traicode.wo.service.UserService;

@Repository
public class UserServiceImpl implements UserService {
	
	@Autowired
	private Environment environment;

	@Autowired
	private HttpHeaders header;
	
	@Autowired
	private RestTemplate  res;
	
	@SuppressWarnings({ "rawtypes", "unchecked"})
	@Override
	public UserLogin findUserByUsername(String username) {
		UserLogin sp = null;
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		map.add("username", username);
		HttpEntity<Object> request = new HttpEntity<Object>(map,header);	
		ResponseEntity<Map> response = res.exchange(environment.getProperty("apiurl")+"api/shop/findusername", HttpMethod.POST, request, Map.class);
		if(response.getBody() != null){
			if(response.getBody().get("MESSAGE").equals("FOUND")){
				sp = new UserLogin();
				Map<String, Object> getUser = (Map<String, Object>) response.getBody().get("RECORDS");
				sp.setUserId(Integer.valueOf(getUser.get("shopId").toString()));
				sp.setUsername(getUser.get("username").toString());
				sp.setPassword(getUser.get("password").toString());
			}
		}
		return sp;
	}
	
}
