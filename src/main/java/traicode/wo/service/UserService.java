package traicode.wo.service;

import org.springframework.stereotype.Service;

import traicode.wo.model.UserLogin;

@Service
public interface UserService {
	UserLogin findUserByUsername(String username);
}
