package traicode.wo.utilities;

import org.springframework.stereotype.Repository;

@Repository
public class LanguageChange {
	private int language;

	public int getLanguage() {
		return language;
	}

	public void setLanguage(int language) {
		this.language = language;
	}
	
	
}
