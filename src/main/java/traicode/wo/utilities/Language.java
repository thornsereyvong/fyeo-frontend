package traicode.wo.utilities;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
@Scope("session")
public class Language {
	private String path;
	private Map<String, ArrayList<String>> langs;
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Map<String, ArrayList<String>> getLangs() {
		return langs;
	}

	public void setLangs(Map<String, ArrayList<String>> langs) {
		this.langs = langs;
	}

	public Language(Map<String, ArrayList<String>> langs) {
		super();
		this.langs = langs;
	}
	public String getLangByKeyIndex(String key, HttpSession session){
		if(this.langs.get(key) != null)
			return this.langs.get(key).get(session.getAttribute("LI") == null ? 0 : (int) session.getAttribute("LI"));
		return "";
	}
	
	public Language() {
		super();
	}
	
	@SuppressWarnings({ "unchecked", "resource" })
	public Language(String path){
		try {
			ObjectMapper mapper = new ObjectMapper();
			path = path+"/file-language/languages.json";			
			BufferedReader oReader = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8")) ;
			String strUTF8Text = "";
			String strLine = null;
			while(true){
			  strLine = oReader.readLine();
			  if(null== strLine) break;
			  strUTF8Text += strLine;
			}
			this.langs = mapper.readValue(strUTF8Text, HashMap.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
