package traicode.wo.api.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import traicode.wo.model.Cart;


@RestController
@RequestMapping("/api/cart")
public class CartController {

	

	@Autowired
	private RestTemplate  restTemplate;
	
	@Autowired
	private HttpHeaders header;
	

	@Autowired
	private Environment environment;
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/list/{page}/{row}",method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> list(HttpServletRequest req,@PathVariable("page") int page,
			@PathVariable("row") int num,
			@RequestParam(value="search", required=false, defaultValue="all") String search
			){
		
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		map.add("search", search);
		map.add("shopId", this.getPrincipal());
		HttpEntity<Object> request = new HttpEntity<Object>(map,header);		
		ResponseEntity<Map> response = restTemplate.exchange(environment.getProperty("apiurl")+"api/cart/list/"+page+"/"+num, HttpMethod.POST, request, Map.class);
		return new ResponseEntity<Map<String,Object>>(response.getBody(), response.getStatusCode());
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/add",method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> add(@RequestBody Cart cart,HttpServletRequest req){	
		cart.setShopId(this.getPrincipal());
		HttpEntity<Object> request = new HttpEntity<Object>(cart,header);	
		ResponseEntity<Map> response = restTemplate.exchange(environment.getProperty("apiurl")+"api/cart/add", HttpMethod.POST, request, Map.class);
		return new ResponseEntity<Map<String,Object>>(response.getBody(), response.getStatusCode());
	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/multi/add",method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> addmulti(@RequestBody List<Cart> carts,HttpServletRequest req){	
		for(int i=0;i<carts.size();i++){
			carts.get(i).setShopId(this.getPrincipal());
		}
		HttpEntity<Object> request = new HttpEntity<Object>(carts,header);	
		ResponseEntity<Map> response = restTemplate.exchange(environment.getProperty("apiurl")+"api/cart/multi/add", HttpMethod.POST, request, Map.class);
		return new ResponseEntity<Map<String,Object>>(response.getBody(), response.getStatusCode());
	}

	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/delete",method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> delete(@RequestBody Cart cart,HttpServletRequest req){	
		HttpEntity<Object> request = new HttpEntity<Object>(cart,header);	
		ResponseEntity<Map> response = restTemplate.exchange(environment.getProperty("apiurl")+"api/cart/delete", HttpMethod.POST, request, Map.class);
		return new ResponseEntity<Map<String,Object>>(response.getBody(), response.getStatusCode());
	}
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/delete/all",method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> deleteAll(HttpServletRequest req){	
		HttpEntity<Object> request = new HttpEntity<Object>(header);	
		ResponseEntity<Map> response = restTemplate.exchange(environment.getProperty("apiurl")+"api/cart/delete/all?shopId="+this.getPrincipal(), HttpMethod.POST, request, Map.class);
		return new ResponseEntity<Map<String,Object>>(response.getBody(), response.getStatusCode());
	}
	
	
	
	public String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}
}
