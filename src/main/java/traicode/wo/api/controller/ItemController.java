package traicode.wo.api.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/public/item")
public class ItemController {


	@Autowired
	private RestTemplate  restTemplate;
	
	@Autowired
	private HttpHeaders header;
	

	@Autowired
	private Environment environment;
	
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/list/{row}/{page}",method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> itmeList(HttpServletRequest req,@PathVariable("row") int row,@PathVariable("page") int page,@RequestParam("search") String search){	
		HttpEntity<Object> request = new HttpEntity<Object>(header);	
		ResponseEntity<Map> response = restTemplate.exchange(environment.getProperty("apiurl")+"api/item/list/"+row+"/"+page+"?search="+search, HttpMethod.GET, request, Map.class);
		return new ResponseEntity<Map<String,Object>>(response.getBody(), response.getStatusCode());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/home/list",method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> homeList(HttpServletRequest req){	
		HttpEntity<Object> request = new HttpEntity<Object>(header);	
		ResponseEntity<Map> response = restTemplate.exchange(environment.getProperty("apiurl")+"api/item/home/list", HttpMethod.GET, request, Map.class);
		return new ResponseEntity<Map<String,Object>>(response.getBody(), response.getStatusCode());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/brand/list/{row}/{page}",method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> brands(HttpServletRequest req,@PathVariable("row") int row,@PathVariable("page") int page,@RequestParam("brand") String brand){
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		map.add("brand", brand);
		HttpEntity<Object> request = new HttpEntity<Object>(map,header);	
		ResponseEntity<Map> response = restTemplate.exchange(environment.getProperty("apiurl")+"api/item/brand/list/"+row+"/"+page, HttpMethod.POST, request, Map.class);
		return new ResponseEntity<Map<String,Object>>(response.getBody(), response.getStatusCode());
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/get/id/{itemid}/{model}",method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getIteId(HttpServletRequest req,@PathVariable("itemid") String itemid,@PathVariable("model") String model){
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		map.add("itemid", itemid);
		map.add("brand", model);
		HttpEntity<Object> request = new HttpEntity<Object>(map,header);	
		ResponseEntity<Map> response = restTemplate.exchange(environment.getProperty("apiurl")+"api/item/get/detail", HttpMethod.POST, request, Map.class);
		return new ResponseEntity<Map<String,Object>>(response.getBody(), response.getStatusCode());
	}
	
	
}
