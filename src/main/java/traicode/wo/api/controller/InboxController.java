package traicode.wo.api.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import traicode.wo.model.Inbox;
import traicode.wo.model.SendMessage;
import traicode.wo.model.UserLogin;
import traicode.wo.utilities.ResponseData;

@RestController
@RequestMapping("/api/inbox")
public class InboxController {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${apiurl}")
	private String apiUrl;

	@Autowired
	private HttpHeaders headers;

	@Autowired
	private UserLogin userLogin;
	
	@GetMapping("/list")
	public ResponseEntity<ResponseData> getInboxByshop( @RequestParam("page") int page,	@RequestParam("numRow") int numRow, @RequestParam("search") String search,HttpSession session) {
		userLogin = (UserLogin) session.getAttribute("userLogin");
		ResponseEntity<ResponseData> response = restTemplate.exchange(apiUrl + "/api/message/list?shopId=" + userLogin.getUserId() + "&pageNum=" + page + "&numRow=" + numRow+ "&search=" + search, HttpMethod.GET, new HttpEntity<String>(headers), ResponseData.class);
		return new ResponseEntity<ResponseData>(response.getBody(),response.getStatusCode());
	}
	
	@GetMapping("/get")
	public ResponseEntity<ResponseData> getMessageOfShopByItem(@RequestParam("item_syscode") String item_syscode, HttpSession session) {
		userLogin = (UserLogin) session.getAttribute("userLogin");
		ResponseEntity<ResponseData> response = restTemplate.exchange(apiUrl + "/api/message/get?shopId=" + userLogin.getUserId() +"&item_syscode="+item_syscode,HttpMethod.GET, new HttpEntity<String>(headers), ResponseData.class);
		return  new ResponseEntity<ResponseData>(response.getBody(),response.getStatusCode());
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostMapping("/add")
	public ResponseEntity<Map<String, Object>> add(@RequestBody SendMessage sendmessage, HttpSession session) {
		sendmessage.setShopId(Integer.valueOf(this.getPrincipal().toString()));
		HttpEntity<Object> request = new HttpEntity<Object>(sendmessage,headers);	
		ResponseEntity<Map> response = restTemplate.exchange(apiUrl + "/api/message/add",HttpMethod.POST, request,  Map.class);
		return new ResponseEntity<Map<String,Object>>(response.getBody(), response.getStatusCode());
	}
	
	@GetMapping("/reply")
	public ResponseEntity<ResponseData> reply(@RequestBody Inbox item_syscode, HttpSession session) {
		item_syscode.setShopId(Integer.valueOf(this.getPrincipal().toString()));
		HttpEntity<Object> request = new HttpEntity<Object>(item_syscode,headers);	
		ResponseEntity<ResponseData> response = restTemplate.exchange(apiUrl + "/api/message/reply",HttpMethod.POST, request, ResponseData.class);
		return  new ResponseEntity<ResponseData>(response.getBody(),response.getStatusCode());
	}
	
	public String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}
}
