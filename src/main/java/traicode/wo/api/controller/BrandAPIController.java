package traicode.wo.api.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
@RequestMapping("/public/brand")
public class BrandAPIController {
	
	
	@Autowired
	private RestTemplate  restTemplate;
	
	@Autowired
	private HttpHeaders header;
	

	@Autowired
	private Environment environment;
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/list",method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getDepartmentList(HttpServletRequest req){	
		HttpEntity<Object> request = new HttpEntity<Object>(header);	
		ResponseEntity<Map> response = restTemplate.exchange(environment.getProperty("apiurl")+"api/brand/list", HttpMethod.GET, request, Map.class);
		return new ResponseEntity<Map<String,Object>>(response.getBody(), response.getStatusCode());
	}
}
