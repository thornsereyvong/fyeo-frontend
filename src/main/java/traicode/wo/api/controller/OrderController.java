package traicode.wo.api.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import traicode.wo.model.Order;

@RestController
@RequestMapping("/api/order")
public class OrderController {

	@Autowired
	private RestTemplate  restTemplate;
	
	@Autowired
	private HttpHeaders header;
	

	@Autowired
	private Environment environment;
	
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/list/{row}/{page}",method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> orderList(HttpServletRequest req,@PathVariable("row") int row,@PathVariable("page") int page,@RequestParam("search") String search){	
		HttpEntity<Object> request = new HttpEntity<Object>(header);	
		ResponseEntity<Map> response = restTemplate.exchange(environment.getProperty("apiurl")+"api/order/list/"+row+"/"+page+"/"+this.getPrincipal()+"?search="+search, HttpMethod.GET, request, Map.class);
		return new ResponseEntity<Map<String,Object>>(response.getBody(), response.getStatusCode());
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/add",method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> add(@RequestBody Order order,HttpServletRequest req){	
		order.setShopId(this.getPrincipal());
		HttpEntity<Object> request = new HttpEntity<Object>(order,header);	
		ResponseEntity<Map> response = restTemplate.exchange(environment.getProperty("apiurl")+"api/order/add", HttpMethod.POST, request, Map.class);
		return new ResponseEntity<Map<String,Object>>(response.getBody(), response.getStatusCode());
	}
	
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/get/{orderId}",method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> get(@PathVariable("orderId") String orderId,HttpServletRequest req){	
		HttpEntity<Object> request = new HttpEntity<Object>(header);	
		ResponseEntity<Map> response = restTemplate.exchange(environment.getProperty("apiurl")+"api/order/get/"+orderId, HttpMethod.GET, request, Map.class);
		return new ResponseEntity<Map<String,Object>>(response.getBody(), response.getStatusCode());
	}
	
	
	public String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}

}
