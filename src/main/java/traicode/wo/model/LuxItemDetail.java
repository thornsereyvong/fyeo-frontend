package traicode.wo.model;

import java.util.List;

public class LuxItemDetail {
	private String model, image, bigImage, framColor, frontColor, lenMaterail, lenProperty, lenColor, infoNote, lenTrans;
	private String priceHK, priceRetail;
	private String shortOption;
	private double hkPrice, retailPrice;
	private double dollarPrice, dollarRetailPrice;
	private List<LuxItemStock> stocks;
	private String[] shortBy;
	private String description;
	private int adv, polarized, rxable, lEdition;
	
	
	public double getDollarPrice() {
		return dollarPrice;
	}
	public void setDollarPrice(double dollarPrice) {
		this.dollarPrice = dollarPrice;
	}
	public double getDollarRetailPrice() {
		return dollarRetailPrice;
	}
	public void setDollarRetailPrice(double dollarRetailPrice) {
		this.dollarRetailPrice = dollarRetailPrice;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getAdv() {
		return adv;
	}
	public void setAdv(int adv) {
		this.adv = adv;
	}
	public int getPolarized() {
		return polarized;
	}
	public void setPolarized(int polarized) {
		this.polarized = polarized;
	}
	public int getRxable() {
		return rxable;
	}
	public void setRxable(int rxable) {
		this.rxable = rxable;
	}
	public int getlEdition() {
		return lEdition;
	}
	public void setlEdition(int lEdition) {
		this.lEdition = lEdition;
	}
	public String[] getShortBy() {
		return shortBy;
	}
	public void setShortBy(String[] shortBy) {
		this.shortBy = shortBy;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getBigImage() {
		return bigImage;
	}
	public void setBigImage(String bigImage) {
		this.bigImage = bigImage;
	}
	public String getFramColor() {
		return framColor;
	}
	public void setFramColor(String framColor) {
		this.framColor = framColor;
	}
	public String getFrontColor() {
		return frontColor;
	}
	public void setFrontColor(String frontColor) {
		this.frontColor = frontColor;
	}
	public String getLenMaterail() {
		return lenMaterail;
	}
	public void setLenMaterail(String lenMaterail) {
		this.lenMaterail = lenMaterail;
	}
	public String getLenProperty() {
		return lenProperty;
	}
	public void setLenProperty(String lenProperty) {
		this.lenProperty = lenProperty;
	}
	public String getLenColor() {
		return lenColor;
	}
	public void setLenColor(String lenColor) {
		this.lenColor = lenColor;
	}
	public String getInfoNote() {
		return infoNote;
	}
	public void setInfoNote(String infoNote) {
		this.infoNote = infoNote;
	}
	public String getLenTrans() {
		return lenTrans;
	}
	public void setLenTrans(String lenTrans) {
		this.lenTrans = lenTrans;
	}
	public String getPriceHK() {
		return priceHK;
	}
	public void setPriceHK(String priceHK) {
		this.priceHK = priceHK;
	}
	public String getPriceRetail() {
		return priceRetail;
	}
	public void setPriceRetail(String priceRetail) {
		this.priceRetail = priceRetail;
	}
	public String getShortOption() {
		return shortOption;
	}
	public void setShortOption(String shortOption) {
		this.shortOption = shortOption;
	}
	public double getHkPrice() {
		return hkPrice;
	}
	public void setHkPrice(double hkPrice) {
		this.hkPrice = hkPrice;
	}
	public double getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(double retailPrice) {
		this.retailPrice = retailPrice;
	}
	public List<LuxItemStock> getStocks() {
		return stocks;
	}
	public void setStocks(List<LuxItemStock> stocks) {
		this.stocks = stocks;
	}
	@Override
	public String toString() {
		return "LuxItemDetail [model=" + model + ", image=" + image + ", bigImage=" + bigImage + ", framColor="
				+ framColor + ", frontColor=" + frontColor + ", lenMaterail=" + lenMaterail + ", lenProperty="
				+ lenProperty + ", lenColor=" + lenColor + ", infoNote=" + infoNote + ", lenTrans=" + lenTrans
				+ ", priceHK=" + priceHK + ", priceRetail=" + priceRetail + ", shortOption=" + shortOption
				+ ", hkPrice=" + hkPrice + ", retailPrice=" + retailPrice + ", stocks=" + stocks + "]";
	}
	
	
}
