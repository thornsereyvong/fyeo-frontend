package traicode.wo.model;

public class LuxItemStock {
	private String size;
	private int skStatus;
	private int qty;
	private String comment;
	
	

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public int getSkStatus() {
		return skStatus;
	}

	public void setStock(int skStatus) {
		this.skStatus = skStatus;
	}

	public LuxItemStock(String size, int skStatus) {
		super();
		this.size = size;
		this.skStatus = skStatus;
	}

	public LuxItemStock() {
		super();
	}

}
