package traicode.wo.model;

import java.util.Arrays;
import java.util.List;

public class LuxItem {
	private int syscode;
	private String brand,modelCode,modelValue, image, description, itemOption, priceHK, priceRetail, colLeft, colRight;
	private int color, size;
	private double hkPrice, retailPrice;
	private double dollarPrice, dollarRetailPrice;
	private String[] shortBy;
	private List<LuxItemDetail> detail;
	private int adv, polarized, rxable, lEdition;
	
	
	public int getSyscode() {
		return syscode;
	}
	public void setSyscode(int syscode) {
		this.syscode = syscode;
	}
	public double getDollarPrice() {
		return dollarPrice;
	}
	public void setDollarPrice(double dollarPrice) {
		this.dollarPrice = dollarPrice;
	}
	public double getDollarRetailPrice() {
		return dollarRetailPrice;
	}
	public void setDollarRetailPrice(double dollarRetailPrice) {
		this.dollarRetailPrice = dollarRetailPrice;
	}
	public int getAdv() {
		return adv;
	}
	public void setAdv(int adv) {
		this.adv = adv;
	}
	public int getPolarized() {
		return polarized;
	}
	public void setPolarized(int polarized) {
		this.polarized = polarized;
	}
	public int getRxable() {
		return rxable;
	}
	public void setRxable(int rxable) {
		this.rxable = rxable;
	}
	public int getlEdition() {
		return lEdition;
	}
	public void setlEdition(int lEdition) {
		this.lEdition = lEdition;
	}
	public String getColLeft() {
		return colLeft;
	}
	public void setColLeft(String colLeft) {
		this.colLeft = colLeft;
	}
	public String getColRight() {
		return colRight;
	}
	public void setColRight(String colRight) {
		this.colRight = colRight;
	}
	public List<LuxItemDetail> getDetail() {
		return detail;
	}
	public void setDetail(List<LuxItemDetail> detail) {
		this.detail = detail;
	}
	public String getPriceHK() {
		return priceHK;
	}
	public void setPriceHK(String priceHK) {
		this.priceHK = priceHK;
	}
	public String getPriceRetail() {
		return priceRetail;
	}
	public void setPriceRetail(String priceRetail) {
		this.priceRetail = priceRetail;
	}
	public String getItemOption() {
		return itemOption;
	}
	public void setItemOption(String itemOption) {
		this.itemOption = itemOption;
	}
	public String getModelValue() {
		return modelValue;
	}
	public void setModelValue(String modelValue) {
		this.modelValue = modelValue;
	}
	public String getModelCode() {
		return modelCode;
	}
	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getColor() {
		return color;
	}
	public void setColor(int color) {
		this.color = color;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public double getHkPrice() {
		return hkPrice;
	}
	public void setHkPrice(double hkPrice) {
		this.hkPrice = hkPrice;
	}
	public double getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(double retailPrice) {
		this.retailPrice = retailPrice;
	}
	public String[] getShortBy() {
		return shortBy;
	}
	public void setShortBy(String[] shortBy) {
		this.shortBy = shortBy;
	}
	@Override
	public String toString() {
		return "LuxItem [brand=" + brand + ", modelCode=" + modelCode + ", modelValue=" + modelValue + ", image="
				+ image + ", description=" + description + ", itemOption=" + itemOption + ", priceHK=" + priceHK
				+ ", priceRetail=" + priceRetail + ", color=" + color + ", size=" + size + ", hkPrice=" + hkPrice
				+ ", retailPrice=" + retailPrice + ", shortBy=" + Arrays.toString(shortBy) + "]";
	}
	
	
}
