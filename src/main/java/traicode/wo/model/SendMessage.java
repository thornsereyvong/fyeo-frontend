package traicode.wo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SendMessage {

	private String msg;
	private int shopId;
	private int itemId;
	private short isReply;
}
