package traicode.wo.model;


public class Wishlist {
	private int wId;
	private int shopId;
	private String brand, itemId, model;
	private String wDate;
	
	private LuxItemDetail itemDetail;
	
	
	public LuxItemDetail getItemDetail() {
		return itemDetail;
	}
	public void setItemDetail(LuxItemDetail itemDetail) {
		this.itemDetail = itemDetail;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getwId() {
		return wId;
	}
	public void setwId(int wId) {
		this.wId = wId;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getwDate() {
		return wDate;
	}
	public void setwDate(String wDate) {
		this.wDate = wDate;
	}
}
