package traicode.wo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Inbox {

	private int inboxId;
	private String msg;
	private int shopId;
	private int itemId;
	private short isReply;
	private int parentId;
}
