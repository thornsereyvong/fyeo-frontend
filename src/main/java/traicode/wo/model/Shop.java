package traicode.wo.model;

public class Shop {
	private int shopId;
	private String shopName;
	private String address, email,tel1,tel2;
	private int inactive;
	private String username;
	private String password;
	private String ship1,ship2,ship3,ship4,ship5,ship6,ship7,ship8,ship9,ship10;
	private String description;
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTel1() {
		return tel1;
	}
	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}
	public String getTel2() {
		return tel2;
	}
	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}
	public int getInactive() {
		return inactive;
	}
	public void setInactive(int inactive) {
		this.inactive = inactive;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getShip1() {
		return ship1;
	}
	public void setShip1(String ship1) {
		this.ship1 = ship1;
	}
	public String getShip2() {
		return ship2;
	}
	public void setShip2(String ship2) {
		this.ship2 = ship2;
	}
	public String getShip3() {
		return ship3;
	}
	public void setShip3(String ship3) {
		this.ship3 = ship3;
	}
	public String getShip4() {
		return ship4;
	}
	public void setShip4(String ship4) {
		this.ship4 = ship4;
	}
	public String getShip5() {
		return ship5;
	}
	public void setShip5(String ship5) {
		this.ship5 = ship5;
	}
	public String getShip6() {
		return ship6;
	}
	public void setShip6(String ship6) {
		this.ship6 = ship6;
	}
	public String getShip7() {
		return ship7;
	}
	public void setShip7(String ship7) {
		this.ship7 = ship7;
	}
	public String getShip8() {
		return ship8;
	}
	public void setShip8(String ship8) {
		this.ship8 = ship8;
	}
	public String getShip9() {
		return ship9;
	}
	public void setShip9(String ship9) {
		this.ship9 = ship9;
	}
	public String getShip10() {
		return ship10;
	}
	public void setShip10(String ship10) {
		this.ship10 = ship10;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
