package traicode.wo.model;

public class Item {
	private String brand, brandCategory, frameCategory, gender, frameMaterial, shap, activity, image;
	private int inactive = 0;
	private String model,frameColor, frontColor, lensColor, sizeInStock, sizeOutStock, sizeInfactory;
	private double hkPrice, wsPrice, rtPrice;
	private boolean status;
	
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getBrandCategory() {
		return brandCategory;
	}
	public void setBrandCategory(String brandCategory) {
		this.brandCategory = brandCategory;
	}
	public String getFrameCategory() {
		return frameCategory;
	}
	public void setFrameCategory(String frameCategory) {
		this.frameCategory = frameCategory;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getFrameMaterial() {
		return frameMaterial;
	}
	public void setFrameMaterial(String frameMaterial) {
		this.frameMaterial = frameMaterial;
	}
	public String getShap() {
		return shap;
	}
	public void setShap(String shap) {
		this.shap = shap;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getInactive() {
		return inactive;
	}
	public void setInactive(int inactive) {
		this.inactive = inactive;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getFrameColor() {
		return frameColor;
	}
	public void setFrameColor(String frameColor) {
		this.frameColor = frameColor;
	}
	public String getFrontColor() {
		return frontColor;
	}
	public void setFrontColor(String frontColor) {
		this.frontColor = frontColor;
	}
	public String getLensColor() {
		return lensColor;
	}
	public void setLensColor(String lensColor) {
		this.lensColor = lensColor;
	}
	public String getSizeInStock() {
		return sizeInStock;
	}
	public void setSizeInStock(String sizeInStock) {
		this.sizeInStock = sizeInStock;
	}
	public String getSizeOutStock() {
		return sizeOutStock;
	}
	public void setSizeOutStock(String sizeOutStock) {
		this.sizeOutStock = sizeOutStock;
	}
	public String getSizeInfactory() {
		return sizeInfactory;
	}
	public void setSizeInfactory(String sizeInfactory) {
		this.sizeInfactory = sizeInfactory;
	}
	public double getHkPrice() {
		return hkPrice;
	}
	public void setHkPrice(double hkPrice) {
		this.hkPrice = hkPrice;
	}
	public double getWsPrice() {
		return wsPrice;
	}
	public void setWsPrice(double wsPrice) {
		this.wsPrice = wsPrice;
	}
	public double getRtPrice() {
		return rtPrice;
	}
	public void setRtPrice(double rtPrice) {
		this.rtPrice = rtPrice;
	}
}
