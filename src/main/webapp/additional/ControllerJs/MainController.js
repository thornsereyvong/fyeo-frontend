
function formatDateTime(str){
	return moment(str, 'DD-MM-YYYY h:mm A').format('YYYY-MM-DD HH:MM:SS');
}

function formatDate(str){
	return moment(str, 'YYYY-MM-DD').format('DD-MM-YYYY');
}

function formatDateDB(str){
	return moment(str, 'DD-MM-YYYY').format('YYYY-MM-DD');
}

function loadingPage(check){
	if(check){
		$("#circularGCLoading").css("display", "");
	}else{
		$("#circularGCLoading").css("display", "none");
	}
}


function countObj(ob){
	return Object.keys(ob).length;
}

function getVal(id){
	return $("#"+id).val();
}

function setVal(id,val){
	return $("#"+id).val(val);
}

function dis(data,id) {
	console.log(data);
	$("#"+id).append(JSON.stringify(data));
}

function messagsTypeError(textMess){
	swal({
		  title: "WARNING",
		  text: textMess,
		  timer: 3000,
		  type: "warning",
	});
}

function messagsTypeClose(){
	swal({
		  timer: 1
	});
}

function messagsTypeSuccess(textMess){
	swal({
		  title: "SUCCESSFUL",
		  text: textMess,
		  timer: 3000,
		  type: "success",
	});
}

function messageTypeFail(textMess){
	swal({
		  title: "FAILED",
		  text: textMess,
		  timer: 3000,
		  type: "warning",

	});
}

function messageTypeUsed(textMess){
	swal({
		  title: "USED",
		  text: textMess,
		  timer: 3000,
		  type: "warning",

	});
}

function messageTypeExisted(textMess){
	swal({
		  title: "EXISTED",
		  text: textMess,
		  timer: 3000,
		  type: "warning",

	});
}

function messageTypeInfo(textMess){
	swal({
		  title: "Information",
		  text: textMess,
		  timer: 3000,
		  type: "info"
	});
}

function messageTypeNotAllowed(textMess){
	swal({
		  title: "NOT ALLOWED",
		  text: textMess,
		  timer: 3000,
		  type: "warning",
	
	});
}
function messageTypeNotAuthorization(textMess){
	swal({
		  title: "NO AUTHORIZATION",
		  text: textMess,
		  timer: 3000,
		  type: "warning",
	
	});
}


var app = angular.module('GlassApp', ['angularUtils.directives.dirPagination','angular-loading-bar', 'ngAnimate','ngFileUpload', 'angularLazyImg','ngSanitize']).config(['cfpLoadingBarProvider','lazyImgConfigProvider', function(cfpLoadingBarProvider,lazyImgConfigProvider) {
    cfpLoadingBarProvider.includeSpinner = true;
    cfpLoadingBarProvider.includeBar = true;
   /* cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
    cfpLoadingBarProvider.spinnerTemplate = '<div><span class="fa fa-spinner">Custom Loading Message...</div>';*/
    /*var scrollable = document.querySelector('#scrollable');
	  lazyImgConfigProvider.setOptions({
	    offset: 100,
	    errorClass: 'error', 
	    successClass: 'success',
	    onError: function(image){},
	    onSuccess: function(image){},
	    container: angular.element(scrollable) 
	  });
	  */
	  
}]);


var self = this;
app.filter('checkIsNull', function () {
	  return function (item) {
	     if(item != "" || item != null){
			return item;
		 }else{
			return "-";
		 }
	  };
});



