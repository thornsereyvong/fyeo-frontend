'use strict';

app.controller('WishListController',['$scope','$http','Upload','$timeout',function($scope, $http, Upload, $timeout){
	$scope.search = "";
	


	  $scope.totalItem = 0;
	  $scope.pageSize = {};
	  $scope.pageSize.rows = [ 
				{ value: 10, label: "10 " },
				{ value: 20, label: "20 " },
	      		{ value: 30, label: "30 " },
	      		{ value: 40, label: "40 " }
	      		];
	  $scope.pageSize.row = $scope.pageSize.rows[1].value;
	  
	  $scope.pagination = {
		      current: 1
	  };
	  
	  
	  
	$scope.getListItem = function(page,row){
		$scope.listItem = [];
				
		$http({
 			method: 'POST',
		    url: baseUrl+"api/wishlist/list/"+page+"/"+row,
		    headers: {
	    		'Accept': 'application/json',
	    		'Content-Type': 'application/json'
		    },
		    params:{"search": $scope.search},
		    ignoreLoadingBar: true
		}).success(function(response) {
			dis(response);
			if(response.MESSAGE == 'SUCCESS'){
				$scope.listItem = response.RECORD.RECORDS;
				$scope.totalItem = response.RECORD.totalRecords;
			}
		});	
	}
	
	$scope.searchOfCode = function(){
		$scope.getListItem($scope.pagination.current,$scope.pageSize.row);
	}
	
	
	$scope.getListItem($scope.pagination.current,$scope.pageSize.row);
	
	$scope.deleteWish = function(id){
		swal({
			  title: "You are about to delete this wishlist.",
			  text: "Click OK to Delete or Cancel",
			  type: "info",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "OK",
			  cancelButtonText: "No, cancel",
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm) {
			  if (isConfirm) {
				  $http({
			 			method: 'POST',
					    url: baseUrl+"api/wishlist/delete",
					    headers: {
				    		'Accept': 'application/json',
				    		'Content-Type': 'application/json'
					    },
					    data:{"wId": id},
					    ignoreLoadingBar: true
					}).success(function(response) {
						if(response.MESSAGE == 'SUCCESS'){
							messagsTypeSuccess(response.MESSAGE);
							$scope.getListItem($scope.pagination.current,$scope.pageSize.row);
							angular.element(document.getElementById('idCountheader')).scope().getList();
						}
					});	
			  } 
			});
		
	
	}
	
	
	$scope.deleteWishAll = function(){
		swal({
			  title: "You are about to empty wishlist.",
			  text: "Click OK to Delete or Cancel",
			  type: "info",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "OK",
			  cancelButtonText: "No, cancel",
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm) {
			  if (isConfirm) {
				  $http({
			 			method: 'POST',
					    url: baseUrl+"api/wishlist/delete/all",
					    headers: {
				    		'Accept': 'application/json',
				    		'Content-Type': 'application/json'
					    },
					    ignoreLoadingBar: true
					}).success(function(response) {
						if(response.MESSAGE == 'SUCCESS'){
							messagsTypeSuccess(response.MESSAGE);
							$scope.getListItem($scope.pagination.current,$scope.pageSize.row);
							angular.element(document.getElementById('idCountheader')).scope().getList();
						}
					});	
			  } 
			});
		
	
	}
	
	
	$scope.cartAdd = function(itemID,brand,model,size,qty,unitprice,comment){
		loadingPage(true);
		var amt = unitprice * qty;
		$http({
 			method: 'POST',
		    url: baseUrl+"api/cart/add",
		    headers: {
		    	'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    },
		    data:{"itemId":itemID,"brand":brand,"model":model,"size":size,"qty":qty,"unitprice":unitprice,"comment":comment,"amount":amt},
		    ignoreLoadingBar: true
		}).success(function(response) {
			if(response.MESSAGE == 'SUCCESS'){
				
				for(var i=0;i<$scope.listItem.length;i++){
					for(var y=0;y<$scope.listItem[i].itemDetail.stocks.length;y++){
						if($scope.listItem[i].itemDetail.model == model){
							if($scope.listItem[i].itemDetail.stocks[y].size == size){
								if($scope.listItem[i].itemDetail.stocks[y].comment == comment){
									$scope.listItem[i].itemDetail.stocks[y].qty = 0;
									$scope.listItem[i].itemDetail.stocks[y].comment = "";
								}
							}
						}
					}
				}
				
				angular.element(document.getElementById('idCountheader')).scope().getList();
			}
			loadingPage(false);
		});	
	}
	
	$scope.changeNum = function(val){
		if(isNaN(val.qty)){
			val.qty = 0;
		}
	}
	
	
	$scope.cartAddList = function(){
		var dataValue = [];
		
		
		for(var i=0;i<$scope.listItem.length;i++){
			for(var y=0;y<$scope.listItem[i].itemDetail.stocks.length;y++){
				if($scope.listItem[i].itemDetail.stocks[y].qty != 0){
					var amt = $scope.listItem[i].itemDetail.stocks[y].qty * $scope.listItem[i].itemDetail.dollarPrice;
					dataValue.push({"itemId": $scope.listItem[i].itemId,"brand": $scope.listItem[i].brand,"model": $scope.listItem[i].model,"size": $scope.listItem[i].itemDetail.stocks[y].size,"qty":$scope.listItem[i].itemDetail.stocks[y].qty,"unitprice":$scope.listItem[i].itemDetail.dollarPrice,"comment":$scope.listItem[i].itemDetail.stocks[y].comment,"amount":amt});
				}
			}
		}
		
		if(dataValue.length == 0){
			messagsTypeError("Please input qty.");
		}else{
			
			loadingPage(true);
			$http({
	 			method: 'POST',
			    url: baseUrl+"api/cart/multi/add",
			    headers: {
			    	'Accept': 'application/json',
			        'Content-Type': 'application/json'
			    },
			    data:dataValue,
			    ignoreLoadingBar: true
			}).success(function(response) {
				if(response.MESSAGE == 'SUCCESS'){
					
					for(var i=0;i<$scope.listItem.length;i++){
						for(var y=0;y<$scope.listItem[i].itemDetail.stocks.length;y++){
							if($scope.listItem[i].itemDetail.stocks[y].qty != 0){
								$scope.listItem[i].itemDetail.stocks[y].qty = 0;
								$scope.listItem[i].itemDetail.stocks[y].comment = "";
								
							}
						}
					}
					
					angular.element(document.getElementById('idCountheader')).scope().getList();
				}
				loadingPage(false);
			});	
			
		}
		
	}
	
	
	
	
	

	 $scope.pageChanged = function(newPage) {
		  $scope.getListItem(newPage, $scope.pageSize.row);
	  };
	  
	  $scope.changeRowPage = function(){
		  $scope.getListItem($scope.pagination.current, $scope.pageSize.row);
	  }
	  
	
	
}]);



