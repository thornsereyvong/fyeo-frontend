'use strict';

app.controller('inboxController',['$scope','$http','$timeout',function($scope, $http, $timeout){

	$scope.listItemInbox = [];
	$scope.findMessageById = function(sys_code) {
		$http({
			method:'GET',
			url:baseUrl+"api/inbox/get",
			 headers: {
		    		'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    },
		    params:{
		    	"item_syscode":sys_code
		    }
		}).success(function(res) {
			if(res.status == 'SUCCESS'){
				$scope.listItemInbox =  (res.data);
				angular.forEach($scope.listItemInbox, function(so) {
					if(so.inboxReply != ""){
						so.inboxReply = JSON.parse(so.inboxReply);
					}
				});
			}
		});
	}
	
	
	
	
	
	
	
	
	
	 $scope.search = "";

	  $scope.totalItem = 0;
	  $scope.pageSize = {};
	  $scope.pageSize.rows = [ 
				{ value: 10, label: "10 " },
				{ value: 20, label: "20 " },
	      		{ value: 30, label: "30 " },
	      		{ value: 40, label: "40 " }
	      		];
	  $scope.pageSize.row = $scope.pageSize.rows[1].value;
	  
	  $scope.pagination = {
		      current: 1
	  };
	  
	  
	  
	$scope.getListItem = function(page,row){
		$scope.listItem = [];
				
		$http({
			method:'GET',
			url:baseUrl+"api/inbox/list",
			 headers: {
		    		'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    },
			params:{
				"page":page || 1,
				"numRow":row,
				"search":$scope.search || ''
			}
		}).success(function(res) {
			if(res.status == 'SUCCESS') {
				$scope.inboxs = res.data.inboxs;
			}
		});
	}
	
	$scope.searchOfCode = function(){
		$scope.getListItem($scope.pagination.current,$scope.pageSize.row);
	}
	
	$scope.getListItem($scope.pagination.current,$scope.pageSize.row);
	

	 $scope.pageChanged = function(newPage) {
		  $scope.getListItem(newPage, $scope.pageSize.row);
	  };
	  
	  $scope.changeRowPage = function(){
		  $scope.getListItem($scope.pagination.current, $scope.pageSize.row);
	  }
	  
	  $scope.AddInboxItem = function(msg){
			$http({
	 			method: 'POST',
			    url: baseUrl+"api/inbox/add",
			    headers: {
			    	'Accept': 'application/json',
			        'Content-Type': 'application/json'
			    },
			    data:{"msg":msg,"itemId":$scope.listItemInbox[0].itemId,"isReply":0,"shopId":0},
			    ignoreLoadingBar: true
			}).success(function(response) {
				if(response.status == 'SUCCESS'){
					$scope.findMessageById($scope.listItemInbox[0].itemId);
					$("#message").val("");
				}
			});
		}
	  
}]);