'use strict';

app.controller('IndexController',['$scope','$http','Upload','$timeout',function($scope, $http, Upload, $timeout){
	$scope.bestSellersControl = {};
	
	$scope.brandId = 'Miu Miu';
	$scope.getListItem = function(){
		$scope.listItem = [];
		$scope.listBrands = [];
		
		$http({
 			method: 'GET',
		    url: baseUrl+"public/item/home/list",
		    headers: {
		    	'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    },
		    ignoreLoadingBar: true
		}).success(function(response) {
			dis(response);
			if(response.MESSAGE == 'SUCCESS'){
				$scope.listItem = response.RECORD.RECORDS;
				$scope.listBrands = response.BRANDS;
				$scope.getListItemBrand($scope.brandId);
			}
		});	
	}
	
	
	
	$scope.getListItemBrand = function(brandId){
		$scope.listItemBrands = [];
		$scope.subListBrands = [];
		$scope.subListBrands.push([]);
		$scope.subListBrands.push([]);
		$scope.subListBrands.push([]);
		$scope.subListBrands.push([]);
		$scope.subListBrands.push([]);
		$scope.subListBrands.push([]);
		
		$http({
 			method: 'GET',
		    url: baseUrl+"public/item/brand/list/10/1",
		    headers: {
		    	'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    },
		    params:{"brand":brandId},
		    ignoreLoadingBar: true
		}).success(function(response) {
			dis(response);
			if(response.MESSAGE == 'SUCCESS'){
				$scope.listItemBrands = response.RECORDS;
				var y = 1;
				//alert($scope.listItemBrands.length);
				//for(var x=0;x<$scope.subListBrands.length;x++){
					/*for(var i=0;i<$scope.listItemBrands.length;i++){
						if(i == 0 || i == 1){
							 $scope.subListBrands[0] = $scope.subListBrands[0].concat(angular.copy($scope.listItemBrands[i]));
						}else if(i == 2 || i == 3){
							 $scope.subListBrands[1] = $scope.subListBrands[1].concat(angular.copy($scope.listItemBrands[i]));
						}else if(i == 4 || i == 5){
							 $scope.subListBrands[2] = $scope.subListBrands[2].concat(angular.copy($scope.listItemBrands[i]));
						}else if(i == 6 || i == 7){
							 $scope.subListBrands[3] = $scope.subListBrands[3].concat(angular.copy($scope.listItemBrands[i]));
						}else if(i == 8 || i == 9){
							 $scope.subListBrands[4] = $scope.subListBrands[4].concat(angular.copy($scope.listItemBrands[i]));
						}else if(i == 10 || i == 11){
							 $scope.subListBrands[5] = $scope.subListBrands[5].concat(angular.copy($scope.listItemBrands[i]));
						}
					
					}*/
				//}
				
				
				 setTimeout(function(){
	 				this.AT_Slider.init();
	 			},100);	
			}
		});	
	}
	
	
	$scope.getListItem();
	
	
	$scope.changeBestSeller = function(id){
		$scope.getListItemBrand(id);
	}
	
	
	
	$scope.getListItemDetail = function(itemId,model){
		$scope.listItemDetail = [];
		$http({
 			method: 'GET',
		    url: baseUrl+"public/item/get/id/"+itemId+"/"+model,
		    headers: {
		    		'Accept': 'application/json',
		    		'Content-Type': 'application/json'
		    },
		    ignoreLoadingBar: true
		}).success(function(response) {
			dis(response);
			if(response.MESSAGE == 'FOUND'){
				$scope.listItemDetail = response.RECORD;
				
			}
		});	
	}
	
	
}]);






/*jQuery(document).ready(function($) {
	setTimeout(function(){
		AT_Slider.init();
	},1000);
	
})*/

/*
app.directive('productListing',['$http',function($http) {
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        templateUrl: 'TopSelling.html',
        controller: function($scope, $timeout) {
            $timeout( function() {
            	
            },500);
        },
        link: function(scope, elm, attr) {
	         $http.get(baseUrl+"public/item/home/list").success(function(response){
	        	 if(response.MESSAGE == 'SUCCESS'){
	        		 scope.listItem = response.RECORD.RECORDS;
	 				 scope.listBrands = response.BRANDS;
	 				 setTimeout(function(){
	 					this.AT_Slider.init();
	 				},500);	
	 			 }
	         });
        }
    }
}]);
*/

/*app.directive('bestSellers',['$http',function($http) {
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        templateUrl: 'bestSellersTmp.html',
        controller: function($scope, $timeout) {
            $timeout( function() {
            	 alert();
            },500);
        },
        link: function(scope, elm, attr) {
		 $http.get(baseUrl+"public/item/brand/list/12/1?search="+id).success(function(response){
        	 if(response.MESSAGE == 'SUCCESS'){
        		 scope.listItemBrands = response.RECORDS;
        		
 				 setTimeout(function(){
 					this.AT_Slider.init();
 				},500);	
 			 }
         });
        }
    }
}]);*/

/*function IndexController($scope){

}
*/


