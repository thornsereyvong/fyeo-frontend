'use strict';

app.controller('ProductDetaController',['$scope','$http','Upload','$timeout',function($scope, $http, Upload, $timeout){
	
	$scope.imageZoom = "";
	
	$scope.getListItem = function(itemId,model){
		$scope.listItem = [];
				
		$http({
 			method: 'GET',
		    url: baseUrl+"public/item/get/id/"+itemId+"/"+model,
		    headers: {
		    		'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    },
		    ignoreLoadingBar: true
		}).success(function(response) {
			if(response.MESSAGE == 'FOUND'){
				$scope.listItem = response.RECORD;
				$("#lef").append($scope.listItem.colLeft);
				$("#rig").append($scope.listItem.colRight);
				$scope.listInboxItem($scope.listItem.syscode);
			}
		});	
	}
	
	
	$scope.showImage = function(image){
		$scope.imageZoom = image;
		 $('#quick-shop-popup').on( 'shown.bs.modal', function () {    
		      $('#quick-shop-popup', function() {
		        var zoomImage = $('.zoom-qs .zoom-image');
		        zoomImage.elevateZoom({
		          gallery:'gallery-qs-image',
		          galleryActiveClass: 'active',
		          zoomType: 'lens',
		          cursor: 'pointer',
		          lensShape: "round",
				  lensSize: 300
		        });
		      });
		    //  AT_Main.handleReviews();
		    }); 
		
	}
	
	$scope.listInboxItem = function(){
		$scope.listItemInbox = [];
		$http({
 			method: 'GET',
		    url: baseUrl+"api/inbox/get",
		    headers: {
		    	'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    },
		    params:{"item_syscode":$scope.listItem.syscode},
		    ignoreLoadingBar: true
		}).success(function(response) {
			dis(response);
			if(response.status == 'SUCCESS'){
				$scope.listItemInbox =  (response.data);
				angular.forEach($scope.listItemInbox, function(so) {
					if(so.inboxReply != ""){
						so.inboxReply = JSON.parse(so.inboxReply);
					}
				});
			}
		});
	}
	
	
	$scope.AddInboxItem = function(msg){
		$http({
 			method: 'POST',
		    url: baseUrl+"api/inbox/add",
		    headers: {
		    	'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    },
		    data:{"msg":msg,"itemId":$scope.listItem.syscode,"isReply":0,"shopId":0},
		    ignoreLoadingBar: true
		}).success(function(response) {
			if(response.status == 'SUCCESS'){
				$scope.listInboxItem();
				$("#message").val("");
			}
		});
	}
	
	
	$scope.cartAdd = function(itemID,brand,model,size,qty,unitprice,comment){
		loadingPage(true);
		var amt = unitprice * qty;
		$http({
 			method: 'POST',
		    url: baseUrl+"api/cart/add",
		    headers: {
		    	'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    },
		    data:{"itemId":itemID,"brand":brand,"model":model,"size":size,"qty":qty,"unitprice":unitprice,"comment":comment,"amount":amt},
		    ignoreLoadingBar: true
		}).success(function(response) {
			if(response.MESSAGE == 'SUCCESS'){
				
				for(var i=0;i<$scope.listItem.detail.length;i++){
					for(var y=0;y<$scope.listItem.detail[i].stocks.length;y++){
						if($scope.listItem.detail[i].model == model){
							if($scope.listItem.detail[i].stocks[y].size == size){
								if($scope.listItem.detail[i].stocks[y].comment == comment){
									$scope.listItem.detail[i].stocks[y].qty = 0;
									$scope.listItem.detail[i].stocks[y].comment = "";
								}
							}
						}
					}
				}
				
				angular.element(document.getElementById('idCountheader')).scope().getList();
			}
			loadingPage(false);
		});	
	}
	
	$scope.changeNum = function(val){
		if(isNaN(val.qty)){
			val.qty = 0;
		}
	}
	
	
	$scope.cartAddList = function(itemId,brand){
		var dataValue = [];
		
		for(var i=0;i<$scope.listItem.detail.length;i++){
			for(var y=0;y<$scope.listItem.detail[i].stocks.length;y++){
				if($scope.listItem.detail[i].stocks[y].qty != 0){
					var amt = $scope.listItem.detail[i].stocks[y].qty * $scope.listItem.detail[i].dollarPrice;
					dataValue.push({"itemId":itemId,"brand":brand,"model":$scope.listItem.detail[i].model,"size":$scope.listItem.detail[i].stocks[y].size,"qty":$scope.listItem.detail[i].stocks[y].qty,"unitprice":$scope.listItem.detail[i].dollarPrice,"comment":$scope.listItem.detail[i].stocks[y].comment,"amount":amt});
				}
			}
		}
		if(dataValue.length == 0){
			messagsTypeError("Please input qty.");
		}else{
			
			loadingPage(true);
			$http({
	 			method: 'POST',
			    url: baseUrl+"api/cart/multi/add",
			    headers: {
			    	'Accept': 'application/json',
			        'Content-Type': 'application/json'
			    },
			    data:dataValue,
			    ignoreLoadingBar: true
			}).success(function(response) {
				if(response.MESSAGE == 'SUCCESS'){
					
					for(var i=0;i<$scope.listItem.detail.length;i++){
						for(var y=0;y<$scope.listItem.detail[i].stocks.length;y++){
							if($scope.listItem.detail[i].qty != 0){
								$scope.listItem.detail[i].stocks[y].qty = 0;
								$scope.listItem.detail[i].stocks[y].comment = "";
							}
						}
					}
					
					angular.element(document.getElementById('idCountheader')).scope().getList();
				}
				loadingPage(false);
			});	
			
		}
		
	}
	
	
	$scope.wishListAdd = function(itemID,brand,model){
		loadingPage(true);
		$http({
 			method: 'POST',
		    url: baseUrl+"api/wishlist/add",
		    headers: {
		    	'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    },
		    data:{"itemId":itemID,"brand":brand,"model":model},
		    ignoreLoadingBar: true
		}).success(function(response) {
			angular.element(document.getElementById('idCountheader')).scope().getList();
			loadingPage(false);
		});	
	}
	
	

}]);



