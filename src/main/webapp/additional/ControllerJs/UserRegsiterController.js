app.controller('RegisterController',['$scope','$http','Upload','$timeout',function($scope, $http, Upload, $timeout){

	
	$scope.forProductNormal = true;
	$scope.productReady = true;
	$scope.getProductFromCompany = true;
	
	$scope.forProductBrand = false;
	$scope.productReadyContact = false;
	$scope.needComapnyDelivery = false;
	
	
	$scope.checkNormal = function(){
	
		if($scope.forProductNormal == true){
			$scope.forProductBrand = false;
			$("#forProductBrand").val(false);
		}
		$scope.forProductNormal = true;
	}
	
	$scope.checkBrandName = function(){
		if($scope.forProductBrand == true){
			$scope.forProductNormal = false;
			$("#forProductNormal").val(false);
		}
		$scope.forProductBrand = true;
	}
	
	$scope.checkProductReady = function(){
		if($scope.productReady == true){
			$scope.productReadyContact = false;
			$("#productReadyContact").val(false);
		}
		$scope.productReady = true;
	}
	
	$scope.checkProductReadyContact = function(){
		if($scope.productReadyContact == true){
			$scope.productReady = false;
			$("#productReady").val(false);
		}
		$scope.productReadyContact = true;
	}
	
	
	$scope.checkProductCom = function(){
		if($scope.getProductFromCompany == true){
			$scope.needComapnyDelivery = false;
			$("#needComapnyDelivery").val(false);
		}
		$scope.getProductFromCompany = true;
	}
	
	$scope.checkProductDe = function(){
		if($scope.needComapnyDelivery == true){
			$scope.getProductFromCompany = false;
			$("#getProductFromCompany").val(false);
		}
		$scope.needComapnyDelivery = true;
	}

	$scope.profileName  = "";
	$scope.uploadFiles = function(file, errFiles) {
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: baseUrl+'public/upload/customer/profile',
                data: {file: file,_csrf: $("#_csrf").val()}
            });
            file.upload.then(function (response) {
                if(response.data.status == "success"){
                	$('#profileCustomer').attr('src', response.data.data.path+response.data.data.fileName);                	
                	$scope.profileName = response.data.data.fileName;
                }
                
            });
        }   
    }
	
	$scope.profileID  = "";
	$scope.uploadFilesProfileID = function(file, errFiles) {
        $scope.errFile = errFiles && errFiles[0];
        
        if (file) {
            file.upload = Upload.upload({
                url: baseUrl+'public/upload/customer/profile',
                data: {file: file,_csrf: $("#_csrf").val()}
            });
            file.upload.then(function (response) {
                if(response.data.status == "success"){
                	$('#profileID').attr('src',response.data.data.path+response.data.data.fileName);              	
                	$scope.profileID = response.data.data.fileName;
                }
                
            });
        }   
    }
	
	
	$scope.profileRece1 = "";
	$scope.uploadFilesReceived1 = function(file, errFiles) {
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: baseUrl+'public/upload/customer/profile',
                data: {file: file,_csrf: $("#_csrf").val()}
            });
            file.upload.then(function (response) {
                if(response.data.status == "success"){
                	$('#profileReceived1').attr('src',response.data.data.path+response.data.data.fileName);             	
                	$scope.profileRece1 = response.data.data.fileName;
                	
                }
                
            });
        }   
    }
	
	
	$scope.getlistemployee = function(){
		$scope.listEmp = [];
		$http({
 			method: 'GET',
		    url: baseUrl+"public/employee/list/data",
		    headers: {
		    	'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    },
		    ignoreLoadingBar: true
		}).success(function(response) {
			if(response.status == 'success'){
				$scope.listEmp = response.data;
			}
		});	
	}
	
	$scope.getlistemployee();
	
	$scope.profileRece2 = "";
	$scope.uploadFilesReceived2 = function(file, errFiles) {
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: baseUrl+'public/upload/customer/profile',
                data: {file: file,_csrf: $("#_csrf").val()}
            });
            file.upload.then(function (response) {
                if(response.data.status == "success"){
                	$('#profileReceived2').attr('src', response.data.data.path+response.data.data.fileName);             	
                	$scope.profileRece2 = response.data.data.fileName;
                }
                
            });
        }   
    }
	
	
	$scope.setShipAddressCount = 0;
	$scope.setShipAddress = [{"shipID":"" ,"shipaddress":""}];
	$scope.setShipAddressCount = $scope.setShipAddress.length;
	
	$scope.addShipAddress = function(){
		$scope.setShipAddressCount = $scope.setShipAddressCount + 1;
		$scope.setShipAddress.push({"shipID":"" ,"shipaddress":"",});
	}

	$scope.removeShipAddress = function($index){
		swal({
			title:  "<span style='font-size: 20px;'>You are about to remove ship address . </span>",
			text: "Click OK to continue or CANCEL to abort.",
			type: "info",
			html: true,
			showCancelButton: true,
			closeOnConfirm: false,
			showLoaderOnConfirm: true,	
        }, 
        function(ok){ 
       
        	if(ok){
        		if($scope.setShipAddress.length <= 1){
        			messagsTypeError("Can not remove ship address. !");
        		}else{
        		  //$scope.setShipAddress.shift();
        		  $scope.setShipAddress.splice($index,1); 
    		      setTimeout(function(){	
    		    	  messagsTypeSuccess("Remove ship address. !");
    		    	  $scope.setShipAddressCount = $scope.setShipAddressCount - 1;
    		    	  $scope.$apply();
	        	  },500);
        		}
        	}

        });
	 }
	
	$scope.checkUserAcc = true;
	$scope.checkUserCust = function(){
		if($scope.username != ""){
			$http({
	 			method: 'GET',
			    url: baseUrl+"public/check/customer/"+$scope.username+"/username",
			    headers: {
			    	'Accept': 'application/json',
			        'Content-Type': 'application/json'
			    },
			    ignoreLoadingBar: true
			}).success(function(response) {
				if(response.status ==  "exist"){
					$scope.setErrorField("divUsername",response.msg);
					$scope.checkUserAcc = false;
				}else{
					$scope.setNomallField("divUsername");
					$scope.checkUserAcc = true;
				}
			});	
		}else{
			$scope.setErrorField("divUsername","The username is required and can not be empty!");
			$scope.checkUserAcc = false;
		}
		
	}
	
	$scope.checkEmailAcc = true;
	$scope.checkEmailCust = function(){
		if($scope.email != ""){
			if($scope.validateEmail($scope.email)){
				$http({
		 			method: 'GET',
				    url: baseUrl+"public/check/customer/"+$scope.email+"/email",
				    headers: {
				    	'Accept': 'application/json',
				        'Content-Type': 'application/json'
				    },
				    ignoreLoadingBar: true
				}).success(function(response) {
					if(response.status ==  "exist"){
						$scope.setErrorField("divEmail","The email is already exist.");
						$scope.checkEmailAcc = false;
					}else{
						$scope.setNomallField("divEmail");
						$scope.checkEmailAcc = true;
					}
				});
			}else{
				$scope.setNomallField("divEmail");
				$scope.setErrorField("divEmail","The email is invalid!");
				$scope.checkEmailAcc = false;
			}
		}else{
			$scope.setErrorField("divEmail","The email is required and can not be empty!");
			$scope.checkEmailAcc = false;
		}
		
	}
	
	$scope.checkTelAcc = true;
	$scope.checkTelCust = function(){
	
		if($("#tel1").val() != ""){
			$http({
	 			method: 'GET',
			    url: baseUrl+"public/check/customer/"+$scope.tel1+"/mobile",
			    headers: {
			    	'Accept': 'application/json',
			        'Content-Type': 'application/json'
			    },
			    ignoreLoadingBar: true
			}).success(function(response) {
				if(response.status ==  "exist"){
					$scope.setErrorField("divTel",response.msg);
					$scope.checkTelAcc = false;
				}else{
					$scope.setNomallField("divTel");
					$scope.checkTelAcc = true;
				}
			});	
		}else{
			$scope.checkTelAcc = false;
			$scope.setErrorField("divTel","The tel is required and can not be empty!");
			
		}
		
	}

	$scope.checkShipAdd = function(inde){
		if($("#checkIndex"+inde) == ""){
			$scope.setErrorField("divShipp_"+inde,"The shipp address is required and can not be empty!");
		}else{
			$scope.setNomallField("divShipp_"+inde);
		}
	}
		
	$scope.removeCustLogo = function(logo,id){
		if(id == 1){
			logo = $scope.profileRece1;
		}else{
			logo = $scope.profileRece2;
		}
		$http({
 			method: 'POST',
		    url: baseUrl+"public/upload/customer/image/remove",
		    headers: {
		    	'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    },
		    params : {filename:logo}
		}).success(function(response) {
			
				if(response.status = "success"){
					if(id == 1){
						$scope.profileRece1 = "";
						$('#profileReceived1').attr('src', baseUrl+"resources/images/default-placeholder.jpg");      
					}else{
						$scope.profileRece2 = "";
						$('#profileReceived2').attr('src', baseUrl+"resources/images/default-placeholder.jpg");    
					}
					
				}else{
					messagsTypeError(response.msg);
				}
		});
	}
	
	
	$scope.actionRegister = function(){
	
		$('#form-register').data('bootstrapValidator').validate();
		var formUser = $("#form-register").data('bootstrapValidator').validate().isValid();
		
		if($scope.needComapnyDelivery){
			var shipAdd = [];
			for(i=0;i<$scope.setShipAddress.length;i++){
			    if($scope.setShipAddress[i].shipaddress != "" && $scope.setShipAddress[i].shipaddress.length < 255){
			    	shipAdd.push({"shipName": $scope.setShipAddress[i].shipaddress});
			    }else if($scope.setShipAddress[i].shipaddress == ""){		
			    	formUser = false;
			    	$scope.setErrorField("divShipp_"+[i],"The ship is required and can not be empty!");
			    }else{
			    	$scope.setErrorField("divShipp_"+[i],"The ship post must be less than 255 characters long.");
			    	formUser = false;
			    }
			}
		}
		
		if($scope.checkUserAcc == false){
			formUser = false;	
		}
			
		
		if($scope.checkEmailAcc == false){
			formUser = false;
		}
		
		
		if($scope.checkTelAcc == false){
			formUser = false;	
		}
			
		$scope.selectEmpSer = function(){
			$('#form-register').bootstrapValidator('revalidateField', 'vb_emp');
	
		}
		$scope.checkEmailCust();
		$scope.checkTelCust();
		if(formUser){
		
			if($scope.password == $scope.cpassword){
				
					var rece = [];
					
					if(getValueFromID("receivd1_name") != "" && $scope.profileRece1 != ""){
						rece.push({"revName": getValueFromID("receivd1_name"),"revImg": $scope.profileRece1});
					}else if(getValueFromID("receivd1_name") == "" && $scope.profileRece1 != ""){
						rece.push({"revName": "","revImg": $scope.profileRece1});
					}else if(getValueFromID("receivd1_name") != "" && $scope.profileRece1 == ""){
						rece.push({"revName": getValueFromID("receivd1_name"),"revImg": ""});
					}
					
					if(getValueFromID("receivd2_name") != "" && $scope.profileRece2 != ""){
						rece.push({"revName": getValueFromID("receivd2_name"),"revImg": $scope.profileRece2});
					}else if(getValueFromID("receivd2_name") == "" && $scope.profileRece2 != ""){
						rece.push({"revName": "","revImg": $scope.profileRece2});
					}else if(getValueFromID("receivd2_name") != "" && $scope.profileRece2 == ""){
						rece.push({"revName": getValueFromID("receivd2_name"),"revImg": ""});
					}
					
					
					if($scope.forProductNormal == true)
						$scope.forProductNormal = 1;
					else
						$scope.forProductNormal = 0;
					
					if($scope.forProductBrand == true)
						$scope.forProductBrand = 1;
					else
						$scope.forProductBrand = 0;
					
					if($scope.productReady == true)
						$scope.productReady = 1;
					else
						$scope.productReady = 0;
					
					if($scope.productReadyContact == true)
						$scope.productReadyContact = 1;
					else
						$scope.productReadyContact = 0;
					
					if($scope.getProductFromCompany == true)
						$scope.getProductFromCompany = 1;
					else
						$scope.getProductFromCompany = 0;
					
					if($scope.needComapnyDelivery == true)
						$scope.needComapnyDelivery = 1;
					else
						$scope.needComapnyDelivery = 0;
						
					var stringValue = "";
					stringValue = {
							"custName": getValueFromID("companyName"),
							"custEmail": getValueFromID("email"),
							"custNameOther": getValueFromID("companyOther"),
							"custContactName": getValueFromID("contactName"),
							"custTel1": getValueFromID("tel1"),
							"custTel2": getValueFromID("tel2"),
							"custAddress": getValueFromID("address"),
							"custLogo": $scope.profileName,
							"custWebsite": getValueFromID("website"),
							"custEmpId": $("#vb_emp").val(),
							"user": {
								"username":  getValueFromID("username"),
								"password": getValueFromID("password")
								},
							"addonInfo": {
								"identityNum": getValueFromID("IDAndPassport"),
								"identityImg": $scope.profileID,
								"fb": getValueFromID("facebook"),
								"whatApp": getValueFromID("whatApp"),
								"line":  getValueFromID("line"),
								"dob": formatDateDB(getValueFromID("dateOfBirth")),
								"weChat": getValueFromID("wechat"),
								"abaAcc": getValueFromID("ABABankAccount")
								}
							};
					
						$http({
				 			method: 'POST',
						    url: baseUrl+"public/customer/register",
						    headers: {
						    	'Accept': 'application/json',
						        'Content-Type': 'application/json'
						    },
						    data : stringValue
						}).success(function(response) {
						
								if(response.status = "success"){
									messagsTypeSuccess(response.msg);
									setTimeout(function(){
										location.href = baseUrl+"register/success";
									},2000);
								}else{
									messagsTypeError(response.msg);
								}
						});
		
			
			}else{
				messagsTypeError("passwords don't match with confirm password.");
			}
			
			
		}
	}
	

	$scope.setErrorField = function(id,message){
		
		var i = '' //<i class="form-control-feedback bv-no-label glyphicon glyphicon-remove" data-bv-icon-for="'+id+'" style="display: block; right: 76px; top: 5px;"></i>;
		var small = '<small class="help-block" data-bv-validator="notEmpty" data-bv-for="'+id+'" data-bv-result="INVALID" style="">'+message+'</small>';
		$("#"+id).find("small").remove();
		$("#"+id).removeAttr('class');
		$("#"+id).attr('class', 'form-group has-feedback has-error');
		$("#"+id).append(small);
	}

	$scope.setSuccessField = function(id){
		//var i = '<i class="form-control-feedback bv-no-label glyphicon glyphicon-ok" data-bv-icon-for="'+id+'" style="display: block;right: 76px; top: 5px;"></i>';
		//var small = '<small class="help-block" data-bv-validator="notEmpty" data-bv-for="salStartDate" data-bv-result="INVALID" style="">The start date greater this month ! </small>';
		$("#"+id).find("i").remove();
		$("#"+id).find("small").remove();
		$("#"+id).removeAttr('class');
		$("#"+id).attr('class', 'form-group has-feedback has-success');

		//$("#"+id).append(i);
	}

	$scope.setNomallField = function(id){
		$("#"+id).removeClass("has-error");
		$("#"+id).removeClass("has-success");
		$("#"+id).find("i").remove();
		$("#"+id).find("small").remove();
	}
	
	$scope.validateEmail = function(email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(String(email).toLowerCase());
	}
	
}]);

function validate(evt) {
	  var theEvent = evt || window.event;
	  var key = theEvent.keyCode || theEvent.which;
	  key = String.fromCharCode( key );
	  var regex = /[0-9]|\./;
	  if( !regex.test(key) ) {
	    theEvent.returnValue = false;
	    if(theEvent.preventDefault) theEvent.preventDefault();
	  }
	}

