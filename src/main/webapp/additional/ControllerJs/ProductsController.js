'use strict';

app.controller('ProductsController',['$scope','$http','Upload','$timeout',function($scope, $http, Upload, $timeout){
	$scope.search = "";
	
	var myParam = location.search.split('brand=')[1];
	myParam = decodeURIComponent((myParam + '').replace(/\+/g, '%20'));
	
	if(myParam == 'undefined' || myParam == ''){
		$scope.search = "";
	}else{
		$scope.search = myParam;
	}
	
	
	
	$scope.wishListAdd = function(itemID){
		loadingPage(true);
		$http({
 			method: 'POST',
		    url: baseUrl+"api/wishlist/add",
		    headers: {
		    	'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    },
		    data:{"itemId":itemID,"brand":brand,"model":model},
		    ignoreLoadingBar: true
		}).success(function(response) {
			if(response.MESSAGE == 'SUCCESS'){
				angular.element(document.getElementById('idCountheader')).scope().getList();
			}
			loadingPage(false);
		});	
	}
	
	

	  $scope.totalItem = 0;
	  $scope.pageSize = {};
	  $scope.pageSize.rows = [ 
				{ value: 15, label: "15 " },
				{ value: 21, label: "21 " },
	      		{ value: 27, label: "27 " },
	      		{ value: 33, label: "33 " }
	      		];
	  $scope.pageSize.row = $scope.pageSize.rows[1].value;
	  
	  $scope.pagination = {
		      current: 1
	  };
	  
	$scope.getListItemBrand = function(){
		$scope.listItemBrands = [];
		$http({
 			method: 'GET',
		    url: baseUrl+"public/brand/list",
		    headers: {
		    		'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    }
		}).success(function(response) {
			if(response.MESSAGE == 'SUCCESS'){
				$scope.listItemBrands = response.RECORDS;
			}
		});	
	}
	
	$scope.getListItems = function(page,row){
		$scope.listItems = [];
		$http({
 			method: 'GET',
		    url: baseUrl+"public/item/list/"+row+"/"+page,
		    headers: {
		    		'Accept': 'application/json',
		        'Content-Type': 'application/json'
		    },
		    params:{"search": $scope.search}
		}).success(function(response) {
			dis(response);
			if(response.MESSAGE == 'SUCCESS'){
				$scope.listItems = response.RECORD.RECORDS;
				$scope.totalItem = response.RECORD.totalRecords;
			}else{
				$scope.totalItem = 0;
			}
		});	
	}
	
	
	$scope.filterData = function(ob){
		$scope.search = ob;
		//alert($scope.search);
		$scope.getListItems($scope.pagination.current,$scope.pageSize.row);
	}
	
	$scope.filterClear = function(){
		$scope.search = '';
		$scope.getListItems($scope.pagination.current,$scope.pageSize.row);
	}
	
	$scope.getListItems($scope.pagination.current,$scope.pageSize.row);
	$scope.getListItemBrand();
	
	 $scope.pageChanged = function(newPage) {
		  $scope.getListItems(newPage, $scope.pageSize.row);
	  };
	  
	  $scope.changeRowPage = function(){
		  $scope.getListItems($scope.pagination.current, $scope.pageSize.row);
	  }
	  
	  
}]);

/*jQuery(document).ready(function($) {
	setTimeout(function(){
		AT_Slider.init();
	},1000);
	
})*/

/*
app.directive('productListing',['$http',function($http) {
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        templateUrl: 'TopSelling.html',
        controller: function($scope, $timeout) {
            $timeout( function() {
            	
            },500);
        },
        link: function(scope, elm, attr) {
	         $http.get(baseUrl+"public/item/home/list").success(function(response){
	        	 if(response.MESSAGE == 'SUCCESS'){
	        		 scope.listItem = response.RECORD.RECORDS;
	 				 scope.listBrands = response.BRANDS;
	 				 setTimeout(function(){
	 					this.AT_Slider.init();
	 				},500);	
	 			 }
	         });
        }
    }
}]);
*/

/*app.directive('bestSellers',['$http',function($http) {
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        templateUrl: 'bestSellersTmp.html',
        controller: function($scope, $timeout) {
            $timeout( function() {
            	 alert();
            },500);
        },
        link: function(scope, elm, attr) {
		 $http.get(baseUrl+"public/item/brand/list/12/1?search="+id).success(function(response){
        	 if(response.MESSAGE == 'SUCCESS'){
        		 scope.listItemBrands = response.RECORDS;
        		
 				 setTimeout(function(){
 					this.AT_Slider.init();
 				},500);	
 			 }
         });
        }
    }
}]);*/

/*function IndexController($scope){

}
*/


