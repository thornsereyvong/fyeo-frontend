'use strict';

app.controller('OrderController',['$scope','$http','Upload','$timeout',function($scope, $http, Upload, $timeout){
	$scope.search = "";
	


	  $scope.totalItem = 0;
	  $scope.pageSize = {};
	  $scope.pageSize.rows = [ 
				{ value: 10, label: "10 " },
				{ value: 20, label: "20 " },
	      		{ value: 30, label: "30 " },
	      		{ value: 40, label: "40 " }
	      		];
	  $scope.pageSize.row = $scope.pageSize.rows[1].value;
	  
	  $scope.pagination = {
		      current: 1
	  };
	  
	$scope.getListItem = function(page,row){
		$scope.listItem = [];
		$http({
 			method: 'GET',
		    url: baseUrl+"api/order/list/"+row+"/"+page,
		    headers: {
	    		'Accept': 'application/json',
	    		'Content-Type': 'application/json'
		    },
		    params:{"search": $scope.search},
		    ignoreLoadingBar: true
		}).success(function(response) {
			dis(response);
			if(response.MESSAGE == 'SUCCESS'){
				$scope.listItem = response.RECORDS;
				$scope.totalItem = response.RECORDS.totalRecords;
			}
		});	
	}
	
	
	$scope.viewOrder = function(id){
		location.href = baseUrl+"user/order/"+id+"/detail.html";
	}
	
	
	$scope.searchOfCode = function(){
		$scope.getListItem($scope.pagination.current,$scope.pageSize.row);
	}
	
	
	$scope.getListItem($scope.pagination.current,$scope.pageSize.row);
	

	 $scope.pageChanged = function(newPage) {
		  $scope.getListItem(newPage, $scope.pageSize.row);
	  };
	  
	  $scope.changeRowPage = function(){
		  $scope.getListItem($scope.pagination.current, $scope.pageSize.row);
	  }
	  
	
	
}]);



