'use strict';

app.controller('CartController',['$scope','$http','Upload','$timeout',function($scope, $http, Upload, $timeout){
	$scope.search = "";
	$scope.checkOb = [];

   $scope.totalItem = 0;
   $scope.pageSize = {};
   $scope.pageSize.rows = [ 
			{ value: 10, label: "10 " },
			{ value: 20, label: "20 " },
      		{ value: 30, label: "30 " },
      		{ value: 40, label: "40 " }
      		];
   $scope.pageSize.row = $scope.pageSize.rows[0].value;
  
   $scope.pagination = {
	      current: 1
   };
	  
	$scope.submitCart = false;
	  $scope.submitButton = function(){
		  if($scope.checkOb.length != 0){
			  $scope.submitCart = true; 
			  $timeout(function () {
			         $('html, body').animate({
			            scrollTop: 50
			        }, 200);
			    }, 100);
		  }else{
			  messageTypeFail("Please select product.");
		  }
	  }
	  
	  
	  $scope.showComment = function(id){
		  var rowCount = $("#pre"+id.cartId).length;
		  if(id.comment == null){
			  id.comment = "";
		  }
		  if(rowCount == 0){
			  $('#'+id.cartId).closest('tr').after('<tr id="pre'+id.cartId+'"><td colspan="12">Comment <textarea class="form-control" id="cartCom'+id.cartId+'" style="    resize: none;height: 50px;border-radius: 3px; padding: 2px 2px 2px 2px;  font-size: 12px;" rows="" cols="">'+id.comment+'</textarea></td></tr>');
		  }else{
			  if ($("#pre"+id.cartId).css('display') == 'none'){
				  $("#pre"+id.cartId).css("display","");
			  }else{
				  $("#pre"+id.cartId).css("display","none"); 
			  }
			 
		  }
		 
	  }
	  
	$scope.getListItem = function(page,row){
		$scope.listItem = [];
		$http({
			method: 'POST',
		    url: baseUrl+"api/cart/list/"+page+"/"+row,
		    headers: {
	    		'Accept': 'application/json',
	    		'Content-Type': 'application/json'
		    },
		    params:{"search": $scope.search},
		    ignoreLoadingBar: true
		}).success(function(response) {
			dis(response);
			if(response.MESSAGE == 'SUCCESS'){
				$scope.listItem = response.RECORD.RECORDS;
				$scope.totalItem = response.RECORD.totalRecords;
				setTimeout(function(){
					$('.date2').daterangepicker({
				        format: 'DD-MM-YYYY',
				        singleDatePicker: true,
				        showDropdowns: true,
				    });
					var someDate = new Date(); 
					$('.date2').val(formatDate(someDate));
				});
				
				for(var i=0;i < countObj($scope.listItem);i++){
					for(var y=0;y < countObj($scope.checkOb);y++){
						if($scope.checkOb[y].cartId == $scope.listItem[i].cartId ){
							$scope.listItem[i].status = true;
							$scope.listItem[i].qty = $scope.checkOb[y].qty;
						}
					} 
				 }
				
				$scope.countPro = $scope.checkOb.length;
				
			}
		});	
	}
	
	$scope.searchOfCode = function(){
		$scope.getListItem($scope.pagination.current,$scope.pageSize.row);
	}
	
	
	$scope.getListItem($scope.pagination.current,$scope.pageSize.row);
	
	$scope.deleteCart = function(id){
		swal({
			  title: "You are about to delete this cart.",
			  text: "Click OK to Delete or Cancel",
			  type: "info",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "OK",
			  cancelButtonText: "No, cancel"
			},
			function(isConfirm) {
			  if (isConfirm) {
				  $http({
			 			method: 'POST',
					    url: baseUrl+"api/cart/delete",
					    headers: {
				    		'Accept': 'application/json',
				    		'Content-Type': 'application/json'
					    },
					    data:{"cartId": id},
					    ignoreLoadingBar: true
					}).success(function(response) {
					
						if(response.MESSAGE == 'SUCCESS'){
							messagsTypeSuccess(response.MESSAGE);
							
							var allAmt = 0;
							  for(var i=0;i < countObj($scope.checkOb);i++){
								  allAmt += allAmt + $scope.checkOb[i].totalAmt;
								  if($scope.checkOb[i].cartId == id){
									  $scope.checkOb.splice(i,1);
								  }
							  }
							 $scope.countPro = $scope.checkOb.length;
							 $scope.countPrice = parseFloat(allAmt);
							 
							$scope.getListItem($scope.pagination.current,$scope.pageSize.row);
							angular.element(document.getElementById('idCountheader')).scope().getList();
						}
					});	
			  } 
			});
		
	
	}
	
	
	$scope.deleteCartAll = function(){
		swal({
			  title: "You are about to delete all cart.",
			  text: "Click OK to Delete or Cancel",
			  type: "info",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "OK",
			  cancelButtonText: "No, cancel"
			},
			function(isConfirm) {
			  if (isConfirm) {
				  $http({
			 			method: 'POST',
					    url: baseUrl+"api/cart/delete/all",
					    headers: {
				    		'Accept': 'application/json',
				    		'Content-Type': 'application/json'
					    },
					    ignoreLoadingBar: true
					}).success(function(response) {
					
						if(response.MESSAGE == 'SUCCESS'){
							messagsTypeSuccess(response.MESSAGE);
							
							var allAmt = 0;
							  for(var i=0;i < countObj($scope.checkOb);i++){
								  allAmt += allAmt + $scope.checkOb[i].totalAmt;
								  if($scope.checkOb[i].cartId == id){
									  $scope.checkOb.splice(i,1);
								  }
							  }
							 $scope.countPro = $scope.checkOb.length;
							 $scope.countPrice = parseFloat(allAmt);
							 
							$scope.getListItem($scope.pagination.current,$scope.pageSize.row);
							angular.element(document.getElementById('idCountheader')).scope().getList();
						}
					});	
			  } 
			});
		
	
	}
	
	
	
	$scope.changeNum = function(val){
		if(isNaN(val.qty)){
			val.qty = 0;
		}
	}
	
	$scope.changeQty = function(ob){
		  if(isNaN(ob.qty)){
			  ob.qty = 1;
		  }
		  if(ob.qty === null || ob.qty == 0){
			  ob.qty = 1;
		  }
		  
		  for(var i=0;i < countObj($scope.listItem);i++){
			  if($scope.listItem[i].cartId == ob.cartId){
				  $scope.listItem[i].qty = ob.qty;
					var sqt = $scope.listItem[i].qty * $scope.listItem[i].unitprice;
					$scope.listItem[i].amount = sqt;
			  }
		  }
		  
		  for(var y=0;y < countObj($scope.checkOb);y++){
				if($scope.checkOb[y].cartId == ob.cartId ){
					$scope.checkOb[y].qty = ob.qty;
					var sqt = 0;
					sqt = $scope.checkOb[y].qty * $scope.checkOb[y].unitPrice;
					 $scope.checkOb[y].amount = sqt;
				
				}
		  } 
		  
		  
		  
		  var allAmt = 0;
		  for(var i=0;i < countObj($scope.checkOb);i++){
			  var amt = $scope.checkOb[i].unitPrice * $scope.checkOb[i].qty;
			  allAmt += allAmt + amt;
		  }
		  
		  $scope.countPro = $scope.checkOb.length;
		  $scope.countPrice = parseFloat(allAmt);
		  
		  
		  
	  }
	
	
	 $scope.selectCheck = function(obj){
		  if(document.getElementById("check"+obj.cartId).checked){
			  var amt = obj.qty * obj.unitprice;
			  $scope.checkOb.push({"cartId": obj.cartId,
				  				   "brandId": obj.brand,
				  				   "comment": obj.comment,
				  				   "status": "Pendding",
				  				   "size": obj.size,
				  				   "remark": obj.remark,
				  				   "qty" : obj.qty,
				  				   "extraCost": 0,
				  				   "modelId": obj.model,
				  				   "itemId": obj.itemId,
				  				   "unitPrice": obj.unitprice,
				  				   "totalAmt": amt,
				  				   "comment": $("#cartCom"+obj.orderId).val()
				  				  });
		  }else{
			  for(var i=0;i < countObj($scope.checkOb);i++){
				  if($scope.checkOb[i].cartId == obj.cartId){
					  $scope.checkOb.splice(i,1);
				  }
			  }
		  }
		  
		  
		  var allAmt = 0;
		  for(var i=0;i < countObj($scope.checkOb);i++){
			  allAmt += allAmt + $scope.checkOb[i].totalAmt;
		  }
		  
		  $scope.countPro = $scope.checkOb.length;
		  $scope.countPrice = parseFloat(allAmt);
	  }
	

	 $scope.pageChanged = function(newPage) {
		  $scope.getListItem(newPage, $scope.pageSize.row);
	  };
	  
	  $scope.changeRowPage = function(){
		  $scope.getListItem($scope.pagination.current, $scope.pageSize.row);
	  }
	  
	  
	  $scope.getProfile = function(){
			$scope.myprofile= [];
			$http({
	 			method: 'GET',
			    url: baseUrl+"api/shop/get",
			    headers: {
			    		'Accept': 'application/json',
			        	'Content-Type': 'application/json'
			    },
			    ignoreLoadingBar: true
			}).success(function(response) {
				if(response.MESSAGE == 'FOUND'){
					$scope.myprofile = response.RECORDS;
					
					$("#email").val($scope.myprofile.email);
					$("#tel1").val($scope.myprofile.tel1);
					$("#tel2").val($scope.myprofile.tel2);
					$("#shopName").val($scope.myprofile.shopName);
					$("#address").val($scope.myprofile.address);
					$("#description").val($scope.myprofile.description);
				}
			});	
		}
		$scope.getProfile();
	
		
		$scope.submitOrder = function(){
			 var radioValue = $("input[name='checkAddress']:checked").val();
			 var shipAddress = "";
			 var contactInfo = "";
			  
			swal({
				  title: "You are about to submit order.",
				  text: "Click OK or Cancel",
				  type: "info",
				  showCancelButton: true,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "OK",
				  cancelButtonText: "No, cancel"
				},
				function(isConfirm) {
				  if (isConfirm) {
					 
					  if(radioValue == 1){
						  shipAddress = $("#shipAddress").val();
					  }else{
						  contactInfo = $("#contactInfo").val();
					  }
						var dataString = {
										'description': $("#description").val(),
										'shipAddress':shipAddress,
										'amount': $scope.countPrice,
										'contactInfo': contactInfo,
										'orderDetails':$scope.checkOb
									};
						$http({
				 			method: 'POST',
						    url: baseUrl+"api/order/add",
						    headers: {
						    		'Accept': 'application/json',
						        	'Content-Type': 'application/json'
						    },
						    data: dataString,
						    ignoreLoadingBar: true
						}).success(function(response) {
							if(response.MESSAGE == 'SUCCESS'){
								messagsTypeSuccess(response.MESSAGE);
								$timeout(function(){
									location.href= baseUrl+"user/order.html";
								},700);
							}
						});	
						
				  }
				});
			
			
		}
	
}]);

$(function(){
	var someDate = new Date(); 
	$('#dateS').append(formatDate(someDate));
	$("#dateSub").append(formatDate(someDate));
});
function formatDate(date) {
	  var day = date.getDate();
	  var monthIndex = date.getMonth()+1;
	  var year = date.getFullYear();
	  return day + '-' + monthIndex + '-' + year;
	}


