var AT_Slider ={
  
  	owlSlider : function(){
      
      	jQuery(".product-list").length && jQuery(".product-list").on('initialize.owl.carousel initialized.owl.carousel change.owl.carousel changed.owl.carousel', function(e) {
          var current = e.relatedTarget.current()
            var items = $(this).find('.owl-stage').children()
            var add = e.type == 'changed' || e.type == 'initialized'

            items.eq(e.relatedTarget.normalize(current )).toggleClass('current', add)
        }).owlCarousel({
			nav			: true
          	,dots 		: true
      		,items		: 6
			,responsive : {
                0:{
                	items: 1
                }
              	,480:{
                	items: 2
                }
            	,768:{
              		items: 3
            	}
              	,1024:{
              		items: 4
            	}
            	,1180:{
              		items: 6
            	}
          	}
          	,navText	: ['<i class="fa fa-angle-left" title="previous"></i>', '<i class="fa fa-angle-right" title="Next"></i>']
		});
      
      	jQuery(".home-tab-third").length && jQuery(".home-tab-third").on('initialize.owl.carousel initialized.owl.carousel change.owl.carousel changed.owl.carousel', function(e) {
            var current = e.relatedTarget.current()
            var items = $(this).find('.owl-stage').children()
            var add = e.type == 'changed' || e.type == 'initialized'

            items.eq(e.relatedTarget.normalize(current )).toggleClass('current', add)
        }).owlCarousel({
			dots 		: true
      		,items		: 3
			,responsive : {
                0:{
                	items: 1
                }
            	,768:{
              		items: 2
            	}
              	,1024:{
              		items: 3
            	}
            	,1180:{
              		items: 3
            	}
          	}
		});
      
      	jQuery("#widget-partner").length && jQuery('#widget-partner').owlCarousel({
          	nav			: true
          	,dots 		: false
      		,items		: 5
			,responsive : {
                0:{
                	items: 2
                }
            	,600:{
              		items: 3
            	}
              	,1024:{
              		items: 4
            	}
            	,1180:{
              		items: 5
            	}
          	}
          	,navText	: ['<i class="fa fa-angle-left" title="previous"></i>', '<i class="fa fa-angle-right" title="Next"></i>']
        });
      
      	jQuery(".sb-latest-product-list").length && jQuery('.sb-latest-product-list').owlCarousel({
            nav			: true
          	,dots 		: false
          	,items		: 1
          	,navText	: ['<i class="fa fa-angle-left" title="previous"></i>', '<i class="fa fa-angle-right" title="Next"></i>']	
        });

        jQuery("#gallery-image").length && jQuery('#gallery-image').owlCarousel({
            nav			: true
          	,dots 		: false
          	,items		: 5
          	,margin		: 10
			,responsive : {
                0:{
                	items: 3
                }
              	,480:{
              		items: 4
            	}
              	,568:{
              		items: 5
            	}
              	,768:{
              		items: 3
            	}
              	,1024:{
              		items: 4
            	}
              	,1200:{
              		items: 5
            	}

          	}
          	,navText	: ['<i class="fa fa-angle-left" title="previous"></i>', '<i class="fa fa-angle-right" title="Next"></i>']	
        });
      
      	jQuery(".three-column-list").length && jQuery('.three-column-list').owlCarousel({
          	autoplay	: false
          	,dots 		: false
      		,items		: 3
          	,margin		: 30
			,responsive : {
                0:{
                	items: 1
                }
            	,480:{
              		items: 2
            	}
            	,1024:{
              		items: 3
            	}
          	}	
        });
    	
	}
  
  	,init : function(){
      this.owlSlider();
    }
  
}

/* Check IE */
var bcMsieVersion = {

  MsieVersion: function() {

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
    else                 // If another browser, return 0
      {
      return 0;
    }
  }

  ,init : function(){
    this.MsieVersion();
  }
}

jQuery(document).ready(function($) {
	
	AT_Slider.init();
      
});