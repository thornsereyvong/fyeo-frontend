<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="${request.contextPath}/head"></jsp:include>
<body class="customer-logged-in templateCart">
<div class="boxes-wrapper">
  <div class="mobile-version visible-xs visible-sm">
    <jsp:include page="${request.contextPath}/header"></jsp:include>
    <div id="page-body">
      <jsp:include page="${request.contextPath}/menu"></jsp:include>
      <div id="body-content">
        <div id="main-content">
          <div class="main-content">
            <div class="page-contact">
              <div class="container">
                <div class="row">
                  <div class="col-md-6 col-sm-12">
                    <div class="contact-form">
                      <h2 class="heading">Contact Us</h2>
                      <p>We look forward to hearing from you. You can send us a message through the form provided below.</p>
                      <form method="post" action="" id="contact_form" accept-charset="UTF-8" class="contact-form">
                        <input type="hidden" name="form_type" value="contact" />
                        <input type="hidden" name="utf8" value="✓" />
                        <div id="contact-form">
                          <div class="row">
                            <div class="col-xs-12 col-sm-6">
                              <div class="form-group">
                                <label for="name">Your Name</label>
                                <input type="text" id="name" class="form-control" value="" name="contact[name]" />
                              </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                              <div class="form-group">
                                <label for="email">Email Address <span class="req">*</span></label>
                                <input required type="email" id="email" class="form-control" value="" name="contact[email]" />
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="phone">Phone Number</label>
                            <input type="text" id="phone" class="form-control" value="" name="contact[phone]" />
                          </div>
                          <div class="form-group">
                            <label for="message">Your Message <span class="req">*</span></label>
                            <textarea required id="message" class="form-control" cols="40" rows="10" name="contact[body]"></textarea>
                          </div>
                          <div class="form-actions">
                            <button type="submit" class="btn btn-default main-color">Send Message</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                    <div id="page-contact-map" class="map" style="height: 290px;"></div>
                    <div class="contact-content"> </div>
                  </div>
                </div>
              </div>
            </div>
            <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCmqy8wIh9KCnj_WKXqaskSCM-AUpXd01Q"></script> 
            <script>
    /*---- Google map -----*/
    (function () {
        "use strict";

        if (jQuery("#page-contact-map")) {
            var locations = [
              ['<div class="map-info-box"><h4>Find Us now!</h4><p>42 avenue des Champs, Paris, France</p></div>', 48.871069, 2.304973, 9]
            ];

            var map = new google.maps.Map(jQuery("#page-contact-map")[0], {
                zoom: 12,
                scrollwheel: false,
              	center: new google.maps.LatLng(48.871069, 2.304973),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();


            var marker, i;

            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    animation: google.maps.Animation.DROP,
                  	icon: '//cdn.shopify.com/s/files/1/1801/4931/t/2/assets/pin.png?4608748305671055686',
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }
    }());

</script> 
          </div>
        </div>
      </div>
      <div id="bottom" class="bottom-container">
        <div class="container"></div>
      </div>
      <jsp:include page="${request.contextPath}/foot"></jsp:include>
    </div>
    <jsp:include page="${request.contextPath}/footer"></jsp:include>
  </div>
</div>
</body>
</html>