<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="${request.contextPath}/head"></jsp:include>

<style>
/* ngIf animation */
#quick-shop-popup .modal-dialog {
    width: 400px;
}
.container{max-width:1170px; margin:auto;}
img{ max-width:100%;}
.inbox_people {
  background: #f8f8f8 none repeat scroll 0 0;
  float: left;
  overflow: hidden;
  width: 40%; border-right:1px solid #c4c4c4;
}
.inbox_msg {
  border: 1px solid #c4c4c4;
  clear: both;
  overflow: hidden;
}
.top_spac{ margin: 20px 0 0;}


.recent_heading {float: left; width:40%;}
.srch_bar {
  display: inline-block;
  text-align: right;
  width: 60%; padding:
}
.headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

.recent_heading h4 {
  color: #05728f;
  font-size: 21px;
  margin: auto;
}
.srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
.srch_bar .input-group-addon button {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  padding: 0;
  color: #707070;
  font-size: 18px;
}
.srch_bar .input-group-addon { margin: 0 0 0 -27px;}

.chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
.chat_ib h5 span{ font-size:13px; float:right;}
.chat_ib p{ font-size:14px; color:#989898; margin:auto}
.chat_img {
  float: left;
  width: 11%;
}
.chat_ib {
  float: left;
  padding: 0 0 0 15px;
  width: 88%;
}

.chat_people{ overflow:hidden; clear:both;}
.chat_list {
  border-bottom: 1px solid #c4c4c4;
  margin: 0;
  padding: 18px 16px 10px;
}
.inbox_chat { height: 550px; overflow-y: scroll;}

.active_chat{ background:#ebebeb;}

.incoming_msg_img {
  display: inline-block;
  width: 6%;
}
.received_msg {
  display: inline-block;
  padding: 0 0 0 10px;
  vertical-align: top;
  width: 92%;
 }
 .received_withd_msg p {
  background: #ebebeb none repeat scroll 0 0;
  border-radius: 3px;
  color: #646464;
  font-size: 14px;
  margin: 0;
  padding: 5px 10px 5px 12px;
  width: 100%;
}
.time_date {
  color: #747474;
  display: block;
  font-size: 12px;
  margin: 8px 0 0;
}
.received_withd_msg { width: 57%;}
.mesgs {
  float: left;
  padding: 30px 15px 0 25px;
  width: 100%;
}

 .sent_msg p {
  background:#ebebeb none repeat scroll 0 0;
  border-radius: 3px;
  font-size: 14px;
  margin: 0; color:#fff;
  padding: 5px 10px 5px 12px;
  width:100%;
}
.outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
.sent_msg {
  float: right;
  width: 46%;
}
.input_msg_write input {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  color: #4c4c4c;
  font-size: 15px;
  min-height: 48px;
  width: 100%;
}

.type_msg {border-top: 1px solid #c4c4c4;position: relative;}
.msg_send_btn {
  background: #05728f none repeat scroll 0 0;
  border: medium none;
  border-radius: 50%;
  color: #fff;
  cursor: pointer;
  font-size: 17px;
  height: 33px;
  position: absolute;
  right: 0;
  top: 11px;
  width: 33px;
}
.messaging { padding: 0 0 50px 0;}
.msg_history {
  height: 516px;
  overflow-y: auto;
}
</style>
<body class="customer-logged-in templateCart">
<div class="boxes-wrapper">
  <div class="mobile-version visible-xs visible-sm">
    <jsp:include page="${request.contextPath}/header"></jsp:include>
    <div id="page-body" ng-controller="ProductDetaController">
      <jsp:include page="${request.contextPath}/menu"></jsp:include>
      <div id="body-content" ng-init="getListItem('${itemId}','${model}')">
        <div class="container">
          <div id="breadcrumb" class="breadcrumb-holder">
            <ul class="breadcrumb">
              <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a itemprop="url" href="${pageContext.request.contextPath}/"> Home </a><i class="icon icon-thing-arrow-right"></i> </li>
              <li><a href="/collections/gaming" title="">${model}</a><i class="icon icon-thing-arrow-right"></i></li>
              <li class="active">${itemId}</li>
            </ul>
          </div>
        </div>
        <div id="main-content" >
          <div class="main-content">
            <div class="container">
              <div class="row">
                <div id="col-main" class="col-md-12">
                  <div class="page-product product-single">
                    <div class="product">
                      <div class="row">
                        <div id="product-image" class="col-sm-12 product-image">
                          <div class="product-image-inner row">
                            <div class="">
                              <table class="table table-bordered">
                                <tr>
                                  <td style="width: 20%;">
                                  <img class="img-responsive"  src="${pageContext.request.contextPath}/additional/images/giphy.gif"  lazy-img="{{listItem.image}}" alt="placehold.it/350x250" /></td>
                                  <td style="width: 42%;border-left:0;border-right:0">
                                  	<div class="list-group-item-text">
                                      <h1 itemprop="name" content="Golddax Product Example">{{listItem.brand}}</h1>
                                      <h5 class="list-group-item-heading">Color: {{listItem.itemOption}} </h5>
                                      <h5 class="list-group-item-heading">Model: {{listItem.modelValue}} </h5>
                                    </div></td>
                                  <td style="    width: 31%;">
                                  	<h2>  <small>$ {{listItem.dollarPrice | number:2}} </small></h2>
                                    <p>Suggested retail price  $ <small> {{listItem.dollarRetailPrice | number:2}} </small> </p>
                                   </td>
                                </tr>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="">
                      <table class="table table-bordered" style="margin-bottom: 30px;">
                        <tr  ng-repeat="ob in listItem.detail">
                          <td style="width: 20%;border-bottom: 0">
                       			<a href="javascript:void(0);" ng-click="showImage(ob.image)" data-target="#quick-shop-popup"  data-toggle="modal" title="Quick View">   <img class="media-object img-rounded img-responsive"  src="${pageContext.request.contextPath}/additional/images/giphy.gif" 	  lazy-img="{{ob.image}}" alt="placehold.it/350x250" ></a></td>
                          <td style="width: 42%;    border-left: 0;border-right: 0; border-bottom: 0;">
                          	<div class="row">
                              <div class="col-lg-7">
                                <h4 class="list-group-item-heading"> {{ob.model}} </h4>
                                <ul class="font2 no-style-list">
                                  <li>FRONT COLOR : {{ob.frontColor}}</li>
                                  <li>FRAM Color : {{ob.framColor}}</li>
                                  <li>LEN Color : {{ob.lenColor}}</li>
                                  <li>LEN MATERIAL : {{ob.lenMaterail}}</li>
                                  <li>LAN PROPERTY : {{ob.lenProperty}}</li>
                                  <li>LAN TRANS : {{ob.lenTrans}}</li>
                                  <li>POLARIZED : {{ob.polarized}}</li>
                                </ul>
                              </div>
                              <div class="col-lg-5"> <a class="pull-right font36" ng-click="wishListAdd('${itemId}',listItem.brand,ob.model)" href="javascript:void(0);"><i class="icon icon-favorites"></i></a> <br>
                                <h2 style="margin-top: 30px;">  <small>$ {{ob.dollarPrice | number:2}} </small></h2>
                                <p class="font12">Suggested retail price 
                                  <small> $ {{ob.dollarRetailPrice | number:2}} </small> </p>
                              </div>
                            </div>
                           </td>
                          <td  style="width: 31%;border-bottom: 0">
                          	<div class="row">
                              <h5>QUANTITY</h5>
                              <hr/>
                              <div id="product-action-9923383239" class="options" ng-repeat="st in ob.stocks">
                                <div class="qty-add-cart">
                                  <div class="quantity-product">
                                    <div class="col-lg-3 form-group1 padding-top-8">
                                      <label>Size {{st.size}} :</label>
                                    </div>
                                    <div class="col-lg-5">
                                      <div class="quantity form-group1" >
                                        <input type="text" ng-model="st.qty" class="item-quantity" min="1" value="{{st.qty}}" ng-change="changeNum(st)" name="quantity" >
                                        <span class="qty-wrapper"> <span class="qty-inner"> <span class="qty-up" title="Increase" ng-click="(st.qty=st.qty+1)" data-src="#quantity"> <i class="fa fa-plus"></i> </span> <span class="qty-down" title="Decrease" ng-click="(st.qty == 0) ? 0:(st.qty=st.qty-1)"  data-src="#quantity"> <i class="fa fa-minus"></i> </span> </span> </span> </div>
                                    </div>
                                    <div class="col-lg-1" style=" padding-top: 5px;"> <span class="label label-success custom_stock" ng-if="st.skStatus == 1">&nbsp;</span> <span class="label label-warning custom_stock" ng-if="st.skStatus == 0">&nbsp;</span> </div>
                                    <div ng-if="st.qty > 0" class="col-lg-12 animationIf"   >
                                      <div class="row">
                                        <div class="col-lg-12">
                                          <label class="font2" style="padding-left: 5px;">CUSTOMER REFERENCE max 250 characters</label>
                                        </div>
                                        <div class="col-lg-6">
                                          <div class="form-group">
                                            <textarea rows=""  class="form-control " ng-model="st.comment" style="resize: none;width: 200px;height: 70px;max-height:70px;min-height:70;min-width:200px;max-width:200px; padding: 9px; margin-top: 6px;" cols=""></textarea>
                                          </div>
                                        </div>
                                        <div class="col-lg-6 text-right">
                                          <div class="form-group">
                                            <button ng-click="cartAdd('${itemId}',listItem.brand,ob.model,st.size,st.qty,ob.dollarPrice,st.comment)" class="btn custom_btn_add_cart btn-xs btn-default"><span class="icon icon-add-to-cart"></span> &nbsp; Add To Cart</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="clearfix"> </div>
                                  </div>
                                </div>
                              </div>
                            </div></td>
                        </tr>
                      </table>
                      <div class="form-group pull-right">
                        <button ng-click="cartAddList('${itemId}',listItem.brand)" class="btn  btn-xs btn-default"><span class="icon icon-add-to-cart"></span> &nbsp; Add To Cart</button>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="product-simple-tab">
                      <div role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tabs-2" aria-controls="tabs-2" role="tab" data-toggle="tab">Description</a></li>
                          <li role="presentation" class=""><a href="#tabs-1" aria-controls="tabs-1" role="tab" data-toggle="tab">Inbox</a></li>
                        </ul>
                        <div class="tab-content">
                          <div role="tabpanel" class="tab-pane active" id="tabs-2">
                            <div class="row">
                              <div class="col-lg-3" id="lef"> </div>
                              <div class="col-lg-9" id="rig"> </div>
                            </div>
                          </div>
                           <div role="tabpanel" class="tab-pane " id="tabs-1">
                            <div class="row">
                             <div class="messaging">
						      <div class="inbox_msg">
						        <div class="mesgs">
						          <div class="msg_history">
						          	
						          	  <div  ng-repeat="ob in listItemInbox" style="    margin-top: 15px;">
							            <div class="incoming_msg">
							              <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">
							               </div>
							              <div class="received_msg">
							                <div class="received_withd_msg">
							                  <p>{{ob.inboxMsg}}</p>
							                  <span class="time_date"> {{ob.inboxDate}}</span></div>
							              </div>
							            </div>
							            <div class="outgoing_msg" ng-repeat="data in ob.inboxReply">
							              <div class="sent_msg" >
							                <p style="color:#fff"  ng-bind-html="data.inboxMsg"></p>
							                <span class="time_date">{{data.inboxDate}}</span> </div>
							            </div>
							         </div>  
						           
						          </div>
						          <div class="type_msg">
						            <div class="input_msg_write">
						              <input type="text" class="write_msg" ng-model="message" id="message" placeholder="Type a message" />
						              <button class="msg_send_btn" type="button" ng-click="AddInboxItem(message)"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
						            </div>
						          </div>
						        </div>
						      </div>
						    </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <script type="text/javascript">
                  jQuery(document).ready(function($) {
                    jQuery(".related-items").length && jQuery(".related-items").on('initialize.owl.carousel initialized.owl.carousel change.owl.carousel changed.owl.carousel', function(e) {
                        var current = e.relatedTarget.current()
                        var items = $(this).find('.owl-stage').children()
                        var add = e.type == 'changed' || e.type == 'initialized'

                        items.eq(e.relatedTarget.normalize(current )).toggleClass('current', add)
                    }).owlCarousel({
                        nav			: true
                        ,dots 		: true
                        ,items		: 4
                        ,responsive : {
                          0:{
                            items: 1
                          }
                          ,480:{
                            items: 2
                          }
                          ,768:{
                            items: 3
                          }
                          ,1024:{
                            items: 3
                          }
                          ,1180:{
                            items: 4
                          }
                        }
                        ,navText	: ['<i class="fa fa-angle-left" title="previous"></i>', '<i class="fa fa-angle-right" title="Next"></i>']
                    });
                  })
                </script> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <script src="${pageContext.request.contextPath}/additional/ControllerJs/productDetailController.js"></script> 
        <script>
	  jQuery(document).ready(function($) {
	
		 
		  
		  $("#close-quick-shop-popup").click(function(){
			  $('#quick-shop-popup').on( 'hide.bs.modal', function () {
			      $(".zoomContainer").remove();
			     // AT_Main.refreshZoom();
			    });
		  });
	  });
	</script>
	
	
        <div id="quick-shop-popup" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1" style="display: none;">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header"> <span class="close" title="Close" data-dismiss="modal" id="close-quick-shop-popup" aria-hidden="true"></span> </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12 product-image">
                    <divclass=" row">
                    	<a class="featured-image ">
                    	  <img class="" ng-src="{{imageZoom}}" style="width: 100%" data-zoom-image="{{imageZoom}}" alt=""> 
                    	</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        
        
        
        
        
        
      </div>
    </div>
    <div id="bottom" class="bottom-container">
      <div class="container"></div>
    </div>
    <jsp:include page="${request.contextPath}/foot"></jsp:include>
  </div>
  <jsp:include page="${request.contextPath}/footer"></jsp:include>
</div>
</div>
</body>
</html>