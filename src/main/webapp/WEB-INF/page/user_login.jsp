<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="${request.contextPath}/head"></jsp:include>
<body class="templateCustomersLogin">
<div class="boxes-wrapper">
  <div class="mobile-version visible-xs visible-sm">
    <jsp:include page="${request.contextPath}/header"></jsp:include>
    <div id="page-body">
      <jsp:include page="${request.contextPath}/menu"></jsp:include>
      

      <div id="body-content">
         <div class="container">
        <div id="main-content">
          <div id="main-content">
            <div class="main-content">
              <div id="col-main" class="page-login">
                <div class="row">
                
                  <div class="col-sm-6 col-xs-12 col-sm-offset-3">
                    <div class="form-wrapper">
                      <div id="customer-login" class="content">
                        <h2 class="heading">Sign In</h2>
                        <p>Welcome back! Sign to your account</p>
                        
                        <form method="post" action="${pageContext.request.contextPath}/login.html" id="customer_login" accept-charset="UTF-8">
                          <input type="hidden" name="form_type" value="customer_login" />
                          <input type="hidden" name="utf8" value="✓" />
                          <div class="control-wrapper">
                            <label for="customer_email">Username<span class="req">*</span></label>
                            <input type="text" required name="tc_username" id="tc_username" placeholder="Username" />
                          </div>
                          <div class="control-wrapper">
                            <label for="customer_password">Password<span class="req">*</span></label>
                            <input type="password" required name="tc_password" id="tc_password" class="password" placeholder="Password" />
                          </div>
                          <div class="control-wrapper last">
                            <!-- <div class="action"> <a class="forgot-pass" href="javascript:;" onclick="showRecoverPasswordForm()">Forgotten Password?</a> <a class="return-store" href="../index.html">Return to Store</a> </div> -->
                            <button class="btn btn-default" type="submit">Login</button>
                          </div>
                        </form>
                      </div>
                      
                    
                    </div>
                  </div>
                  
                  
                </div>
              </div>
              <script type="text/javascript">
					  if (window.location.hash == '#recover') { showRecoverPasswordForm(); }
					
					  function showRecoverPasswordForm() {
					    $('#recover-password').fadeIn();
					    $('#customer-login').hide();
					    window.location.hash = '#recover';
					    return false;
					  }
					
					  function hideRecoverPasswordForm() {
					    $('#recover-password').hide();
					    $('#customer-login').fadeIn();
					    window.location.hash = '';
					    return false;
					  }
				</script> 
            </div>
          </div>
        </div>
      </div>
     </div>
      
      <div id="bottom" class="bottom-container">
        <div class="container"></div>
      </div>
      <jsp:include page="${request.contextPath}/foot"></jsp:include>
    </div>
    <jsp:include page="${request.contextPath}/footer"></jsp:include>
  </div>
</div>
</body>
</html>