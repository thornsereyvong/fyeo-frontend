<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="${request.contextPath}/head"></jsp:include>
<style>
.product-grid-item {
    width: 20%;
}
.product-wrapper .product-head .product-name, .product-wrapper-2 .product-head .product-name {
    margin-bottom: 4px;
}
</style>
<body class="templateIndex">
<div class="boxes-wrapper" ng-controller="IndexController">
  <div class="mobile-version visible-xs visible-sm">
    <jsp:include page="${request.contextPath}/header"></jsp:include>
    <div id="page-body">
      <jsp:include page="${request.contextPath}/menu"></jsp:include>
      <div id="body-content" >
        <div id="main-content">
          <div class="main-content">
            <div id="home-main-content">
              <jsp:include page="${request.contextPath}/slide"></jsp:include>
              
        
              
              <div id="shopify-section-1496637533754" class="shopify-section">
                <div class="section-separator section-separator-1496637533754">
                  <div class="separator separator-1496637533754">
                    <div class="spacing" style="margin-bottom: 30px"> </div>
                  </div>
                </div>
              </div>
              <div id="shopify-section-1487662644044" class="shopify-section">
                <div role="tabpanel" id="home-tabs-third-1487662644044" class="home-tab-third-section">
                  <div class="container"> 
                    <div class="nav-tab-wrapper">
                      <div class="title-wrapper">
                        <h2>Best Sellers</h2>
                      </div>
                      <ul class="nav nav-tabs tab-heading" role="tablist">
                        <li role="presentation"  ng-repeat="ob in listBrands" ng-class="(brandId == ob.brandName) ? 'active tab-title':'tab-title';"  ng-click="getListItemBrand(ob.brandName)">
                        	<a href="#home-tab-third-1487662644044-1" aria-controls="home-tab-third-1487662644044-1" role="tab" data-toggle="tab"> {{ob.brandName}} </a> 
                        </li>
                      </ul>
                    </div>
                    <div  class="tab-content"><!-- home-carousel  -->
	                    <div class="clearfix"><!-- cata-product cp-grid   -->
		                    <div class="product-grid-item "  ng-repeat="ob in listItemBrands" >
		                      <div class="product-wrapper">
		                        <h3 class="noti-title" style="display: none;">You may also like:</h3>
		                        <div class="product-head">
		                          <div class="product-image">
		                            <div class="product-wrap-info">
		                              <h5 class="product-name"> <a href="${pageContext.request.contextPath}/product/{{ob.brand}}/{{ob.itemid}}.html">{{ob.brand}}</a> </h5>
		                              <h5 class="citemname"> <a href="${pageContext.request.contextPath}/product/{{ob.brand}}/{{ob.itemid}}.html">{{ob.itemname}}</a> </h5>
		                              <div class="product-type">{{ob.color}}</div>
		                              <div class="product-review"> <span class="shopify-product-reviews-badge" data-id="9923365255"></span> </div>
		                              <div class="product-des-list">
		                               <p>Item Name : {{ob.itemname}}</p>
		                              </div>
		                            </div>
		                            <div class="featured-img"> <a href="${pageContext.request.contextPath}/product/{{ob.brand}}/{{ob.itemid}}.html"> <img class="featured-image"  src="${pageContext.request.contextPath}/additional/images/giphy.gif"  lazy-img="{{ob.image}}" alt="Black Fashion Zapda Example" /> <!-- <span class="product-label"> <span class="label-sale"> <span class="sale-text"> Sale </span> </span> </span> --> </a> </div>
		                          </div>
		                        </div>
		                        <div class="product-content">
		                          <div class="pc-inner">
		                            <div class="product-review"> <span class="shopify-product-reviews-badge" data-id="9923365255"></span> </div>
		                            <div class="product-des-grid">
		                             <!--  <p>Description
		                                Our new HPB12 / A12 battery is rated at 2000mAh and designed to power up Black and De...</p> -->
		                            </div>
		                            <div class="availability">
		                              <label>Availability:</label>
		                              <span class="available">In Stock</span> </div>
		                            <div class="price-cart-wrapper">
		                              <div class="product-price"> 
		                              	<span class="price-sale"><small style="color:#4e4e4e">from</small><span class="money ">$ {{ob.dollarwsprice | number:2}}</span></span> <br>
		                                <span class="price-sale"><small style="color:#4e4e4e;" class="font12">Suggested retail price  $ </small> <span class="money font12">{{ob.dollarrtprice | number:2}}</span></span></div>
		                            </div>
		                            <div class="product-button" >
		                              <div data-handle="black-fashion-zapda-shoes" ng-click="getListItemDetail(ob.itemid,ob.brand)" class="quick_shop quick-shop-button hidden-sm hidden-xs" data-toggle="modal" title="Quick View"> <i class="icon icon-compare"></i>Quick View  </div>
		                             <!--  <div class="product-wishlist"> <a class="add-to-wishlist add-product-wishlist" data-handle-product="black-fashion-zapda-shoes" href="javascript:void(0);" ng-click="wishListAdd(ob.itemId)" title="Wishlist"><i class="icon icon-favorites"></i>Wishlist</a> </div> -->
		                            </div>
		                          </div>
		                        </div>
		                      </div>
		                    </div>
		                  </div>
		                 </div> 
		               
                        </div>
                      </div>
                      
                      
                      
                    </div>
                  </div>
                </div>
              </div>

            
              <div id="shopify-section-1496638831513" class="shopify-section">
                <div class="section-separator section-separator-1496638831513">
                  <div class="separator separator-1496638831513">
                    <div class="spacing" style="margin-bottom: 45px"> </div>
                  </div>
                </div>
              </div>
              
              
              <div id="shopify-section-1487565724251" class="shopify-section">
                <div class="home-top-banner style-2">
                  <div class="container">
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="banner-item"> <a href="#"> <img src="${pageContext.request.contextPath}/additional/images/B4_Main_banner.jpg" alt="" /> </a> </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="shopify-section-1496638831513" class="shopify-section">
                <div class="section-separator section-separator-1496638831513">
                  <div class="separator separator-1496638831513">
                    <div class="spacing" style="margin-bottom: 45px"> </div>
                  </div>
                </div>
              </div>
              
              
              
              <div class="shopify-section">
                <div class="home-product  home-carousel">
                  <div class="container">
                    <div class="title-wrapper">
                      <h2>Top Selling of the Week </h2>
                    </div>
                    <div class="product-list">
                      <div class="product-wrapper pageLeft-animate"   ng-repeat="ob in listItem" >
                        <div class="product-head">
                          <div class="product-image">
                          	<h5 class="product-name"> <a href="${pageContext.request.contextPath}/product/{{ob.brand}}/{{ob.itemid}}.html">{{ob.brand}}</a> </h5>
                          	<h5 class="citemname"> <a href="${pageContext.request.contextPath}/product/{{ob.brand}}/{{ob.itemid}}.html">{{ob.itemname}}</a> </h5>
                            <div class="product-type">{{ob.color}}</div>
                            <div class="featured-img"> <a href="${pageContext.request.contextPath}/product/{{ob.brand}}/{{ob.itemid}}.html">
                            	<img class="featured-image" src="${pageContext.request.contextPath}/additional/images/giphy.gif"  lazy-img="{{ob.image}}" alt="Kinla Product Sample" />  <!-- <span class="product-label"> <span class="label-sale"> <span class="sale-text"> Sale </span> </span> </span>  --> </a> </div>
                          </div>
                        </div>
                        <div class="product-content">
                         <!--  <p style="    font-size: 12px;">Color: {{ob.frontColor}}</p> -->
                          <div class="pc-inner">
                            <div class="price-cart-wrapper">
                              <div class="product-price"> 
                                <!-- <span class="price-compare"> <span class=money>$229.00</span></span>  --> 
                                <span class="price-sale"><small style="color:#4e4e4e">from</small> <span class="money ">$ {{ob.dollarwsprice | number:2}}</span></span> <br>
                                <span class="price-sale"><small style="color:#4e4e4e;" class="font12">Suggested retail price  $ </small> <span class="money font12">{{ob.dollarrtprice | number:2}}</span></span>  </div>
                              <!-- <div class="add-cart-button"> <a href="#" class="btn-primary add-to-cart" title="Add to cart"> <span class="icon icon-add-to-cart"></span> </a> </div> -->
                            </div>
                            <div class="product-button">
                              <div data-handle="condimentum-turpis" class="quick_shop quick-shop-button hidden-sm hidden-xs" data-target="#quick-shop-popup"  data-toggle="modal" title="Quick View"> <i class="icon icon-compare"></i>Quick View  </div>
                             <!--  <div class="product-wishlist"> <a class="add-to-wishlist add-product-wishlist" ng-click="wishListAdd(ob.itemid)" data-handle-product="condimentum-turpis" href="javascript:void(0);" title="Wishlist"><i class="icon icon-favorites"></i>Wishlist</a> </div> -->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              
              <div id="shopify-section-1496638848829" class="shopify-section">
                <div class="section-separator section-separator-1496638848829">
                  <div class="separator separator-1496638848829">
                    <div class="spacing" style="margin-bottom: 33px"> </div>
                  </div>
                </div>
              </div>
              <!-- END content_for_index --> 
            </div>
          </div>
        </div>
        <!-- End Main Content --> 
        
	<script>
	  jQuery(document).ready(function($) {
	
		  $('#myModal').on('shown.bs.modal', function () {
			 // $("#myModal").css("background","#333");
		  })
		  
	  });
	</script>
				
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog  modal-lg" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
			      </div>
			      <div class="modal-body">
			      
			      	 <table class="table table-bordered">
                        <tr>
                          <td style="width: 20%;">
                          <img class="img-responsive"  src="${pageContext.request.contextPath}/additional/images/giphy.gif"  lazy-img="{{listItemDetail.image}}" alt="placehold.it/350x250" /></td>
                          <td style="width: 42%;border-left:0;border-right:0">
                          	<div class="list-group-item-text">
                              <h1 itemprop="name" content="Golddax Product Example">{{listItemDetail.brand}}</h1>
                              <h5 class="list-group-item-heading">Color: {{listItemDetail.itemOption}} </h5>
                              <h5 class="list-group-item-heading">Model: {{listItemDetail.modelValue}} </h5>
                            </div></td>
                          <td style="    width: 31%;">
                          	<h2>  <small>$ {{listItemDetail.dollarPrice | number:2}} </small></h2>
                            <p>Suggested retail price  $ <small> {{listItemDetail.dollarRetailPrice | number:2}} </small> </p>
                           </td>
                        </tr>
                      </table>
			      
			      
			      
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" class="btn btn-primary">Save changes</button>
			      </div>
			    </div>
			  </div>
			</div>
			
			
			
      </div>
      <script src="${pageContext.request.contextPath}/additional/ControllerJs/IndexController.js"></script>
      <jsp:include page="${request.contextPath}/foot"></jsp:include>
    </div>
    <jsp:include page="${request.contextPath}/footer"></jsp:include>
  </div>
</div>
</body>
</html>