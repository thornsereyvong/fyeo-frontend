<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="${request.contextPath}/head"></jsp:include>


<body class="customer-logged-in templateCart">
	<div class="boxes-wrapper">
		<div class="mobile-version visible-xs visible-sm">
			<jsp:include page="${request.contextPath}/header"></jsp:include>

			<div id="page-body" ng-controller="OrderDetailController">
				<jsp:include page="${request.contextPath}/menu"></jsp:include>
				<div id="body-content">
				  <div class="container"> 
				  	<jsp:include page="${request.contextPath}/user-admin"></jsp:include>
				  	
				  	<div class="col-md-10">
				  		<div class="panel panel-default">
						  <div class="panel-heading">
						    <h3 class="panel-title">Orders Detail</h3>
						  </div>
						  <div class="panel-body">
						  	<div class="row">
				            	<div class="col-lg-6">
				            	
				            	</div>
				            </div>
						   	<table class="table table-hover">
						   		<thead>
									<tr>
										<th class="width-75 text-center">No</th>
										<th>Brand</th>
										<th>Model</th>
										<th>Size</th>
										<th>Qty</th>
										<th>Unit Price</th>
										<th>Total Amount</th>
										<th>Comment</th>
									</tr>
								</thead>
								 <tr  ng-repeat="ob in listItemDetail">
									<td>{{$index+1}}</td>
									<td>{{ob.brand}}</td>
									<td>{{ob.model}}</td>
									<td>{{ob.size}}</td>
									<td>{{ob.qty}}</td>
									<td> $ {{ob.unitprice | number:2}}</td>
									<td> $ {{ob.amount | number:2}}</td>
									<td>{{ob.comment}}</td>
								</tr>
								
						   	</table>
						   	
						   	  
				            <div class="row">
				            	<div class="col-lg-6 col-lg-offset-6">
				            		<div class="panel panel-default">
									  <div class="panel-heading">
									    <h3 class="panel-title">Total SUMMARY</h3>
									  </div>
									  <div class="panel-body">
									  	<table class="table">
									  		<tr>
									  			<td class=" text-left">Status</td>
									  			<td class=" text-right">{{listItem.status}}</td>
									  		</tr>
									  		<tr>
									  			<td class=" text-left">Order Date</td>
									  			<td class=" text-right">{{listItem.orderDate}}</td>
									  		</tr>
									  		<tr>
									  			<td class=" text-left">Total</td>
									  			<td class=" text-right"> $ {{listItem.amount}}</td>
									  		</tr>
									  		<tr>
									  			<td  class=" text-right" colspan="2"><a href="${pageContext.request.contextPath}/user/order.html" class="btn btn-info">Back to List</a></td>
									  		</tr>
									  		
									  		
									  		
									  	</table>
									  </div>
									 </div>
		            			</div>
		            		</div>
						  </div>
						</div>
				  	</div>
				  	
				  </div>
					<div id="main-content">
						<div class="main-content">
							   
							
						
						</div>
					</div>

				</div>
				<div id="bottom" class="bottom-container">
					<div class="container"></div>
				</div>

				<jsp:include page="${request.contextPath}/foot"></jsp:include>
				<script type="text/javascript">
				'use strict';

				app.controller('OrderDetailController',['$scope','$http','Upload','$timeout',function($scope, $http, Upload, $timeout){
					  
					$scope.getListItem = function(){
						$scope.listItem = [];
						$http({
				 			method: 'GET',
						    url: baseUrl+"api/order/get/${id}",
						    headers: {
					    		'Accept': 'application/json',
					    		'Content-Type': 'application/json'
						    },
						    ignoreLoadingBar: true
						}).success(function(response) {
							dis(response);
							if(response.MESSAGE == 'SUCCESS'){
								$scope.listItem = response.ORDER;
								$scope.listItemDetail = response.DETAIL;
							}
						});	
					}
					$scope.getListItem();
					
					
				}]);


				</script>
			</div>
			<jsp:include page="${request.contextPath}/footer"></jsp:include>
		</div>
	</div>
</body>
</html>