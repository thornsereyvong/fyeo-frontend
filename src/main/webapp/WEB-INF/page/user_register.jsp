<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="${request.contextPath}/head"></jsp:include>
<body class="templateCustomersLogin">
<div class="boxes-wrapper">
  <div class="mobile-version visible-xs visible-sm">
    <jsp:include page="${request.contextPath}/header"></jsp:include>
    <div id="page-body">
      <jsp:include page="${request.contextPath}/menu"></jsp:include>
      

      <div id="body-content">
         <div class="container">
        <div id="main-content">
          <div id="main-content">
            <div class="main-content">
              <div id="col-main" class="page-login">
                <div class="row">
                 
                  <div class="col-sm-12 col-xs-12 login-or">
                    <div class="form-wrapper">
                      <h2 class="heading">Create New Account</h2>
                      <p>Create your own Electro Account</p>
                      <form method="post" action="https://electro-theme-02.myshopify.com/account" id="create_customer" accept-charset="UTF-8">
                        <input type="hidden" name="form_type" value="create_customer" />
                        <input type="hidden" name="utf8" value="✓" />
                        <div id="register-form">
                          <div class="control-wrapper">
                            <label for="first-name">First Name</label>
                            <input type="text" name="customer[first_name]" id="first-name" placeholder="First Name"  autocapitalize="words" autofocus />
                          </div>
                          <div class="control-wrapper">
                            <label for="last-name">Last Name</label>
                            <input type="text" name="customer[last_name]" id="last-name" placeholder="Last Name"  autocapitalize="words" autofocus />
                          </div>
                          <div class="control-wrapper">
                            <label for="email">Email Address<span class="req">*</span></label>
                            <input type="email" value="" name="customer[email]" id="email" placeholder="Email Address" />
                          </div>
                          <div class="control-wrapper">
                            <label for="password">Password<span class="req">*</span></label>
                            <input type="password" value="" name="customer[password]" id="password" placeholder="Password" class="password" />
                          </div>
                          <div class="control-wrapper last">
                            <button class="btn btn-default" type="submit">Register</button>
                          </div>
                        </div>
                      </form>
                      <h2 class="semi-bold">Sign up today and you'll be able to :</h2>
                      <ul class="list-unstyled list-benefits">
                        <li><i class="fa fa-check main-color"></i> Speed your way through the checkout</li>
                        <li><i class="fa fa-check main-color"></i> Track your orders easily</li>
                        <li><i class="fa fa-check main-color"></i> Keep a record of all your purchases</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <script type="text/javascript">
	  if (window.location.hash == '#recover') { showRecoverPasswordForm(); }
	
	  function showRecoverPasswordForm() {
	    $('#recover-password').fadeIn();
	    $('#customer-login').hide();
	    window.location.hash = '#recover';
	    return false;
	  }
	
	  function hideRecoverPasswordForm() {
	    $('#recover-password').hide();
	    $('#customer-login').fadeIn();
	    window.location.hash = '';
	    return false;
	  }
</script> 
            </div>
          </div>
        </div>
      </div>
     </div>
      
      <div id="bottom" class="bottom-container">
        <div class="container"></div>
      </div>
      <jsp:include page="${request.contextPath}/foot"></jsp:include>
    </div>
    <jsp:include page="${request.contextPath}/footer"></jsp:include>
  </div>
</div>
</body>
</html>