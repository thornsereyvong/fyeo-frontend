<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="${request.contextPath}/head"></jsp:include>


<body class="customer-logged-in templateCart">
	<div class="boxes-wrapper" ng-controller="UserProfileController">
		<div class="mobile-version visible-xs visible-sm">
			<jsp:include page="${request.contextPath}/header"></jsp:include>

			<div id="page-body">
				<jsp:include page="${request.contextPath}/menu"></jsp:include>
				<div id="body-content">
				  <div class="container"> 
				  	<jsp:include page="${request.contextPath}/user-admin"></jsp:include>
				  	
				  		<div class="col-md-10">
				  		<div class="panel panel-default">
						  <div class="panel-heading">
						    <h3 class="panel-title">My Profile</h3>
						  </div>
						  <div class="panel-body">
						   			
						   			<form>
							      
							        <div class="row">
							       
							        
							            <div class="col-md-6" style="padding=0.5em;">
							                <div class="card">
							                    <div class="card-body">
							                        <h2 class="card-title">Information</h2>
							                        <hr>
							                        <div class="form-group">
							                            <label for="email" class="col-form-label">Shop Name</label>
							                            <input type="email" class="form-control" id="shopName" placeholder=""   required>
							                            <div class="email-feedback">
							                            
							                            </div>
							                        </div>
							                        <div class="form-group">
							                            <label for="email" class="col-form-label">Email</label>
							                            <input type="email" class="form-control" id="email" placeholder="example@gmail.com"   required>
							                            <div class="email-feedback">
							                            
							                            </div>
							                        </div>
							                        <div class="form-group">
							                            <label for="tel" class="col-form-label">Tel 1</label>
							                            <input type="text" class="form-control" id="tel1" placeholder="+33 6 99 99 99 99" required>
							                            <div class="phone-feedback">
							                            
							                            </div>
							                        </div>
							                        <div class="form-group">
							                            <label for="tel" class="col-form-label">Tel 2</label>
							                            <input type="text" class="form-control" id="tel2" placeholder="+33 6 99 99 99 99" required>
							                            <div class="phone-feedback">
							                            
							                            </div>
							                        </div>
							                    </div>
							                </div>
							            </div>
							                
							            <div class="col-md-6">
							                <div class="card"> 
							                    <div class="card-body">
							                        <h2 class="card-title">Address</h2>
							                         <hr>
							                        <div class="form-group">
							                            <label for="password" class="col-form-label">Address</label>
							                            <input type="text" class="form-control" id="address"  required>
							                            <div class="password-feedback">
							                            
							                            </div>
							                        </div>
							                        <div class="form-group">
							                            <label for="password_conf" class="col-form-label">Description</label>
							                            <input type="text" class="form-control" id="description"  required>
							                            <div class="password_conf-feedback">
							                            
							                            </div>
							                        </div>
							                    </div>
							                </div>
							            </div>
							            
							            <div class="clearfix"></div>
							            
							             <div class="col-md-12">
							                <div class="card"> 
							                    <div class="card-body">
							                        <h2 class="card-title">Ship Address</h2>
							                        <table class="table">
							                        	
							                        	<tr ng-if="myprofile.ship1 != ''">
							                        		<td>Ship Address 1</td>
							                        		<td>{{myprofile.ship1}}</td>
							                        	</tr>
							                        	<tr ng-if="myprofile.ship2 != ''">
							                        		<td>Ship Address 2</td>
							                        		<td>{{myprofile.ship2}}</td>
							                        	</tr>
							                        	<tr ng-if="myprofile.ship3 != ''">
							                        		<td>Ship Address 3</td>
							                        		<td>{{myprofile.ship3}}</td>
							                        	</tr>
							                        	<tr ng-if="myprofile.ship4 != ''">
							                        		<td>Ship Address 4</td>
							                        		<td>{{myprofile.ship4}}</td>
							                        	</tr>
							                        	<tr ng-if="myprofile.ship5 != ''">
							                        		<td>Ship Address 5</td>
							                        		<td>{{myprofile.ship5}}</td>
							                        	</tr>
							                        	<tr ng-if="myprofile.ship6 != ''">
							                        		<td>Ship Address 6</td>
							                        		<td>{{myprofile.ship6}}</td>
							                        	</tr>
							                        	<tr ng-if="myprofile.ship7 != ''">
							                        		<td>Ship Address 7</td>
							                        		<td>{{myprofile.ship7}}</td>
							                        	</tr>
							                        	<tr ng-if="myprofile.ship8 != ''">
							                        		<td>Ship Address 8</td>
							                        		<td>{{myprofile.ship8}}</td>
							                        	</tr>
							                        	<tr ng-if="myprofile.ship9 != ''">
							                        		<td>Ship Address 9</td>
							                        		<td>{{myprofile.ship9}}</td>
							                        	</tr>
							                        	<tr ng-if="myprofile.ship10 != ''">
							                        		<td>Ship Address 10</td>
							                        		<td>{{myprofile.ship10}}</td>
							                        	</tr>
							                        </table>
							                    </div>
							                </div>
							            </div>
							            
							        </div>
							        
							        </form>
						   			
						   			
						   			
						   			
						  </div>
						</div>
				  	</div>
				  	
				  	
				  </div>
					<div id="main-content">
						<div class="main-content">
							   
							
						
						</div>
					</div>

				</div>
				<div id="bottom" class="bottom-container">
					<div class="container"></div>
				</div>

				<jsp:include page="${request.contextPath}/foot"></jsp:include>
			</div>
			<jsp:include page="${request.contextPath}/footer"></jsp:include>
		</div>
	</div>
	
	
	<script type="text/javascript">
	'use strict';

	app.controller('UserProfileController',['$scope','$http','Upload','$timeout',function($scope, $http, Upload, $timeout){
		
		$scope.getProfile = function(){
			$scope.myprofile= [];
					
			$http({
	 			method: 'GET',
			    url: baseUrl+"api/shop/get",
			    headers: {
			    		'Accept': 'application/json',
			        	'Content-Type': 'application/json'
			    },
			    ignoreLoadingBar: true
			}).success(function(response) {
				dis(response);
				if(response.MESSAGE == 'FOUND'){
					$scope.myprofile = response.RECORDS;
				
					$("#email").val($scope.myprofile.email);
					$("#tel1").val($scope.myprofile.tel1);
					$("#tel2").val($scope.myprofile.tel2);
					$("#shopName").val($scope.myprofile.shopName);
					$("#address").val($scope.myprofile.address);
					$("#description").val($scope.myprofile.description);
				}
			});	
		}
		$scope.getProfile();
		
		
	}]);




	
	</script>
</body>
</html>