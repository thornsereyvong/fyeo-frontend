<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="${request.contextPath}/head"></jsp:include>

<style>
.container{max-width:1170px; margin:auto;}
img{ max-width:100%;}
.inbox_people {
  background: #f8f8f8 none repeat scroll 0 0;
  float: left;
  overflow: hidden;
  width: 40%; border-right:1px solid #c4c4c4;
}
.inbox_msg {
  border: 1px solid #c4c4c4;
  clear: both;
  overflow: hidden;
}
.top_spac{ margin: 20px 0 0;}


.recent_heading {float: left; width:40%;}
.srch_bar {
  display: inline-block;
  text-align: right;
  width: 60%; padding:
}
.headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

.recent_heading h4 {
  color: #05728f;
  font-size: 21px;
  margin: auto;
}
.srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
.srch_bar .input-group-addon button {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  padding: 0;
  color: #707070;
  font-size: 18px;
}
.srch_bar .input-group-addon { margin: 0 0 0 -27px;}

.chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
.chat_ib h5 span{ font-size:13px; float:right;}
.chat_ib p{ font-size:14px; color:#989898; margin:auto}
.chat_img {
  float: left;
  width: 11%;
}
.chat_ib {
  float: left;
  padding: 0 0 0 15px;
  width: 88%;
}

.chat_people{ overflow:hidden; clear:both;}
.chat_list {
  border-bottom: 1px solid #c4c4c4;
  margin: 0;
  padding: 18px 16px 10px;
}
.inbox_chat { height: 550px; overflow-y: scroll;}

.active_chat{ background:#ebebeb;}

.incoming_msg_img {
  display: inline-block;
  width: 6%;
}
.received_msg {
  display: inline-block;
  padding: 0 0 0 10px;
  vertical-align: top;
  width: 92%;
 }
 .received_withd_msg p {
  background: #ebebeb none repeat scroll 0 0;
  border-radius: 3px;
  color: #646464;
  font-size: 14px;
  margin: 0;
  padding: 5px 10px 5px 12px;
  width: 100%;
}
.time_date {
  color: #747474;
  display: block;
  font-size: 12px;
  margin: 8px 0 0;
}
.received_withd_msg { width: 57%;}
.mesgs {
  float: left;
  padding: 30px 15px 0 25px;
  width: 60%;
}

 .sent_msg p {
  background: #05728f none repeat scroll 0 0;
  border-radius: 3px;
  font-size: 14px;
  margin: 0; color:#fff;
  padding: 5px 10px 5px 12px;
  width:100%;
}
.outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
.sent_msg {
  float: right;
  width: 46%;
}
.input_msg_write input {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  color: #4c4c4c;
  font-size: 15px;
  min-height: 48px;
  width: 100%;
}

.type_msg {border-top: 1px solid #c4c4c4;position: relative;}
.msg_send_btn {
  background: #05728f none repeat scroll 0 0;
  border: medium none;
  border-radius: 50%;
  color: #fff;
  cursor: pointer;
  font-size: 17px;
  height: 33px;
  position: absolute;
  right: 0;
  top: 11px;
  width: 33px;
}
.messaging { padding: 0 0 50px 0;}
.msg_history {
  height: 516px;
  overflow-y: auto;
}
</style>
<body class="customer-logged-in templateCart" ng-controller="inboxController">
	<div class="boxes-wrapper">
		<div class="mobile-version visible-xs visible-sm">
			<jsp:include page="${request.contextPath}/header"></jsp:include>

			<div id="page-body">
				<jsp:include page="${request.contextPath}/menu"></jsp:include>
				<div id="body-content">
				  <div class="container"> 
				  	<jsp:include page="${request.contextPath}/user-admin"></jsp:include>
				  	
				  	<div class="col-md-10">
				  		<div class="panel panel-default">
						  <div class="panel-heading">
						    <h3 class="panel-title">Inbox</h3>
						  </div>
						  <div class="panel-body">
						   
						   <div class="messaging">
						      <div class="inbox_msg">
						        <div class="inbox_people">
						          <div class="headind_srch">
						            <div class="recent_heading">
						              <h4>Inbox</h4>
						            </div>
						            <div class="srch_bar">
						              <div class="stylish-input-group">
						                <input type="text" style="width: 100%;" class="search-bar" ng-model="search" ng-change="searchOfCode()"   placeholder="Search" >
						                </div>
						            </div>
						          </div>
						          <div class="inbox_chat">
						            <div class="chat_list" ng-repeat="inb in inboxs" ng-click="findMessageById(inb.itemId)" style="cursor: pointer;">
						              <div class="chat_people"  >
						                <div class="chat_img"> <img ng-src="{{inb.itemImgUrl}}" alt="sunil"> </div>
						                <div class="chat_ib">
						                  <h5>{{inb.itemName}} <span class="chat_date">{{inb.inboxDate}}</span></h5>
						                  <p>{{inb.inboxMsg}}</p>
						                </div>
						              </div>
						            </div>
						          </div>
						        </div>
						         <div class="mesgs">
						          <div class="msg_history">
						          	  <table ng-if="listItemInbox.length != 0">
						          	  	<tr>
						          	  		<td style="    padding: 0;"><img ng-src="{{listItemInbox[0].itemImgUrl}}" style="width: 120px;"></td>
						          	  		<td style="    padding: 0;">Brand: {{listItemInbox[0].itemBrand}}</td>
						          	  	</tr>
						          	  </table>
						          	  <div  ng-repeat="ob in listItemInbox" style="    margin-top: 15px;">
							            <div class="incoming_msg">
							              <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">
							               </div>
							              <div class="received_msg">
							                <div class="received_withd_msg">
							                  <p>{{ob.inboxMsg}}</p>
							                  <span class="time_date"> {{ob.inboxDate}}</span></div>
							              </div>
							            </div>
							            <div class="outgoing_msg" ng-repeat="data in ob.inboxReply">
							              <div class="sent_msg" >
							                <p style="color:#fff"  ng-bind-html="data.inboxMsg"></p>
							                <span class="time_date">{{data.inboxDate}}</span> </div>
							            </div>
							         </div>  
						           
						          </div>
						          <div class="type_msg">
						            <div class="input_msg_write">
						              <input type="text" class="write_msg" ng-model="message" id="message" placeholder="Type a message" />
						              <button class="msg_send_btn" type="button" ng-click="AddInboxItem(message)"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
						            </div>
						          </div>
						        </div>
						      </div>
						    </div>
						  </div>
						</div>
				  	</div>
				  </div>
					<div id="main-content">
						<div class="main-content">
						</div>
					</div>
				</div>
				<div id="bottom" class="bottom-container">
					<div class="container"></div>
				</div>

				<jsp:include page="${request.contextPath}/foot"></jsp:include>
			</div>
			<jsp:include page="${request.contextPath}/footer"></jsp:include>
		</div>
	</div>
	<script type="text/javascript" src="${pageContext.request.contextPath}/additional/angular/ng-infinite-scroll.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/additional/ControllerJs/InboxController.js"></script>
</body>
</html>