<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="${request.contextPath}/head"></jsp:include>


<body class="customer-logged-in templateCart">
	<div class="boxes-wrapper">
		<div class="mobile-version visible-xs visible-sm">
			<jsp:include page="${request.contextPath}/header"></jsp:include>

			<div id="page-body" ng-controller="OrderController">
				<jsp:include page="${request.contextPath}/menu"></jsp:include>
				<div id="body-content">
				  <div class="container"> 
				  	<jsp:include page="${request.contextPath}/user-admin"></jsp:include>
				  	
				  	<div class="col-md-10">
				  		<div class="panel panel-default">
						  <div class="panel-heading">
						    <h3 class="panel-title">Orders</h3>
						  </div>
						  <div class="panel-body">
						  	<div class="row">
							  <div class="col-sm-3 form-group">
									<div class="form-group">
										<input style="    height: 34px;" type="text" class="form-control ng-pristine ng-valid" ng-model="search" ng-change="searchOfCode()" placeholder="Search">
									</div>
								</div>
								<div class="col-sm-2 col-sm-offset-7 ">
									<div class="form-group pull-right">
										<div class="pull-right">
							        		<select class="form-control focus ng-pristine ng-valid" ng-model="pageSize.row" id="row" ng-options="obj.value as obj.label for obj in pageSize.rows"></select>
										</div>
									</div>
								</div>
						  </div>
						   	<table class="table table-hover">
						   		<thead>
									<tr>
										<th class="width-75 text-center">No</th>
										<th>Order Date</th>
										<th>Tracking Number</th>
										<th>Ship Address</th>
										<th class="min-width-90">Total Amount</th>
										<th class="min-width-90">Status</th>
										<th>Action</th>
									</tr>
								</thead>
								 <tr  dir-paginate="ob in listItem | itemsPerPage: pageSize.row" total-items="totalItem" current-page="pagination.current">
									<td>{{$index+1}}</td>
									<td>{{ob.cDate}}</td>
									<td>{{ob.trackNum}}</td>
									<td>{{ob.shipAddress}}</td>
									<td>$ {{ob.amount | number:2}}</td>
									<td>{{ob.status}}</td>
									<td>  <button class="btn btn-xs btn-info" ng-click="viewOrder(ob.orderCode)" style="    font-size: 15px;  padding: 5px;"><i class="fa  fa-eye"></i></button></td>
								</tr>
								<tfoot>
									<tr>
										<td><div class="pagination-holder pagination-catalog">
						                    <div class="row">
						                      <div class="col-md-12 col-xs-12">
						                        <dir-pagination-controls boundary-links="true"  on-page-change="pageChanged(newPageNumber)"></dir-pagination-controls>
						                      </div>
						                    </div>
						                  </div></td>
									</tr>
								</tfoot>
						   	</table>
						  </div>
						</div>
				  	</div>
				  	
				  </div>
					<div id="main-content">
						<div class="main-content">
							   
							
						
						</div>
					</div>

				</div>
				<div id="bottom" class="bottom-container">
					<div class="container"></div>
				</div>

				<jsp:include page="${request.contextPath}/foot"></jsp:include>
					 <script src="${pageContext.request.contextPath}/additional/ControllerJs/OrderController.js"></script> 
			</div>
			<jsp:include page="${request.contextPath}/footer"></jsp:include>
		</div>
	</div>
</body>
</html>