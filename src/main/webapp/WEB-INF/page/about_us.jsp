<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="${request.contextPath}/head"></jsp:include>
<body class="customer-logged-in templateCart">
<div class="boxes-wrapper">
  <div class="mobile-version visible-xs visible-sm">
    <jsp:include page="${request.contextPath}/header"></jsp:include>
    <div id="page-body">
      <jsp:include page="${request.contextPath}/menu"></jsp:include>
      <div id="body-content">
        <div id="main-content">
          <div class="main-content">
            <div id="col-main" class="page-about-us">
              <div class="page-content"> 
                
                <!-- Banner -->
                
                <div class="au-banner" style="background-image: url(${pageContext.request.contextPath}/additional/images/banner2.jpg);">
                  <div class="container">
                    <div class="au-banner-text">
                      <h3>About Us</h3>
                      <p>Fyeo  (For your eyes only).</p>
                    </div>
                  </div>
                </div>
                
                <!-- 3 Columns -->
                
                <div class="au-three-column">
                  <div class="container">
                    <div class="three-column-list owl-carousel owl-theme owl-loaded">
                      <div class="owl-stage-outer">
                        <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0.25s ease 0s; width: 1200px;">
                          <div class="owl-item active" style="width: 370px; margin-right: 30px;">
                            <div class="three-column-item"> <img src="//cdn.shopify.com/s/files/1/1801/4931/t/2/assets/au_column_1.jpg?7947390190342823348" alt="">
                              <h4>What we really do?</h4>
                              <p class="caption">Donec libero dolor, tincidunt id laoreet vitae, 
                                ullamcorper eu tortor. Maecenas pellentesque, 
                                dui vitae iaculis mattis, tortor nisi faucibus magna, 
                                vitae ultrices lacus purus vitae metus.</p>
                            </div>
                          </div>
                          <div class="owl-item active" style="width: 370px; margin-right: 30px;">
                            <div class="three-column-item"> <img src="//cdn.shopify.com/s/files/1/1801/4931/t/2/assets/au_column_2.jpg?7947390190342823348" alt="">
                              <h4>Our Vision</h4>
                              <p class="caption">Donec libero dolor, tincidunt id laoreet vitae, 
                                ullamcorper eu tortor. Maecenas pellentesque, 
                                dui vitae iaculis mattis, tortor nisi faucibus magna, 
                                vitae ultrices lacus purus vitae metus.</p>
                            </div>
                          </div>
                          <div class="owl-item active" style="width: 370px; margin-right: 30px;">
                            <div class="three-column-item"> <img src="//cdn.shopify.com/s/files/1/1801/4931/t/2/assets/au_column_3.jpg?7947390190342823348" alt="">
                              <h4>History of Beginning</h4>
                              <p class="caption">Donec libero dolor, tincidunt id laoreet vitae, 
                                ullamcorper eu tortor. Maecenas pellentesque, 
                                dui vitae iaculis mattis, tortor nisi faucibus magna, 
                                vitae ultrices lacus purus vitae metus.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="owl-controls">
                        <div class="owl-nav">
                          <div class="owl-prev" style="display: none;">prev</div>
                          <div class="owl-next" style="display: none;">next</div>
                        </div>
                        <div class="owl-dots" style="display: none;"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <!-- Our Team -->
                
               
                <!-- Text Boxes -->
                
                <div class="au-text-boxes">
                  <div class="container">
                    <div class="row">
                      <div class="col-sm-8 col-xs-12">
                        <div class="text-boxes-left">
                          <div class="text-boxes-item">
                            <h4>What we really do?</h4>
                            <p>Donec libero dolor, tincidunt id laoreet vitae,  ullamcorper eu tortor. Maecenas pellentesque,  dui vitae iaculis mattis, tortor nisi faucibus magna,  vitae ultrices lacus purus vitae metus.</p>
                          </div>
                          <div class="text-boxes-item">
                            <h4>Our Vision</h4>
                            <p>Donec libero dolor, tincidunt id laoreet vitae,  ullamcorper eu tortor. Maecenas pellentesque,  dui vitae iaculis mattis, tortor nisi faucibus magna,  vitae ultrices lacus purus vitae metus.</p>
                          </div>
                          <div class="text-boxes-item">
                            <h4>History of the Company</h4>
                            <p>Donec libero dolor, tincidunt id laoreet vitae,  ullamcorper eu tortor. Maecenas pellentesque,  dui vitae iaculis mattis, tortor nisi faucibus magna,  vitae ultrices lacus purus vitae metus.</p>
                          </div>
                          <div class="text-boxes-item">
                            <h4>Cooperate with Us!</h4>
                            <p>Donec libero dolor, tincidunt id laoreet vitae,  ullamcorper eu tortor. Maecenas pellentesque,  dui vitae iaculis mattis, tortor nisi faucibus magna,  vitae ultrices lacus purus vitae metus.</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-4 col-xs-12">
                        <div class="text-boxes-right">
                          <h5>Support 24/7</h5>
                          <h5>Best Quanlity</h5>
                          <h5>Fastest Delivery</h5>
                          <h5>Customer Care</h5>
                          <h5>Over 200 Satisfied Customers</h5>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="bottom" class="bottom-container">
        <div class="container"></div>
      </div>
      <jsp:include page="${request.contextPath}/foot"></jsp:include>
    </div>
    <jsp:include page="${request.contextPath}/footer"></jsp:include>
  </div>
</div>
</body>
</html>