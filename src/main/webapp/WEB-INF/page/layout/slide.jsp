<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style>
    .swiper-slide-1487563635809-0.swiper-slide-active .heading{
      color: #333e48; font-size: 58px; -webkit-animation-name: fadeIn; animation-name: fadeIn; -webkit-animation-delay: 1s; animation-delay: 1s;
    }
    .swiper-slide-1487563635809-0.swiper-slide-active .subheading{
      color: #333e48; font-size: 15px; -webkit-animation-name: moveFromLeft; animation-name: moveFromLeft; -webkit-animation-delay: 2s; animation-delay: 2s;
    }
    .swiper-slide-1487563635809-0.swiper-slide-active .caption{
      color: #333e48; font-size: 57px; -webkit-animation-name: moveFromBottom; animation-name: moveFromBottom; -webkit-animation-delay: 3s; animation-delay: 3s;
    }
    .swiper-slide-1487563635809-0.swiper-slide-active .caption-link{
      -webkit-animation-name: moveFromLeft; animation-name: moveFromLeft; -webkit-animation-delay: 4s; animation-delay: 4s;
    }
</style>
 <style>
  .swiper-slide-1487582180485.swiper-slide-active .heading{
    color: #34bcec; font-size: 18px; -webkit-animation-name: fadeIn; animation-name: fadeIn; -webkit-animation-delay: 1s; animation-delay: 1s;
  }
  .swiper-slide-1487582180485.swiper-slide-active .subheading{
    color: #333e48; font-size: 28px; -webkit-animation-name: moveFromRight; animation-name: moveFromRight; -webkit-animation-delay: 2s; animation-delay: 2s;
  }
  .swiper-slide-1487582180485.swiper-slide-active .caption{
    color: #ffffff; font-size: 48px; -webkit-animation-name: fadeIn; animation-name: fadeIn; -webkit-animation-delay: 1s; animation-delay: 1s;
  }
  .swiper-slide-1487582180485.swiper-slide-active .caption-link{
    -webkit-animation-name: moveFromBottom; animation-name: moveFromBottom; -webkit-animation-delay: 3s; animation-delay: 3s;
  }
</style>   
<style>
     .swiper-slide-1487563635809-1.swiper-slide-active .heading{
       color: #34bcec; font-size: 18px; -webkit-animation-name: fadeIn; animation-name: fadeIn; -webkit-animation-delay: 1s; animation-delay: 1s;
     }
     .swiper-slide-1487563635809-1.swiper-slide-active .subheading{
       color: #ffffff; font-size: 48px; -webkit-animation-name: moveFromLeft; animation-name: moveFromLeft; -webkit-animation-delay: 2s; animation-delay: 2s;
     }
     .swiper-slide-1487563635809-1.swiper-slide-active .caption{
       color: #ffffff; font-size: 48px; -webkit-animation-name: fadeIn; animation-name: fadeIn; -webkit-animation-delay: 1s; animation-delay: 1s;
     }
     .swiper-slide-1487563635809-1.swiper-slide-active .caption-link{
       -webkit-animation-name: moveFromBottom; animation-name: moveFromBottom; -webkit-animation-delay: 3s; animation-delay: 3s;
     }
</style>   
          				
<div id="shopify-section-1487563635809" class="shopify-section">
              <div class="bc-wrapper">
                <div class="home-slideshow-wrapper swiper-container" data-autoplay="true" data-time="10000" data-animation="slide">
                  <div id="home-slideshow" class="swiper-wrapper">
                    <div class="swiper-slide swiper-slide-1487563635809-0" style="background-image:url(${pageContext.request.contextPath}/additional/images/_vetrine_70_TAKEDOWN2.jpg); width: auto;">
                     <a href="#"><img src = "${pageContext.request.contextPath}/additional/images/_vetrine_70_TAKEDOWN2.jpg" alt="" /></a>
                      <div class="container">
                        <div class="swiper-content text-left slide-1" style="bottom: 15%;">
                          <!-- <div class="container-box">
                            <div class="heading">The new<br>
                              Standard</div>
                            <div class="subheading">UNdER FAVORABLE SMARTWATCHES</div>
                            <div class="caption"><span class="title-price">From</span>
                              <p><span class="small-size">$</span>749<span class="small-size">99</span></p>
                            </div>
                            <div class="caption-link"> <a class="btn btn-default slider-button" href="collections.html">Start Buying</a> </div>
                           
                          </div> -->
                        </div>
                      </div>
                    </div>
                    
                   <%--  <div class="swiper-slide has-video swiper-slide-1487563635809-1" style="background-image:url(${pageContext.request.contextPath}/additional/files/1/1801/4931/files/3-1_1920xb8cd.jpg?v=1519724268); width: auto;"> <a href="collections.html"><img src = "${pageContext.request.contextPath}/additional/files/1/1801/4931/files/3-1_1920xb8cd.jpg?v=1519724268" alt="" /></a>
                      <div class="container">
                        <div class="swiper-content text-left slide-2" style="bottom: 30%;">
                          <div class="container-box">
                            <div class="heading">Shop To Get What You Love</div>
                            <div class="subheading">TimePieces that make a statement up to <span class="big-font">40% off</span></div>
                            <div class="caption-link"> <a class="btn btn-default slider-button" href="collections.html">Start Buying</a> </div>
                          
                          </div>
                        </div>
                      </div>
                      <div class="video-slide" style="display: none;">
                        <video controls class="video" autoplay loop muted="" preload="auto" data-width="0" data-height="0">
                          <source src="https://cdn.shopify.com/s/files/1/1801/4931/files/The_New_Microsoft_Surface_Pro_4.mp4?15121836849689228889" type="video/mp4">
                        </video>
                      </div>
                    </div> --%>
                   <%--  <div class="swiper-slide swiper-slide-1487582180485" style="background-image:url(${pageContext.request.contextPath}/additional/files/1/1801/4931/files/2-1_1920xb8cd.jpg?v=1519724268); width: auto;"> <a href="collections.html"><img src = "${pageContext.request.contextPath}/additional/files/1/1801/4931/files/2-1_1920xb8cd.jpg?v=1519724268" alt="" /></a>
                      <div class="container">
                        <div class="swiper-content text-left slide-3" style="bottom: 50%;">
                          <div class="container-box">
                            <div class="heading">Sell to get what you love</div>
                            <div class="subheading">TimePieces that make a statement up to <span class="big-font">40% off</span></div>
                            <div class="caption-link"> <a class="btn btn-default slider-button" href="collections.html">Start Buying</a> </div>
                           
                          </div>
                        </div>
                      </div>
                    </div> --%>
                    
                    
                  </div>
                  <div class="swiper-pagination swiper-pagination-white"></div>
                </div>
                
                
              </div>
              <script>
				  jQuery(document).ready(function($) {
					  
				    if ( bcMsieVersion.MsieVersion() == 0 ){
				      AT_Main.homeSlideshow();
				    }else{
				      if ( bcMsieVersion.MsieVersion() > 9 || !!navigator.userAgent.match(/Trident.*rv\:11\./)){
				        AT_Main.homeIE();
				      } else{
				        jQuery('#home-slideshow').owlCarousel({
				          nav: true,
				          dots: false,
				          items: 1,
				          navText: ['<i class="fa fa-angle-left" title="previous"></i>', '<i class="fa fa-angle-right" title="Next"></i>']
				        })
				      }
				    }
				
				  });
			</script> 
            </div>