<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
	<script>
	  jQuery(document).ready(function($) {
	
		  $('#quick-shop-popup').on( 'shown.bs.modal', function () {    
			    //  AT_Main.handleReviews();
			  });
		  
		  $("#close-quick-shop-popup").click(function(){
			  $('#quick-shop-popup').on( 'hide.bs.modal', function () {
			      $(".zoomContainer").remove();
			    });
		  });
	  });
	</script>
	
<div id="quick-shop-popup" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <span class="close" title="Close" data-dismiss="modal" id="close-quick-shop-popup" aria-hidden="true"></span>
      </div>

      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
				 <table class="table table-bordered">
                       <tr>
                         <td style="width: 20%;">
                         <img class="img-responsive"  src="${pageContext.request.contextPath}/additional/images/giphy.gif"  lazy-img="{{listItemDetail.image}}" alt="placehold.it/350x250" /></td>
                         <td style="width: 42%;border-left:0;border-right:0">
                         	<div class="list-group-item-text">
                             <h1 itemprop="name" content="Golddax Product Example">{{listItemDetail.brand}}</h1>
                             <h5 class="list-group-item-heading">Color: {{listItemDetailDetail.itemOption}} </h5>
                             <h5 class="list-group-item-heading">Model: {{listItemDetailDetail.modelValue}} </h5>
                           </div></td>
                         <td style="    width: 31%;">
                         	<h2> HK <small>$ {{listItemDetail.hkPrice | number:2}} </small></h2>
                           <p>Suggested retail price HK $ <small> {{listItemDetail.dollarRetailPrice | number:2}} </small> </p>
                          </td>
                       </tr>
                     </table>
                     
                     
                     
                    <%--  <table class="table table-bordered" style="margin-bottom: 30px;">
                        <tr  ng-repeat="ob in listItemDetail.detail">
                          <td style="width: 20%;border-bottom: 0">
                       			<a href="javascript:void(0);" ng-click="showImage(ob.image)" data-target="#quick-shop-popup"  data-toggle="modal" title="Quick View">   <img class="media-object img-rounded img-responsive"  src="${pageContext.request.contextPath}/additional/images/giphy.gif" 	  lazy-img="{{ob.image}}" alt="placehold.it/350x250" ></a></td>
                          <td style="width: 42%;    border-left: 0;border-right: 0; border-bottom: 0;">
                          	<div class="row">
                              <div class="col-lg-7">
                                <h4 class="list-group-item-heading"> {{ob.model}} </h4>
                                <ul class="font2 no-style-list">
                                  <li>FRONT COLOR : {{ob.frontColor}}</li>
                                  <li>FRAM Color : {{ob.framColor}}</li>
                                  <li>LEN Color : {{ob.lenColor}}</li>
                                  <li>LEN MATERIAL : {{ob.lenMaterail}}</li>
                                  <li>LAN PROPERTY : {{ob.lenProperty}}</li>
                                  <li>LAN TRANS : {{ob.lenTrans}}</li>
                                  <li>POLARIZED : {{ob.polarized}}</li>
                                </ul>
                              </div>
                              <div class="col-lg-5"> <a class="pull-right font36" ng-click="wishListAdd('${itemId}',listItemDetail.brand,ob.model)" href="javascript:void(0);"><i class="icon icon-favorites"></i></a> <br>
                                <h2 style="margin-top: 30px;"> HK <small>$ {{ob.hkPrice | number:2}} </small></h2>
                                <p class="font12">Suggested retail price <br/>
                                  HK <small> $ {{ob.dollarRetailPrice | number:2}} </small> </p>
                              </div>
                            </div>
                           </td>
                          <td  style="width: 31%;border-bottom: 0">
                          	<div class="row">
                              <h5>QUANTITY</h5>
                              <hr/>
                              <div id="product-action-9923383239" class="options" ng-repeat="st in ob.stocks">
                                <div class="qty-add-cart">
                                  <div class="quantity-product">
                                    <div class="col-lg-3 form-group1 padding-top-8">
                                      <label>Size {{st.size}} :</label>
                                    </div>
                                    <div class="col-lg-5">
                                      <div class="quantity form-group1" >
                                        <input type="text" ng-model="st.qty" class="item-quantity" min="1" value="{{st.qty}}" ng-change="changeNum(st)" name="quantity" >
                                        <span class="qty-wrapper"> <span class="qty-inner"> <span class="qty-up" title="Increase" ng-click="(st.qty=st.qty+1)" data-src="#quantity"> <i class="fa fa-plus"></i> </span> <span class="qty-down" title="Decrease" ng-click="(st.qty == 0) ? 0:(st.qty=st.qty-1)"  data-src="#quantity"> <i class="fa fa-minus"></i> </span> </span> </span> </div>
                                    </div>
                                    <div class="col-lg-1" style=" padding-top: 5px;"> <span class="label label-success custom_stock" ng-if="st.skStatus == 1">&nbsp;</span> <span class="label label-warning custom_stock" ng-if="st.skStatus == 0">&nbsp;</span> </div>
                                    <div ng-if="st.qty > 0" class="col-lg-12 animationIf"   >
                                      <div class="row">
                                        <div class="col-lg-12">
                                          <label class="font2" style="padding-left: 5px;">CUSTOMER REFERENCE max 250 characters</label>
                                        </div>
                                        <div class="col-lg-6">
                                          <div class="form-group">
                                            <textarea rows=""  class="form-control " ng-model="st.comment" style="resize: none;width: 200px;height: 70px;max-height:70px;min-height:70;min-width:200px;max-width:200px; padding: 9px; margin-top: 6px;" cols=""></textarea>
                                          </div>
                                        </div>
                                        <div class="col-lg-6 text-right">
                                          <div class="form-group">
                                            <button ng-click="cartAdd('${itemId}',listItemDetail.brand,ob.model,st.size,st.qty,ob.hkPrice,st.comment)" class="btn custom_btn_add_cart btn-xs btn-default"><span class="icon icon-add-to-cart"></span> &nbsp; Add To Cart</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="clearfix"> </div>
                                  </div>
                                </div>
                              </div>
                            </div></td>
                        </tr>
                      </table> --%>
                      
                      
                       <div class="form-group pull-right">
                        <button ng-click="cartAddList('${itemId}',listItemDetail.brand)" class="btn  btn-xs btn-default"><span class="icon icon-add-to-cart"></span> &nbsp; Add To Cart</button>
                      </div>
           

          </div>
        </div>
      </div>

    </div>
  </div>
</div>



	