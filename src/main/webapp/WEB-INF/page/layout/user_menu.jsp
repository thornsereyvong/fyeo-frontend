<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div id="sidebar" class="col-md-2 hidden-sm hidden-xs">
	<div class="sidebar sidebar-catalog"> 
					                    
            <div class="sb-categories">
              <div class="sb-widget">
                <h4 class="sb-title">User Menu</h4>
                <ul class="sb-categories-list">
                  <li class="title"> <a href="${pageContext.request.contextPath}/user/order.html"> <span>My Order</span> </a> </li>
                  <li class="title"> <a href="${pageContext.request.contextPath}/user/inbox.html"> <span>Inbox</span> </a> </li>
                  <li class="title"> <a href="${pageContext.request.contextPath}/user/wishlist.html"> <span>Wishlist</span> </a> </li>
                  <li class="title"> <a href="${pageContext.request.contextPath}/user/cart.html"> <span>Cart</span> </a> </li>
                  <li class="title"> <a href="${pageContext.request.contextPath}/user/profile.html"> <span>Profile</span> </a> </li>
                </ul>
              </div>
            </div>
            
            </div>
</div>