<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<footer id="footer-content" ng-controller="footerController"> 
      
    <!--   <div id="widget-newsletter">
        <div class="container">
          <div class="newsletter-container">
            <div class="newsletter-title"><i class="icon icon-newsletter"></i><span>Sign up to Newsletter</span></div>
            <p>...and receive <span>$20 coupon for first shopping.</span></p>
            <form action="http://bitcode.us10.list-manage.com/subscribe/post?u=55ec8b9611a3d9c0ad6f3fc62&amp;id=1cbb85b057" method="post" id="mc-embedded-subscribe-form" class="form-inline form-subscribe" name="mc-embedded-subscribe-form" target="_blank">
              <input class="form-control" type="email" required placeholder="Enter your email address" name="EMAIL" id="email-input" />
              <button id="email-submit" type="submit" title="Subscibe" class="btn-custom">Sign Up</button>
            </form>
          </div>
        </div>
      </div> -->

      
      <div class="footer-top">
        <div class="container">
          <div class="row">
            <div id="footer-contact" class="col-md-5 col-sm-12 col-xs-12">
              <div class="footer-logo"> <a href="index.html" title="Electro store" class="logo-site"> <img class="logo" src="${pageContext.request.contextPath}/additional/images/logo.png" alt="Electro store" /> </a> </div>
              <div class="f-support-box"> <span class="icon icon-support"> </span>
                <div class="f-support-text"> <span class="f-text-1">Phone</span> <span class="f-text-2"> (+100) 123 456 7890</span> </div>
              </div>
              <div class="f-address"> <span class="f-text-3">Address:</span> <span class="f-text-4"> 45 Grand Central Terminal New York, NY 1017 United State USA</span> </div>
              <div id="widget-social">
                <ul class="widget-social list-inline">
                  <li> <a target="_blank" href="#" class="social-icon-inner facebook" title="Facebook"> <span class="social-icon"> <i class="fa fa-facebook"></i> </span> </a> </li>
                  <li> <a target="_blank" href="#" class="social-icon-inner twitter" title="Twitter"> <span class="social-icon"> <i class="fa fa-twitter"></i> </span> </a> </li>
                  <li> <a target="_blank" href="#" class="social-icon-inner instagram" title="Instagram"> <span class="social-icon"> <i class="fa fa-instagram"></i> </span> </a> </li>
                  <li> <a target="_blank" href="#" class="social-icon-inner rss" title="Rss"> <span class="social-icon"> <i class="fa fa-rss"></i> </span> </a> </li>
                  <li> <a target="_blank" href="#" class="social-icon-inner google" title="Google"> <span class="social-icon"> <i class="fa fa-google-plus"></i> </span> </a> </li>
                  <li> <a target="_blank" href="#" class="social-icon-inner pinterest" title="Pinterest"> <span class="social-icon"> <i class="fa fa-pinterest"></i> </span> </a> </li>
                  <li> <a target="_blank" href="#" class="social-icon-inner Dribbble" title="Dribbble"> <span class="social-icon"> <i class="fa fa-dribbble"></i> </span> </a> </li>
                  <li> <a target="_blank" href="#" class="social-icon-inner linkedin" title="Linkedin"> <span class="social-icon"> <i class="fa fa-linkedin"></i> </span> </a> </li>
                </ul>
              </div>
            </div>
            <div class="col-xs-12 col-md-7">
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="footer-block">
                    <h3>All Brands<span class="visible-xs"></span></h3>
                    <ul class="list-links list-unstyled">
                       <li ng-repeat="ob in listDepart" ng-class="($index == 0) ? 'active':''">
                        		 <a href="#"> <span>{{ob.brandName}}</span> </a> 
                        </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="footer-block">
                    <h3>Information<span class="visible-xs"></span></h3>
                    <ul class="list-links list-unstyled">
                      <li> <a href="${pageContext.request.contextPath}/about-us.html">About Us</a> </li>
                      <li> <a href="${pageContext.request.contextPath}/contact-us.html">Contact Us</a> </li>
                      <li> <a href="${pageContext.request.contextPath}/privancy-policy.html">Privacy policy</a> </li>
                      <li> <a href="${pageContext.request.contextPath}/term-condition.html">Terms &amp; condition</a> </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="footer-block">
                    <h3>My Account<span class="visible-xs"></span></h3>
                    <ul class="list-links list-unstyled">
                      <li> <a href="${pageContext.request.contextPath}/user/profile.html">Profile</a> </li>
                      <li> <a href="${pageContext.request.contextPath}/user/order.html">My Order</a> </li>
                      <li> <a href="${pageContext.request.contextPath}/user/inbox.html">Inbox</a> </li>
                      <li> <a href="${pageContext.request.contextPath}/user/cart.html">Cart</a> </li>
                      <li> <a href="${pageContext.request.contextPath}/user/wishlist.html">Wishlist</a> </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      

      
      <div class="footer-copyright">
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-xs-12">
              <div class="copyright">
                <p>&copy;<a href="index.html">Fyeo</a>. All Rights Reserved</p>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12">
              <ul class="payment-icons list-inline">
                <li><img src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/payment_177cd.png?4608748305671055686" alt=""></li>
                <li><img src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/payment_277cd.png?4608748305671055686" alt=""></li>
                <li><img src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/payment_377cd.png?4608748305671055686" alt=""></li>
                <li><img src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/payment_477cd.png?4608748305671055686" alt=""></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      
      <script type="text/javascript">
      	app.controller('footerController',['$scope','$http','Upload','$timeout',function($scope, $http, Upload, $timeout){


      		$scope.getListDepartment = function(){
      			$scope.listDepart = [];
      			$http({
      	 			method: 'GET',
      			    url: baseUrl+"public/brand/list",
      			    headers: {
      			    		'Accept': 'application/json',
      			        'Content-Type': 'application/json'
      			    },
      			    ignoreLoadingBar: true
      			}).success(function(response) {
      				if(response.MESSAGE == 'SUCCESS'){
      					$scope.listDepart = response.RECORDS;
      				}
      			});	
      		}
      		
      		$scope.getListDepartment();
      		
      		
    		
    		
    	}]);
      </script>
      
    <script>

var Shopify = Shopify || {};
Shopify.optionsMap = {};
Shopify.updateOptionsInSelector = function(selectorIndex) {
    
  switch (selectorIndex) {
    case 0:
      var key = 'root';
      var selector = jQuery('.single-option-selector:eq(0)');
      break;
    case 1:
      var key = jQuery('.single-option-selector:eq(0)').val();
      var selector = jQuery('.single-option-selector:eq(1)');
      break;
    case 2:
      var key = jQuery('.single-option-selector:eq(0)').val();  
      key += ' / ' + jQuery('.single-option-selector:eq(1)').val();
      var selector = jQuery('.single-option-selector:eq(2)');
  }
  
  var initialValue = selector.val();
  selector.empty();    
  var availableOptions = Shopify.optionsMap[key];
  for (var i=0; i<availableOptions.length; i++) {
    var option = availableOptions[i];
    var newOption = jQuery('<option></option>').val(option).html(option);
    selector.append(newOption);
  }
  jQuery('.swatch[data-option-index="' + selectorIndex + '"] .swatch-element').each(function() {
    if (jQuery.inArray($(this).attr('data-value'), availableOptions) !== -1) {
      $(this).removeClass('soldout').show().find(':radio').removeAttr('disabled','disabled').removeAttr('checked');
    }
    else {
      $(this).addClass('soldout').hide().find(':radio').removeAttr('checked').attr('disabled','disabled');
    }
  });
  if (jQuery.inArray(initialValue, availableOptions) !== -1) {
    selector.val(initialValue);
  }
  selector.trigger('change');  
  
};
Shopify.linkOptionSelectors = function(product) {
  // Building our mapping object.
  for (var i=0; i<product.variants.length; i++) {
    var variant = product.variants[i];
    if (variant.available) {
      // Gathering values for the 1st drop-down.
      Shopify.optionsMap['root'] = Shopify.optionsMap['root'] || [];
      Shopify.optionsMap['root'].push(variant.option1);
      Shopify.optionsMap['root'] = Shopify.uniq(Shopify.optionsMap['root']);
      // Gathering values for the 2nd drop-down.
      if (product.options.length > 1) {
        var key = variant.option1;
        Shopify.optionsMap[key] = Shopify.optionsMap[key] || [];
        Shopify.optionsMap[key].push(variant.option2);
        Shopify.optionsMap[key] = Shopify.uniq(Shopify.optionsMap[key]);
      }
      // Gathering values for the 3rd drop-down.
      if (product.options.length === 3) {
        var key = variant.option1 + ' / ' + variant.option2;
        Shopify.optionsMap[key] = Shopify.optionsMap[key] || [];
        Shopify.optionsMap[key].push(variant.option3);
        Shopify.optionsMap[key] = Shopify.uniq(Shopify.optionsMap[key]);
      }
    }
  }
  // Update options right away.
  Shopify.updateOptionsInSelector(0);
  if (product.options.length > 1) Shopify.updateOptionsInSelector(1);
  if (product.options.length === 3) Shopify.updateOptionsInSelector(2);
  // When there is an update in the first dropdown.
  jQuery(".single-option-selector:eq(0)").change(function() {
    Shopify.updateOptionsInSelector(1);
    if (product.options.length === 3) Shopify.updateOptionsInSelector(2);
    return true;
  });
  // When there is an update in the second dropdown.
  jQuery(".single-option-selector:eq(1)").change(function() {
    if (product.options.length === 3) Shopify.updateOptionsInSelector(2);
    return true;
  });  
};
 

  
</script>  
<script type="text/javascript">
  
  var _bc_config = {
    "home_slideshow_auto_delay" 	: ""
    ,"enable_multiple_currencies" 	: "true"
    ,"money_format" 				: '<span class=money>0</span>'
    ,"enable_image_blance" 			: "true"
    ,"image_blance_width" 			: "250"
    ,"image_blance_height" 			: "230"
    ,"enable_title_blance"			: "true"
  };
    
</script> 
 
<script src="${pageContext.request.contextPath}/additional/angular/angular-strap.min.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/angular-strap.tpl.min.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/alert.min.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/alert.tpl.min.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/angular-material.min.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/angular-animate.min.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/loading-bar.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/angular-aria.min.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/angular-messages.min.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/angular-sanitize.min.js" data-semver="1.5.5"></script>
<script src="${pageContext.request.contextPath}/additional/angular/FileSaver.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/svg-assets-cache.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/dirPagination.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/sweetalert.min.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/ng-file-upload.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/ng-file-upload-shim.js"></script>
<script src="${pageContext.request.contextPath}/additional/angular/angular-lazy-img.min.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/eu-require-opt-in77cd.js?4608748305671055686"></script> 
      <div id="checkLayout"> <span class="visible-xs-block"></span> <span class="visible-sm-block"></span> <span class="visible-md-block"></span> <span class="visible-lg-block"></span> </div>
</footer>