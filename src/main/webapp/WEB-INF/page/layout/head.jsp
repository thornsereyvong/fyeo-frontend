<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html ng-app="GlassApp" >
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
<link rel="shortcut icon" href="${pageContext.request.contextPath}/additional/images/favicon77cd.png" type="image/png" />
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<link rel="canonical" href="index.html" />
<title>${title} | Fyeo</title>
<meta property="og:type" content="website" />
<meta property="og:title" content="Electro store" />
<meta property="og:url" content="index.html" />
<meta property="og:site_name" content="Electro store" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/additional/javascripts/daterangepicker-bs3.css"/>
<link href="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/jquery.swiper77cd.css?4608748305671055686" rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/bootstrap.3x77cd.css?4608748305671055686" rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/jquery.plugin77cd.css?4608748305671055686" rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/electro77cd.css?4608748305671055686" rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/bc.global.scss77cd.css?4608748305671055686" rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/bc.style.scss77cd.css?4608748305671055686" rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/bc.responsive.scss77cd.css?4608748305671055686" rel="stylesheet" type="text/css" media="all" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel='stylesheet' type='text/css'>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel='stylesheet' type='text/css'>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="${pageContext.request.contextPath}/additional/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/additional/angular/sweetalert.css">

<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/jquery-1.9.1.min77cd.js?4608748305671055686" ></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/bootstrap.3x.min77cd.js?4608748305671055686" defer></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/jquery.easing.1.377cd.js?4608748305671055686" defer></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/modernizr77cd.js?4608748305671055686"  async></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/jquery.swiper77cd.js?4608748305671055686"></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/cookies77cd.js?4608748305671055686"></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/jquery.fancybox.min77cd.js?4608748305671055686"></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/jquery.jgrowl.min77cd.js?4608748305671055686" defer></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/jquery.elevatezoom.min77cd.js?4608748305671055686" defer></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/jquery.owl.carousel.min77cd.js?4608748305671055686" defer></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/jquery.plugin77cd.js?4608748305671055686" defer></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/jquery.countdown77cd.js?4608748305671055686" defer></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/cart77cd.js?4608748305671055686" defer></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/rivets-cart.min77cd.js?4608748305671055686" defer></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/bc.ajax-search77cd.js?4608748305671055686" defer></script>
<%-- <script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/option_selection77cd.js?4608748305671055686" defer></script> --%>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/jquery.fakecrop77cd.js?4608748305671055686" defer></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/bc.global77cd.js?4608748305671055686" defer></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/bc.slider77cd.js?4608748305671055686"></script>
<script src="${pageContext.request.contextPath}/additional/files/1/1801/4931/t/2/assets/bc.script77cd.js?4608748305671055686"></script>

<script src="${pageContext.request.contextPath}/additional/javascripts/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/additional/javascripts/daterangepicker.js"></script>

<%-- <script type='text/javascript' src='${pageContext.request.contextPath}/additional/angular/angular.js'></script> --%>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-sanitize.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>

<script type='text/javascript' src='${pageContext.request.contextPath}/additional/ControllerJs/MainController.js'></script>
<script type="text/javascript">
var baseUrl = '${pageContext.request.contextPath}/';
</script>

</head>
<style>
.citemname{
color: #fed700 !important
}
.wactive{
	color: #fed700;
}
.font12{
	font-size: 10px;
}
.price-sale {
    font-size: 15px;
}
.cata-list .cata-product .product-wrapper .product-content .product-price .price-sale, .cata-list .cata-product .product-wrapper .product-content .price .price-sale {
    font-size: 15px;
}
.cata-list-small .cata-product .product-grid-item .product-wrapper .product-content .product-price .price-sale, .cata-list-small .cata-product .product-grid-item .product-wrapper .product-content .price .price-sale {
    font-size: 15px;
}


</style>
<style>
.shopify-payment-button__button--hidden {
	visibility: hidden;
}
.shopify-payment-button__button {
	border-radius: 4px;
	border: none;
	box-shadow: 0 0 0 0 transparent;
	color: white;
	cursor: pointer;
	display: block;
	font-size: 1em;
	font-weight: 500;
	line-height: 1;
	text-align: center;
	width: 100%;
	transition: background 0.2s ease-in-out;
}
.shopify-payment-button__button[disabled] {
	opacity: 0.6;
	cursor: default;
}
.shopify-payment-button__button--unbranded {
	background-color: #1990c6;
	padding: 1em 2em;
}
.shopify-payment-button__button--unbranded:hover:not([disabled]) {
	background-color: #136f99;
}
.shopify-payment-button__more-options {
	background: transparent;
	border: 0 none;
	cursor: pointer;
	display: block;
	font-size: 1em;
	margin-top: 1em;
	text-align: center;
	width: 100%;
}
.shopify-payment-button__more-options:hover:not([disabled]) {
	text-decoration: underline;
}
.shopify-payment-button__more-options[disabled] {
	opacity: 0.6;
	cursor: default;
}
.shopify-payment-button__button--branded {
	display: flex;
	flex-direction: column;
	min-height: 44px;
	position: relative;
	z-index: 1;
}
.shopify-payment-button__button--branded .shopify-cleanslate {
	flex: 1 !important;
	display: flex !important;
	flex-direction: column !important;
}


.animationIf.ng-enter, .animationIf.ng-leave {
	-webkit-transition: opacity ease-in-out 1s;
	-moz-transition: opacity ease-in-out 1s;
	-ms-transition: opacity ease-in-out 1s;
	-o-transition: opacity ease-in-out 1s;
	transition: opacity ease-in-out 1s;
}
.animationIf.ng-enter, .animationIf.ng-leave.ng-leave-active {
	opacity: 0;
}
.animationIf.ng-leave, .animationIf.ng-enter.ng-enter-active {
	opacity: 1;
}
.font36 {
	font-size: 35px;
}
.page-product .product-simple-tab .tab-content .tab-pane p, .page-product .product-extended-tab .tab-content .tab-pane p {
	margin: 0 0 5px;
}
.page-product .tab-content table td {
	text-align: center;
}
.font2 {
	font-size: 12px;
}
.textarea, textarea.form-control:active {
	background: #fff;
}
.textarea_custom {
	width: 204px;
	height: 54px;
	padding: 6px;
	margin-top: 6px;
}
.custom_btn_add_cart {
	min-width: 98px !important;
	padding: 0;
	font-size: 11px;
	width: 90px !important;
	margin-top: 47px;
}
.form-group1 {
	margin-bottom: 8px;
}
.table-bordered, .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
	border: 0.2px solid #ddd;
}
.custom_stock {
	padding: 1px 1px 1px 9px;
}
.no-style-list {
	list-style-type: none;
}
.padding-top-8{
	padding-top:8px;
}
</style>