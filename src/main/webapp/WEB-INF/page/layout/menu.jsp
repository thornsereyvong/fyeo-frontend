<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<header class="header-content" data-stick="true"> 
      <!-- Begin Top Bar -->
      
      <div class="top-bar">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="top-bar-left">
                <ul class="top-bar-contact list-inline">
                  <li class=""><span>Welcome to FYEO center.</span></li>
                  <li class=""><span>For Your Eyes Only</span></li>
                </ul>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="top-bar-right">
                <ul class="list-inline">
                  <li class="hidden-xs" class="email"> <a href="#" title="Store Location"> <i class="icon icon-mail main-color"></i><span>info@fyeo.com</span> </a> </li>
                  <li class="hidden-xs"> <a href="${pageContext.request.contextPath}/user/order.html" title="Your Order"><i class="icon icon-transport"></i>Your Order</a> </li>
                 <% String sessions = request.getAttribute("user").toString();%>
                 <%
		     	 	if(sessions.equalsIgnoreCase("anonymousUser")){
			 	 %>
                  <li><a href="${pageContext.request.contextPath}/login.html" id="customer_login_link" title=">Sign in">Sign in </a> </li>
		 	   <% }else{ %>
	 	   			<li><a href="${pageContext.request.contextPath}/logout.html" id="customer_login_link" title=">Sign in">Logout</a> </li>
              <%--     <li><a href="${pageContext.request.contextPath}/user/profile.html" id="customer_login_link" title=">Sign in">${user}</a> </li> --%>
		 	    <%  } %>
 
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <!-- End Top Bar --> 
      
      <!-- Begin Main Header -->
      
      <div class="header-middle">
        <div class="container">
          <div class="row">
            <div class="row-wrapper"> 
              
              <div class="header-logo col-md-3">
              	 <a href="${pageContext.request.contextPath}/" title="Electro store" class="logo-site"> 
              	 	<img class="logo" src="${pageContext.request.contextPath}/additional/images/logo.png" style="width: 150px;" alt="Electro store" /> 
              	 </a> 
             </div>
   
              <div class="col-xs-12 col-md-6 top-search-holder col-sm-6">
                <div class="searchbox">
                  <div id="circularG" class="hide">
                    <div id="circularG_1" class="circularG"></div>
                    <div id="circularG_2" class="circularG"></div>
                    <div id="circularG_3" class="circularG"></div>
                    <div id="circularG_4" class="circularG"></div>
                    <div id="circularG_5" class="circularG"></div>
                    <div id="circularG_6" class="circularG"></div>
                    <div id="circularG_7" class="circularG"></div>
                    <div id="circularG_8" class="circularG"></div>
                  </div>
                  <form id="search" class="navbar-form search" action="${pageContext.request.contextPath}/search-product.html" method="get">
                    <input type="hidden" name="type" value="product" />
                    <input id="bc-product-search" type="text" name="q" class="form-control"  placeholder="Search for item" autocomplete="off" />
                    <button type="submit" class="search-icon btn-default"><i class="icon icon-search"></i></button>
                  </form>
                  <div id="result-ajax-search" class="hidden-xs">
                    <ul class="search-results">
                    </ul>
                  </div>
                </div>
              </div>
              

              
              <div class="col-xs-12 col-md-3 top-cart-row col-sm-6" ng-controller="CountHeaderController"  id="idCountheader">
                <div class="top-cart-row-container">
                  <div class="navbar navbar-responsive-menu visible-xs visible-sm">
                    <div class="btn-navbar responsive-menu" data-toggle="offcanvas"> <span class="bar"></span> <span class="bar"></span> <span class="bar"></span> </div>
                  </div>
                  <div class="wishlist-checkout-holder">
                    <div class="header-mobile-icon wishlist-target">
                    	 <a href="${pageContext.request.contextPath}/user/wishlist.html" class="num-items-in-wishlist show-wishlist" title="Wishlist"> 
                    	 	<span class="wishlist-icon"><i class="icon icon-favorites"></i><span class="number">{{cou.wishlist}}</span></span> 
                    	 </a> 
                    </div>
                  </div>
                  <div class="top-cart-holder">
                    <div class="cart-target">
                    	 <a href="${pageContext.request.contextPath}/user/cart.html" class="baske">
                    	 	 <i class="icon icon-shopping-bag"></i><span class="number"><span class="n-item">{{cou.cart}}</span></span> 
                    	 </a>
                      <!-- <div class="cart-dd dropdown-menu">
                        <div id="cart-info">
                          <div id="cart-content" class="cart-content">
                            <div class="loading"></div>
                          </div>
                        </div>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
              

              
            </div>
          </div>
        </div>
      </div>
      
	<%
	
	%>
      
      <div class="header-bot" id="err">
        <div class="container">
          <div class="row"> 
            <div class="sticky-logo col-md-3" style="display: none;"> <a href="${pageContext.request.contextPath}/" title="Electro store" class="logo-site"> <img class="logo" src="${pageContext.request.contextPath}/additional/images/logo.png" alt="Electro store" /> </a> </div>
            <div class="vertical-menu col-md-3 hidden-xs hidden-sm">
              <div class="sidemenu-holder">
                <div class="navigation ${menuOpening}" ng-controller="DepartmentController">
                  <div class="head"><i class="icon icon-category-icon"></i> All Brands</div>
                  <nav class="navbar" style="${menuStyle}">
                    <div class="collapse navbar-collapse">
                      <ul class="main-nav">
                        <li ng-repeat="ob in listDepart" ng-class="($index == 0) ? 'active':''">
                        		 <a href="${pageContext.request.contextPath}/products.html?brand={{ob.brandName}}"> <span>{{ob.brandName}}</span> </a> 
                        </li>
                      </ul>
                    </div>
                  </nav>
                </div>
              </div>
            </div>
            
           
            <div class="horizontal-menu-container col-sm-8 col-md-9">
              <div class="horizontal-menu">
                <div class="navigation hidden-xs hidden-sm">
                  <nav class="navbar">
                    <div class="collapse navbar-collapse">
                      <ul class="main-nav">
                        <li class="${homeMenu}"> <a href="${pageContext.request.contextPath}/"> <span> Home</span> </a> </li>
                        <li class="${proMenu}"> <a href="${pageContext.request.contextPath}/products.html"> <span> All Products</span> </a> </li>
                        <li class="${aboutMenu}"> <a href="${pageContext.request.contextPath}/about-us.html"> <span> About Us</span> </a> </li>
                        <li class="${contactMenu}"> <a href="${pageContext.request.contextPath}/contact-us.html"> <span> Contact Us</span> </a> </li>
                      </ul>
                    </div>
                  </nav>
                </div>
              </div>
              
              <div class="free-ship">
              	<ul class="top-bar-contact list-inline">
                  <li class="phone"><i class="icon icon-phone main-color"></i><span>(+800) 123 456 7890</span></li>
                </ul>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      
    </header>
    
    <script src="${pageContext.request.contextPath}/additional/ControllerJs/DepartmentController.js"></script>
    <script src="${pageContext.request.contextPath}/additional/ControllerJs/CountHeaderController.js"></script>
    