<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
 <div id="scroll-to-top" title="Back To Top"> <a href="javascript:;"><i class="fa fa-angle-up"></i></a> </div>
 
  <div class="loading" id="circularGCLoading" style="display: none;">
    <div id="circularG">
      <div id="circularG_1" class="circularG"></div>
      <div id="circularG_2" class="circularG"></div>
      <div id="circularG_3" class="circularG"></div>
      <div id="circularG_4" class="circularG"></div>
      <div id="circularG_5" class="circularG"></div>
      <div id="circularG_6" class="circularG"></div>
      <div id="circularG_7" class="circularG"></div>
      <div id="circularG_8" class="circularG"></div>
    </div>
  </div>
  
