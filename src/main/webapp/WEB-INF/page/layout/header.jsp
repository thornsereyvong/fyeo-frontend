<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="menu-mobile navbar">
      <div class="nav-collapse is-mobile-nav">
        <ul class="main-nav">
          <li class="li-on-mobile"> <span>Menu</span> </li>
          <li class="active"> <a href="${pageContext.request.contextPath}/"> <span>Home</span> </a> </li>
          <li class=""> <a href="${pageContext.request.contextPath}/products.html"> <span>All Products</span> </a> </li>
          <li class=""> <a href="${pageContext.request.contextPath}/about-us.html"> <span>About Us</span> </a> </li>
          <li class=""> <a href="${pageContext.request.contextPath}/contact-us.html"> <span>Contact Us</span> </a> </li>
        </ul>
      </div>
    </div>
  </div>