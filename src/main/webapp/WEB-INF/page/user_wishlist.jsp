<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="${request.contextPath}/head"></jsp:include>


<body class="customer-logged-in templateCart">
	<div class="boxes-wrapper">
		<div class="mobile-version visible-xs visible-sm">
			<jsp:include page="${request.contextPath}/header"></jsp:include>

			<div id="page-body">
				<jsp:include page="${request.contextPath}/menu"></jsp:include>
				<div id="body-content">
				  <div class="container"> 
				  	<jsp:include page="${request.contextPath}/user-admin"></jsp:include>
				  	
				  			<div class="col-md-10" ng-controller="WishListController">
						  		<div class="panel panel-default">
								  <div class="panel-heading">
								    <h3 class="panel-title">Wishlist</h3>
								  </div>
								  <div class="panel-body text-left">
								    
								    		<div class="col-sm-3 form-group">
												<div class="form-group">
													<input style="    height: 34px;" type="text" class="form-control ng-pristine ng-valid" ng-model="search" ng-change="searchOfCode()" placeholder="Search">
												</div>
											</div>
											<div class="col-sm-2 col-sm-offset-7">
												<div class="form-group">
													<div class="pull-right">
										        		<select class="form-control focus ng-pristine ng-valid" ng-model="pageSize.row" id="row" ng-options="obj.value as obj.label for obj in pageSize.rows"></select>
													</div>
												</div>
											</div>
											<table class="table table-bordered" style="margin-bottom: 30px;">
						                        <tr  dir-paginate="ob in listItem | itemsPerPage: pageSize.row" total-items="totalItem" current-page="pagination.current">
						                          <td style="width: 20%;border-bottom: 0">
						                       			<a href="javascript:void(0);" ng-click="showImage(ob.itemDetail.image)" data-target="#quick-shop-popup"  data-toggle="modal" title="Quick View">   <img class="media-object img-rounded img-responsive"  src="${pageContext.request.contextPath}/additional/images/giphy.gif" 	  lazy-img="{{ob.itemDetail.image}}" alt="placehold.it/350x250" ></a></td>
						                          <td style="width: 42%;    border-left: 0;border-right: 0; border-bottom: 0;">
						                          	<div class="row">
						                              <div class="col-lg-7 text-left	">
						                                <h4 class="list-group-item-heading"> {{ob.model}} </h4>
						                                <ul class="font2 no-style-list">
						                                  <li>FRONT Color : {{ob.itemDetail.frontColor}}</li>
						                                  <li>FRAM Color : {{ob.itemDetail.framColor}}</li>
						                                  <li>LEN Color : {{ob.itemDetail.lenColor}}</li>
						                                  <li>LEN MATERIAL : {{ob.itemDetail.lenMaterail}}</li>
						                                  <li>LAN PROPERTY : {{ob.itemDetail.lenProperty}}</li>
						                                  <li>LAN TRANS : {{ob.itemDetail.lenTrans}}</li>
						                                  <li>POLARIZED : {{ob.itemDetail.polarized}}</li>
						                                </ul>
						                              </div>
						                              <div class="col-lg-5"> <a class="pull-right font36" ng-click="deleteWish(ob.wId)" href="javascript:void(0);"><i class="fa  fa-trash-o"></i></a> <br>
						                                <h2 style="margin-top: 30px;"> HK <small>$ {{ob.itemDetail.dollarPrice | number:2}} </small></h2>
						                                <p class="font12">Suggested retail price <br/>
						                                  HK <small> $ {{ob.itemDetail.dollarRetailPrice | number:2}} </small> </p>
						                              </div>
						                            </div>
						                           </td>
						                          <td  style="width: 31%;border-bottom: 0">
						                          	<div class="row">
						                              <h5>QUANTITY</h5>
						                              <hr/>
						                              <div id="product-action-9923383239" class="options" ng-repeat="st in ob.itemDetail.stocks">
						                                <div class="qty-add-cart">
						                                  <div class="quantity-product">
						                                    <div class="col-lg-4 form-group1 padding-top-8">
						                                      <label>Size {{st.size}} :</label>
						                                    </div>
						                                    <div class="col-lg-5">
						                                      <div class="quantity form-group1" >
						                                        <input type="text" ng-model="st.qty" class="item-quantity" min="1" value="{{st.qty}}" ng-change="changeNum(st)" name="quantity" >
						                                        <span class="qty-wrapper"> <span class="qty-inner"> <span class="qty-up" title="Increase" ng-click="(st.qty=st.qty+1)" data-src="#quantity"> <i class="fa fa-plus"></i> </span> <span class="qty-down" title="Decrease" ng-click="(st.qty == 0) ? 0:(st.qty=st.qty-1)"  data-src="#quantity"> <i class="fa fa-minus"></i> </span> </span> </span> </div>
						                                    </div>
						                                    <div class="col-lg-1" style=" padding-top: 5px;"> <span class="label label-success custom_stock" ng-if="st.skStatus == 1">&nbsp;</span> <span class="label label-warning custom_stock" ng-if="st.skStatus == 0">&nbsp;</span> </div>
						                                    <div ng-if="st.qty > 0" class="col-lg-12 animationIf"   >
						                                      <div class="row">
						                                        <div class="col-lg-12">
						                                          <label class="font2" style="padding-left: 5px;">CUSTOMER REFERENCE max 250 characters</label>
						                                        </div>
						                                        <div class="col-lg-5">
						                                          <div class="form-group">
						                                            <textarea rows=""  class="form-control " ng-model="st.comment" style="resize: none; width: 158px; height: 70px; max-height: 70px;min-height: 70;min-width: 158px;max-width: 200px;padding: 9px;margin-top: 6px;" cols=""></textarea>
						                                          </div>
						                                        </div>
						                                        <div class="col-lg-6 text-right">
						                                          <div class="form-group">
						                                            <button ng-click="cartAdd(ob.itemId,ob.brand,ob.model,st.size,st.qty,ob.itemDetail.dollarPrice,st.comment)" class="btn custom_btn_add_cart btn-xs btn-default"><span class="icon icon-add-to-cart"></span> &nbsp; Add To Cart</button>
						                                          </div>
						                                        </div>
						                                      </div>
						                                    </div>
						                                    <div class="clearfix"> </div>
						                                  </div>
						                                </div>
						                              </div>
						                            </div></td>
						                        </tr>
						                      </table>
						                      
						                      <div class="row">
						                       <div class="col-lg-3 ">
								            		<div class="panel panel-default">
													  <div class="panel-heading">
													    <h3 class="panel-title">Empty Wishlist</h3>
													  </div>
													  <div class="panel-body">
													  	<button ng-click="deleteWishAll()" style="    padding: 4px 15px;" class="btn btn-danger"> Empty Wishlist <i class="fa fa-trash-o"></i></button>
													  </div>
													 </div>
						            			</div>
						            			
						            			<div class="col-lg-3 col-lg-offset-6">
						            			 <div class="form-group pull-right">
							                        <button ng-click="cartAddList()" class="btn  btn-xs btn-default"><span class="icon icon-add-to-cart"></span> &nbsp; Add To Cart</button>
							                      </div>
								    			</div>
						                      </div>
						                     
						                     
								    
								    
								    
		                            </div>
		                            
		                            <div clsss="panel-footer">
		                            	<div class="pagination-holder pagination-catalog">
						                    <div class="row">
						                      <div class="col-md-12 col-xs-12">
						                        <dir-pagination-controls boundary-links="true"  on-page-change="pageChanged(newPageNumber)"></dir-pagination-controls>
						                      </div>
						                    </div>
						                  </div>
		                            </div>
		                            
		                            
		                          </div>
		                        </div>
		                      </div>
		                    </div>
		                   
						</div>
					</div>
				  </div>
					<div id="main-content">
						<div class="main-content">
							   
						</div>
					</div>

				</div>
				<div id="bottom" class="bottom-container">
					<div class="container"></div>
				</div>
  				 <script src="${pageContext.request.contextPath}/additional/ControllerJs/WishListController.js"></script> 
				<jsp:include page="${request.contextPath}/foot"></jsp:include>
			</div>
			<jsp:include page="${request.contextPath}/footer"></jsp:include>
		</div>
	</div>
</body>
</html>