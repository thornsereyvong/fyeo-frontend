<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="${request.contextPath}/head"></jsp:include>


<body class="customer-logged-in templateCart">
	<div class="boxes-wrapper">
		<div class="mobile-version visible-xs visible-sm">
			<jsp:include page="${request.contextPath}/header"></jsp:include>

			<div id="page-body">
				<jsp:include page="${request.contextPath}/menu"></jsp:include>
				<div id="body-content">
				  <div class="container"> 
				  	<jsp:include page="${request.contextPath}/user-admin"></jsp:include>
				  	
				  			<div class="col-md-10" ng-controller="CartController">
						  		<div class="panel panel-default">
								  <div class="panel-heading">
								    <h3 class="panel-title">Cart</h3>
								  </div>
								  <div class="panel-body">
								  <div ng-show="submitCart">
								 	<div class="row">
						            	<div class="col-lg-8 col-md-8 col-sm-5 col-xs-12">
						            		<div class="panel panel-default">
											  <div class="panel-heading">
											    <h3 class="panel-title">Information</h3>
											  </div>
											  <div class="panel-body">
											  	<div class="row">
											  		<div class="col-lg-12">
											  		
											                <div class="card">
											                    <div class="card-body">
											                        <h2 class="card-title"></h2>
											                        <hr>
											                        <div class="form-group">
											                            <label for="email" class="col-form-label">Shop Name</label>
											                            <input type="email" disabled="disabled" class="form-control" id="shopName" placeholder=""   required>
											                            <div class="email-feedback">
											                            
											                            </div>
											                        </div>
											                        <div class="form-group">
											                            <label for="email" class="col-form-label">Email</label>
											                            <input type="email" class="form-control"  disabled="disabled"  id="email" placeholder="example@gmail.com"   required>
											                            <div class="email-feedback">
											                            
											                            </div>
											                        </div>
											                        <div class="form-group">
											                            <label for="tel" class="col-form-label">Tel 1</label>
											                            <input type="text" class="form-control"  disabled="disabled"  id="tel1" placeholder="+33 6 99 99 99 99" required>
											                            <div class="phone-feedback">
											                            
											                            </div>
											                        </div>
											                        <div class="form-group">
											                            <label for="tel" class="col-form-label">Tel 2</label>
											                            <input type="text" class="form-control"  disabled="disabled"  id="tel2" placeholder="+33 6 99 99 99 99" required>
											                            <div class="phone-feedback">
											                            </div>
											                        </div>
											                         <div class="form-group">
											                         
											                            <label for="tel" class="col-form-label"><input type="radio" value="1" checked="checked" name="checkAddress"> Ship Address</label>
											                            <select class="form-control" id="shipAddress">
											                            	<option value="{{myprofile.ship1}}">{{myprofile.ship1}}</option>
											                            	<option value="{{myprofile.ship2}}">{{myprofile.ship2}}</option>
											                            	<option value="{{myprofile.ship3}}">{{myprofile.ship3}}</option>
											                            	<option value="{{myprofile.ship4}}">{{myprofile.ship4}}</option>
											                            	<option value="{{myprofile.ship5}}">{{myprofile.ship5}}</option>
											                            	<option value="{{myprofile.ship6}}">{{myprofile.ship6}}</option>
											                            	<option value="{{myprofile.ship7}}">{{myprofile.ship7}}</option>
											                            	<option value="{{myprofile.ship8}}">{{myprofile.ship8}}</option>
											                            	<option value="{{myprofile.ship9}}">{{myprofile.ship9}}</option>
											                            	<option value="{{myprofile.ship10}}">{{myprofile.ship10}}</option>
											                            </select>
											                        </div>
											                        
											                        <div class="form-group">
											                         
											                            <label for="tel" class="col-form-label">
											                            	<input type="radio"  name="checkAddress" value="2"> Other Ship Address</label>
											                           		
											                        </div>
											                        <div class="form-group">
											                        	<textarea rows="" id="contactInfo" style="width: 565px; height: 93px;resize:none;" cols=""></textarea>
											                        </div>
											                        
											                        
											                    </div>
											                </div>
											  		
												  		
							                        </div>
											  	</div>
											  </div>
											 </div>
				            			</div>
				            			<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                  
											                    <div class="widget">
											                        <h4 class="widget-title">Order Summary</h4>
											                        <div class="summary-block">
											                            <div class="summary-content">
											                                <div class="summary-head"><h5 class="summary-title">Date</h5></div>
											                                <div class="summary-price">
											                                    <p class="summary-text" id="dateSub"></p>
											                                </div>
											                            </div>
											                        </div>
											                        <div class="summary-block">
											                            <div class="summary-content">
											                               <div class="summary-head"> <h5 class="summary-title">Product Order </h5></div>
											                                <div class="summary-price">
											                                    <p class="summary-text">{{countPro}}</p>
											                                </div>
											                            </div>
											                        </div>
											                        <div class="summary-block">
											                            <div class="summary-content">
											                               <div class="summary-head"> <h5 class="summary-title">Total</h5></div>
											                                <div class="summary-price">
											                               	 $ {{countPrice | number:2}}
											                                </div>
											                            </div>
											                        </div>
											                        
											                        <div class="summary-block">
											                            <div class="summary-content">
											                               <div class="summary-head"> 
											                               	<h5 class="summary-title">Description</h5>
											                              	 <textarea id="description" rows="" cols="" class="form-control" style=" resize: none; border-radius: 3px; padding: 2px 2px 2px 2px; font-size: 12px; width: 187px;   height: 150px; margin-top: 12px;"></textarea>
											                               </div>
											                               
											                            </div>
											                        </div>
											                        
											                        
											                        <br>
											                        <button ng-click="submitCart = false;" style="    padding: 4px 15px;" class="btn btn-danger">Cancel</button>
																	<button ng-click="submitOrder()" style="    padding: 4px 15px;" class="btn btn-info">Check Out </button>
											                    </div>
											                   
											                </div>
						          </div>
								  </div>
								  	<div ng-show="submitCart == false">
										  <div class="row">
											  <div class="col-sm-3 form-group">
													<div class="form-group">
														<input style="    height: 34px;" type="text" class="form-control ng-pristine ng-valid" ng-model="search" ng-change="searchOfCode()" placeholder="Search">
													</div>
												</div>
												<div class="col-sm-2 col-sm-offset-7 ">
													<div class="form-group pull-right">
														<div class="pull-right">
											        		<select class="form-control focus ng-pristine ng-valid" ng-model="pageSize.row" id="row" ng-options="obj.value as obj.label for obj in pageSize.rows"></select>
														</div>
													</div>
												</div>
										  </div>
										  
													
										<div class="table-responsive">		
										   <table class="table table-hover">
								                <thead>
								                    <tr>
								                    	<th style="width: 30px;">Check </th>
								                    	<th style="width: 30px;">Image</th>
								                        <th style="width: 30px;">Brand</th>
								                        <th style="width: 30px;">Product</th>
								                        <th style="width: 30px;">Model</th>
								                        <th style="width: 30px;">Size</th>
								                        <th style="width: 40px;">Quantity</th>
								                        <th style="width: 75px;">Unit Price</th>	
								                        <th style="width: 75px;">Total Amt</th>	
								                        <th style="width: 30px;">AV</th>
								                        <th style="width: 103px;">Delivery Date</th>
								                        <th style="width: 61px;">Action</th>
								                    </tr>
								                </thead>
								                <tbody>
								                     <tr  dir-paginate="ob in listItem | itemsPerPage: pageSize.row" total-items="totalItem" current-page="pagination.current" id="{{ob.cartId}}">
								                    	<td  style="padding: 0;padding-top: 10px;">
								                    		<input type="checkbox" ng-model="ob.status" id="check{{ob.cartId}}" ng-click="selectCheck(ob)" >
								                    	</td>
								                    	<td >
								                        	<a href="${pageContext.request.contextPath}/product/{{ob.brand}}/{{ob.itemId}}.html"> <img class="img-responsive"  src="${pageContext.request.contextPath}/additional/images/giphy.gif"  lazy-img="{{ob.itemDetail.image}}" alt="placehold.it/350x250" /></a></td>
								                        </td>
								                        <td>
								                        	{{ob.brand}}
								                        </td>
								                        <td >
								                        	<strong>{{ob.itemId}}</strong><br>
								                        </td>
								                        <td >
								                        	{{ob.model}}<br>
								                        </td>
								                        <td >
								                        	{{ob.size}}
								                        </td>
								                        
								                        <td >
								                        	<div class="qty-add-cart">
							                                  <div class="quantity-product">
							                                    <div class="col-lg-12">
							                                      <div class="quantity form-group1">
							                                        <input type="text" ng-model="ob.qty" class="item-quantity ng-pristine ng-valid" min="1" value="0" ng-change="changeQty(ob)" name="quantity">
							                                        <span class="qty-wrapper"> <span class="qty-inner"> <span class="qty-up" title="Increase" ng-click="(ob.qty=ob.qty+1);changeQty(ob)" data-src="#quantity"> <i class="fa fa-plus"></i> </span> <span class="qty-down" title="Decrease" ng-click="(ob.qty == 0) ? 0:(ob.qty=ob.qty-1);changeQty(ob)" data-src="#quantity"> <i class="fa fa-minus"></i> </span> </span> </span> </div>
							                                    </div>
							                                    <div class="clearfix"> </div>
							                                  </div>
							                                </div>
								                        </td>
								                         <td style="font-size: 13px;" >
								                        	$ {{ob.unitprice | number:2}}
								                        </td>
								                         <td style="font-size: 13px;">
								                        	$ {{ob.amount | number:2}}
								                        </td>
								                        <!--  <td >
								                         	<textarea class="form-control"  ng-model="ob.comment" style="    resize: none;height: 50px;border-radius: 3px; padding: 2px 2px 2px 2px;  font-size: 12px;" rows="" cols=""></textarea>
								                         </td> -->
								                         <td>
								                         	<span class="label label-success custom_stock" ng-if="ob.itemDetail.stocks[0].skStatus == 1">&nbsp;</span> 
									                        <span class="label label-warning custom_stock" ng-if="ob.itemDetail.stocks[0].skStatus == 0">&nbsp;</span> 
								                         </td>
								                       <td >
								                          
									                        <div class="form-group has-feedback" id="control_startDate">
									                          <div class="input-group">
									                            <div class="input-group-addon" style="padding: 3px 5px;"> <i class="fa fa-calendar"></i> </div>
									                            <input type="text" class="cust_input form-control pull-right date2 "  style="border-radius: 0; height: 30px;     padding: 0 0 0 5px;" >
									                          </div>
									                          </div>
								                        </td> 
								                        <td>
								                        	  <button class="btn btn-xs " ng-click="showComment(ob)" style="font-size: 14px;  padding: 5px;"><i class="fa fa-edit"></i></button>
									                          <button class="btn btn-xs " ng-click="deleteCart(ob.cartId)" style="    font-size: 14px;  padding: 5px;"><i class="fa  fa-trash-o"></i></button>
								                        </td>
								                       
								                    </tr>
								                  
								                   
								                </tbody>
								            </table>
								            <div class="col-lg-12">
								            	<div class="pagination-holder pagination-catalog">
								                    <div class="row">
								                      <div class="col-md-12 col-xs-12">
								                        <dir-pagination-controls boundary-links="true"  on-page-change="pageChanged(newPageNumber)"></dir-pagination-controls>
								                      </div>
								                    </div>
								                  </div>
								                  
								            </div>
								            </div>
								            
								            
								            <div class="row" style="margin-top: 10px;">
								            	<div class="col-lg-6">
								            		<div class="panel panel-default">
													  <div class="panel-heading">
													    <h3 class="panel-title">CART SUMMARY</h3>
													  </div>
													  <div class="panel-body">
													  	<table class="table">
													  		<tr>
													  			<td>Date</td>
													  			<td>SELECTED PRODUCTS</td>
													  			<td>Total</td>
													  		</tr>
													  		<tr>
													  			<td><span id="dateS"></span></td>
													  			<td>{{countPro}}</td>
													  			<td>	<span ng-show="(countPrice != 0) || (countPrice != '')"> $  </span> {{countPrice | number:2}}</td>
													  		</tr>
													  		<tr>
													  			<td colspan="3" class="text-left">
													  				<button ng-click="submitButton()" style="    padding: 4px 15px;" class="btn btn-info">Check Out <i class="fa fa-shopping-cart"></i></button>
													  			</td>
													  		</tr>
													  	</table>
													  </div>
													 </div>
						            			</div>
						            			
						            			
						            			<div class="col-lg-3 col-lg-offset-3">
								            		<div class="panel panel-default">
													  <div class="panel-heading">
													    <h3 class="panel-title">Empty Cart</h3>
													  </div>
													  <div class="panel-body">
													  	<button ng-click="deleteCartAll()" style="    padding: 4px 15px;" class="btn btn-danger">Empty Cart <i class="fa fa-trash-o"></i></button>
													  </div>
													 </div>
						            			</div>
						            		</div>
						            </div>
						            
						            
						            
						            
								  </div>
								</div>
						  	</div>
				  </div>
					<div id="main-content">
						<div class="main-content">
							   
								
								<div id="quick-shop-popup" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1" style="display: none;">
								  <div class="modal-dialog">
								    <div class="modal-content">
								
								      <div class="modal-header">
								        <span class="close" title="Close" data-dismiss="modal" id="close-quick-shop-popup" aria-hidden="true"></span>
								      </div>
								
								      <div class="modal-body">
								        <div class="row">
								          
								        </div>
								      </div>
								
								    </div>
								  </div>
								</div>



						
						</div>
					</div>

				</div>
				<div id="bottom" class="bottom-container">
					<div class="container"></div>
				</div>
				<script src="${pageContext.request.contextPath}/additional/ControllerJs/CartController.js"></script> 
				<jsp:include page="${request.contextPath}/foot"></jsp:include>
			</div>
			<jsp:include page="${request.contextPath}/footer"></jsp:include>
		</div>
	</div>
	<script>
		$(document).ready(function(){
			$('.date2').daterangepicker({
		        format: 'DD-MM-YYYY',
		        singleDatePicker: true,
		        showDropdowns: true,
		        startDate: moment(),
		        minDate: moment(),
		        locale: { 
		            format: 'DD-MM-YYYY',
		        }
		    });
			
			 $('#quick-shop-popup').on( 'shown.bs.modal', function () {    
				    //  AT_Main.handleReviews();
				  });
		});
	</script>
	<style type="text/css">
	
.box { background-color: #fff; border-radius: 8px; border: 2px solid #e9ebef; padding: 50px; margin-bottom: 40px; }
.box-title { margin-bottom: 30px; text-transform: uppercase; font-size: 16px; font-weight: 700; color: #094bde; letter-spacing: 2px; }
.plan-selection { border-bottom: 2px solid #e9ebef;     padding-bottom: 10px;
    margin-bottom: 10px}
.plan-selection:last-child { border-bottom: 0px; margin-bottom: 0px; padding-bottom: 0px; }
.plan-data { position: relative; }
.plan-data label { font-size: 20px; margin-bottom: 15px; font-weight: 400; }
.plan-text { padding-left: 35px; }
.plan-price { position: absolute; right: 0px; color: #094bde; font-size: 20px; font-weight: 700; letter-spacing: -1px; line-height: 1.5; bottom: 43px; }
.term-price { bottom: 18px; }
.secure-price { bottom: 68px; }
.summary-block { border-bottom: 2px solid #d7d9de; }
.summary-block:last-child { border-bottom: 0px; }
.summary-content { padding: 28px 0px; }
.summary-price { color: #094bde; font-size: 20px; font-weight: 400; letter-spacing: -1px; margin-bottom: 0px; display: inline-block; float: right; }
.summary-small-text { font-weight: 700; font-size: 12px; color: #8f929a; }
.summary-text { margin-bottom: -10px; }
.summary-title { font-weight: 700; font-size: 14px; color: #1c1e22; }
.summary-head { display: inline-block; width: 120px; }

.widget { margin-bottom: 30px; background-color: #e9ebef; padding: 50px; border-radius: 6px; }
.widget:last-child { border-bottom: 0px; }
.widget-title { color: #094bde; font-size: 16px; font-weight: 700; text-transform: uppercase; margin-bottom: 25px; letter-spacing: 1px; display: table; line-height: 1; }
	.quantity input {
    width: 93px;
    }
	.dropdown-menu {
  
    width: 241px !important;
    }
		.cust_input{
			border-radius: 0;
    height: 30px;
    padding: 0;
		}
		select, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"] {
    padding: 15px 15px;
    line-height: 2.35;
    height: 40px;
    }
	</style>
</body>
</html>