<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="${request.contextPath}/head"></jsp:include>
<body class="customer-logged-in templateCart">
<div class="boxes-wrapper">
  <div class="mobile-version visible-xs visible-sm">
    <jsp:include page="${request.contextPath}/header"></jsp:include>
<style>
@import url("https://use.fontawesome.com/releases/v5.0.11/css/all.css");
input[type="checkbox"], input[type="radio"]{
	position: absolute;
	right: 9000px;
}

/*Check box*/
input[type="checkbox"] + .label-text:before{
	content: "\f0c8";
	font-family: "Font Awesome 5 Free";
	speak: none;
	font-style: normal;
	font-variant: normal;
	text-transform: none;
	line-height: 1;
	-webkit-font-smoothing:antialiased;
	width: 1em;
	display: inline-block;
	margin-right: 5px;
}

input[type="checkbox"]:checked + .label-text:before{
	content: "\f14a";
	color: #2980b9;
	animation: effect 250ms ease-in;
	font-weight: 900;
}

input[type="checkbox"]:disabled + .label-text{
	color: #aaa;
}

input[type="checkbox"]:disabled + .label-text:before{
	content: "\f0c8";
	color: #ccc;
}

/*Radio box*/

input[type="radio"] + .label-text:before{
	content: "\f111";
	font-family: "Font Awesome 5 Free";
	speak: none;
	font-style: normal;
	font-variant: normal;
	text-transform: none;
	line-height: 1;
	-webkit-font-smoothing:antialiased;
	width: 1em;
	display: inline-block;
	margin-right: 5px;
}

input[type="radio"]:checked + .label-text:before{
	content: "\f192";
	color: #8e44ad;
	animation: effect 250ms ease-in;
}

input[type="radio"]:disabled + .label-text{
	color: #aaa;
}

input[type="radio"]:disabled + .label-text:before{
	content: "\f111";
	color: #ccc;
}

/*Radio Toggle*/

.toggle input[type="radio"] + .label-text:before{
	content: "\f204";
	font-family: "Font Awesome 5 Free";
	speak: none;
	font-style: normal;
	font-weight: 900;
	font-variant: normal;
	text-transform: none;
	line-height: 1;
	-webkit-font-smoothing:antialiased;
	width: 1em;
	display: inline-block;
	margin-right: 10px;
}

.toggle input[type="radio"]:checked + .label-text:before{
	content: "\f205";
	color: #16a085;
	animation: effect 250ms ease-in;
}

.toggle input[type="radio"]:disabled + .label-text{
	color: #aaa;
}

.toggle input[type="radio"]:disabled + .label-text:before{
	content: "\f204";
	color: #ccc;
}


@keyframes effect{
	0%{transform: scale(0);}
	25%{transform: scale(1.3);}
	75%{transform: scale(1.4);}
	100%{transform: scale(1);}
}
</style>
    <div id="page-body"> 
      <jsp:include page="${request.contextPath}/menu"></jsp:include>

      <div id="body-content" ng-controller="ProductsController">
        <div class="container"> 
          
          <div id="breadcrumb" class="breadcrumb-holder">
            <ul class="breadcrumb">
              <li itemscope > <a itemprop="url" href="${pageContext.request.contextPath}/"> Home </a><i class="icon icon-thing-arrow-right"></i> </li>
              <li class="active">All Products</li>
            </ul>
          </div>
          <div id="main-content">
            <div class="main-content">
              <div class="page-cata row">
                <div id="sidebar" class="col-md-3 hidden-sm hidden-xs">
                  <div class="sidebar sidebar-catalog"> 
                 
                    <div class="sb-widget sb-filter-wrapper">
                      <h4 class="sb-title">Filters</h4>
                      <div class="sbw-filter">
                        <div class="grid-uniform">
                          <div class="sb-widget sb-filter brand" id="filter-1">
                            <div class="sbf-title"> <span>Brand <i class="fa fa-angle-down visible-xs"></i> <i class="fa fa-angle-up visible-xs"></i> </span> <a href="#" class="clear-filter hidden hide" id="clear-filter-1" style="float: right;">Clear</a> </div>
                            <ul class="advanced-filters">
                            	<li class="advanced-filter rt">
                              	<div class="form-check" style="padding-left: 2px;">
									<label>
										<input type="radio" name="radio" ng-model="filterBrand"   ng-click="filterClear()" > <span class="label-text"> All</span>
									</label>
								</div>
                               </li>
                              <li class="advanced-filter rt" ng-repeat="ob in listItemBrands">
                              	<div class="form-check" style="    padding-left: 2px;">
									<label>
										<input type="radio" name="radio" ng-model="ob.brandId" ng-checked="ob.brandName == search"  ng-change="filterData(ob.brandName)" > <span class="label-text">{{ob.brandName}}</span>
									</label>
								</div>
                               </li>
                            </ul>
                          </div>
                         
                         <!--  <div class="sb-widget sb-filter color" id="filter-2">
                            <div class="sbf-title"> <span>Color <i class="fa fa-angle-down visible-xs"></i> <i class="fa fa-angle-up visible-xs"></i> </span> <a href="#" class="clear-filter hidden hide" id="clear-filter-2" style="float: right;">Clear</a> </div>
                            <ul class="advanced-filters list-inline afs-color">
                              <li class="advanced-filter af-color" data-group="Color" style="background-color:black; background-image: url(../../cdn.shopify.com/s/files/1/1801/4931/t/2/assets/black77cd.png?4608748305671055686);"> <a href="all/color_black.html" title="Black"></a> </li>
                              <li class="advanced-filter af-color" data-group="Color" style="background-color:blue; background-image: url(../../cdn.shopify.com/s/files/1/1801/4931/t/2/assets/blue77cd.png?4608748305671055686);"> <a href="all/color_blue.html" title="Blue"></a> </li>
                              <li class="advanced-filter af-color" data-group="Color" style="background-color:brown; background-image: url(../../cdn.shopify.com/s/files/1/1801/4931/t/2/assets/brown77cd.png?4608748305671055686);"> <a href="all/color_brown.html" title="Brown"></a> </li>
                              <li class="advanced-filter af-color" data-group="Color" style="background-color:gold; background-image: url(../../cdn.shopify.com/s/files/1/1801/4931/t/2/assets/gold77cd.png?4608748305671055686);"> <a href="all/color_gold.html" title="Gold"></a> </li>
                              <li class="advanced-filter af-color" data-group="Color" style="background-color:grey; background-image: url(../../cdn.shopify.com/s/files/1/1801/4931/t/2/assets/grey77cd.png?4608748305671055686);"> <a href="all/color_grey.html" title="Grey"></a> </li>
                              <li class="advanced-filter af-color" data-group="Color" style="background-color:pink; background-image: url(../../cdn.shopify.com/s/files/1/1801/4931/t/2/assets/pink77cd.png?4608748305671055686);"> <a href="all/color_pink.html" title="Pink"></a> </li>
                              <li class="advanced-filter af-color" data-group="Color" style="background-color:red; background-image: url(../../cdn.shopify.com/s/files/1/1801/4931/t/2/assets/red77cd.png?4608748305671055686);"> <a href="all/color_red.html" title="Red"></a> </li>
                              <li class="advanced-filter af-color" data-group="Color" style="background-color:white; background-image: url(../../cdn.shopify.com/s/files/1/1801/4931/t/2/assets/white77cd.png?4608748305671055686);"> <a href="all/color_white.html" title="White"></a> </li>
                              <li class="advanced-filter af-color" data-group="Color" style="background-color:yellow; background-image: url(../../cdn.shopify.com/s/files/1/1801/4931/t/2/assets/yellow77cd.png?4608748305671055686);"> <a href="all/color_yellow.html" title="Yellow"></a> </li>
                            </ul>
                          </div> -->
                          
                          
                          
                        </div>
                      </div>
                    </div>
                  
                    <div class="sb-widget">
                      <div class="sb-banner"> <a href="#"> <img src = "${pageContext.request.contextPath}/additional/images/mobile-category-banner-06.png" alt="" /> </a> </div>
                    </div>
                    
                  
                    
                   
                    
                    
                  </div>
                </div>
                <div id="col-main" class="col-md-9 col-xs-12">
                  <div class="cata-header"> <img src="${pageContext.request.contextPath}/additional/images/B8_Main_banner.jpg" alt="" /> </div>
                  <div class="wrap-cata-title">
                    <h2>Products</h2>
                    <p class="pagination-top-showing"> </p>
                  </div>
                  <div class="cata-toolbar">
                    <ul class="group-toolbar list-inline">
                      <li class="group-gl">
                        <ul class="list-inline">
                          <li class="grid-list"> 
	                          <span class="grid" title="Grid"><i class="fa fa-th"></i></span>
	                          <!--  <span class="grid-extended" title="Grid Extended"><i class="fa fa-align-justify"></i></span>  -->
	                          <span class="list" title="List"><i class="fa fa-list"></i></span> 
	                          <span class="list-small" title="List Small"><i class="fa fa-th-list"></i></span> 
                          </li>
                        </ul>
                      </li>
                      
                      <li class="pagination-top">
                        <ul class="list-inline">
                          <li>	
                          <select class="form-control focus" ng-model="pageSize.row" ng-change="changeRowPage()" id ="row" ng-options="obj.value as obj.label for obj in pageSize.rows"></select>
                          </li>
                         
                        </ul>
                      </li>
                    </ul>
                  </div>
              
                  <div class="cata-product cp-grid clearfix">
                  
                  
                    <div class="product-grid-item product-price-range"  dir-paginate="ob in listItems | itemsPerPage: pageSize.row" total-items="totalItem" current-page="pagination.current">
                      <div class="product-wrapper pageLeft-animate">
                        <h3 class="noti-title" style="display: none;">You may also like:</h3>
                        <div class="product-head">
                          <div class="product-image">
                            <div class="product-wrap-info">
                            <h5 class="product-name"> <a href="${pageContext.request.contextPath}/product/{{ob.brand}}/{{ob.itemId}}.html">{{ob.brand}}</a> </h5>
                            <h5 class="citemname"> <a href="${pageContext.request.contextPath}/product/{{ob.brand}}/{{ob.itemid}}.html">{{ob.itemname}}</a> </h5>
                              <div class="product-type">{{ob.color}}</div>
                              
                              <div class="product-review"> <span class="shopify-product-reviews-badge" data-id="9923365255"></span> </div>
                              <div class="product-des-list">
                               <p>Item Name : {{ob.itemname}}</p>
                              </div>
                            </div>
                            <div class="featured-img"> <a href="${pageContext.request.contextPath}/product/{{ob.brand}}/{{ob.itemId}}.html"> <img class="featured-image"  src="${pageContext.request.contextPath}/additional/images/giphy.gif"  lazy-img="{{ob.image}}" alt="Black Fashion Zapda Example" /> <!-- <span class="product-label"> <span class="label-sale"> <span class="sale-text"> Sale </span> </span> </span> --> </a> </div>
                          </div>
                        </div>
                        <div class="product-content">
                          <div class="pc-inner">
                            <div class="product-review"> <span class="shopify-product-reviews-badge" data-id="9923365255"></span> </div>
                            <div class="product-des-grid">
                             <!--  <p>Description
                                Our new HPB12 / A12 battery is rated at 2000mAh and designed to power up Black and De...</p> -->
                            </div>
                            <div class="availability">
                              <label>Availability:</label>
                              <span class="available">In Stock</span> </div>
                            <div class="price-cart-wrapper">
                              <div class="product-price"> 
                              	<span class="price-sale"><small style="color:#4e4e4e">from</small>  <span class="money ">$ {{ob.wsPriceDollar | number:2}}</span></span> <br>
                                <span class="price-sale"><small style="color:#4e4e4e;" class="font12">Suggested retail price  $ </small> <span class="money font12">{{ob.rtPriceDollar | number:2}}</span></span></div>
                            </div>
                            <div class="product-button" >
                              <div data-handle="black-fashion-zapda-shoes" data-target="#quick-shop-popup" class="quick_shop quick-shop-button hidden-sm hidden-xs" data-toggle="modal" title="Quick View"> <i class="icon icon-compare"></i>Quick View  </div>
                              <!-- <div class="product-wishlist"> <a class="add-to-wishlist add-product-wishlist" data-handle-product="black-fashion-zapda-shoes" href="javascript:void(0);" ng-click="wishListAdd(ob.itemId)" title="Wishlist"><i class="icon icon-favorites"></i>Wishlist</a> </div> -->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
  
                  <div class="pagination-holder pagination-catalog">
                    <div class="row">
                      <div class="col-md-12 col-xs-12">
                        <dir-pagination-controls boundary-links="true"  on-page-change="pageChanged(newPageNumber)"></dir-pagination-controls>
                      </div>
                      <!-- <div class="col-xs-12 col-md-7">
                        <ul class="pagination">
                          <li class="active"><a href="javascript:;">1</a></li>
                          <li><a href="all4658.html?page=2">2</a></li>
                          <li><a href="all9ba9.html?page=3">3</a></li>
                          <li><a href="allfdb0.html?page=4">4</a></li>
                        </ul>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
        <script src="${pageContext.request.contextPath}/additional/ControllerJs/ProductsController.js"></script>
      
      <div id="bottom" class="bottom-container">
        <div class="container"></div>
      </div>
      
      <jsp:include page="${request.contextPath}/foot"></jsp:include>
    </div>
    <jsp:include page="${request.contextPath}/footer"></jsp:include>
  </div>
</div>
</body>
</html>