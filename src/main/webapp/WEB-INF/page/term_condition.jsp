<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="${request.contextPath}/head"></jsp:include>
<body class="customer-logged-in templateCart">
<div class="boxes-wrapper">
  <div class="mobile-version visible-xs visible-sm">
    <jsp:include page="${request.contextPath}/header"></jsp:include>
    <div id="page-body">
      <jsp:include page="${request.contextPath}/menu"></jsp:include>
      <div id="body-content" class="has-header-fixed">
        <div class="container"> 
          
 
          
          <div id="breadcrumb" class="breadcrumb-holder">
            <ul class="breadcrumb">
              <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a itemprop="url" href="/"> Home </a><i class="icon icon-thing-arrow-right"></i> </li>
              <li class="active">Term Condition</li>
            </ul>
          </div>
       
          
          <div id="main-content">
            <div class="main-content">
              <div class="row">
                <!-- <div id="col-main" class="col-sm-12 page-cart">
                  <h1 class="page-heading">Shopping Cart Summary</h1>
                  <div class="text-center cart-empty-wrapper" rv-show="cart.item_count | lt 1" style="display: none;">
                    <p class="cart empty">Your shopping cart is empty.</p>
                    <a href="/collections/all"><i class="fa fa-long-arrow-right"></i> Continue Shopping</a> </div>
           
                </div> -->
               
              </div>
            </div>
          </div>
          
          <!-- End Main Content --> 
          
        </div>
      </div>
      <div id="bottom" class="bottom-container">
        <div class="container"></div>
      </div>
      <jsp:include page="${request.contextPath}/foot"></jsp:include>
    </div>
    <jsp:include page="${request.contextPath}/footer"></jsp:include>
  </div>
</div>
</body>
</html>